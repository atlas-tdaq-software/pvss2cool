--//////////////////////////////////////////////////////////////////////////////////
-- Scripts creates the pvss2cool_folders_R2 table for Run2 as the copy
-- of definitions of a set of folders of Run1.
-- Must be followed by physycal creating those folders in new COOL DB
-- by 'pvss2cool configurePvssCool -d NEW_DB_NAME'
--
-- BEFORE STARTING this script DO the following:
-- 1. Fill in the folder name list in line of INSERT INTO PVSS2COOL_FOLDERS_R2
--    statement the names of folders to be copied from PVSS2COOL_FOLDERS
-- 2. Set the initial value of PVSS2COOL_FOLDERS_R2_SEQ in the CREATE SEQUENCE
--    statement to be more than maximum FOLDERID in PVSS2COOL_FOLDERS
--
-- Since in Run2 (and later on) the same PVSS2COOL_FILEDS and PVSS2COOL_LISTS
-- tables will be used (whether a folder in .._FOLDERS or in .._FOLDERS_R2),
-- we've to remove the foreign key FOLDERID from both those tables.
--
--//////////////////////////////////////////////////////////////////////////////////
-- Create Folders table
CREATE TABLE PVSS2COOL_FOLDERS_R2 (
	FOLDERID          NUMBER(20) NOT NULL,
	FOLDERNAME        VARCHAR2(4000) NOT NULL,
	FOLDERDESC        VARCHAR2(4000),
	FOLDERGROUP       NUMBER(10) DEFAULT 0 NOT NULL,
	GROUPTABLE        VARCHAR2(255),
	DPTYPENAME        VARCHAR2(255),
	DPSUBTYPENAME     VARCHAR2(255),
	DPTINFO           NUMBER(10),
	USEALIAS          NUMBER(1),
	USELIST           NUMBER(1),
	DPPATTERN         VARCHAR2(255),
	CHANNELIDTYPE     NUMBER(10),
	CHANNELIDINFO     VARCHAR2(255),
	LASTUPDATEBEFORE  VARCHAR2(25),
	LASTUPDATEAFTER   VARCHAR2(25),
	MECHANISM         NUMBER(10),
	UPDATEINTERVAL    NUMBER(10),
	INTERVALCOUNT     NUMBER(10),
	STATUSREQ         NUMBER(10),
	STATUS            NUMBER(10),
	LISTCHANGE        NUMBER(1),
	ALIASDPE          VARCHAR2(255),
	TIMEPRECISION     VARCHAR2(63)
);

ALTER TABLE PVSS2COOL_FOLDERS_R2 ADD (
 	CONSTRAINT PVSS2COOL_FOLDERS_R2_PK PRIMARY KEY (FOLDERID),
	UNIQUE (FOLDERNAME)
);

INSERT INTO PVSS2COOL_FOLDERS_R2
  select * from pvss2cool_folders
  where foldername in ('/LUCID/DCS/LV');

update PVSS2COOL_FOLDERS_R2
	set status = 0, statusreq = 1, lastupdatebefore = null, lastupdateafter = null
	where folderid is not null;

-- Create sequence for folder IDs
CREATE SEQUENCE PVSS2COOL_FOLDERS_R2_SEQ NOCACHE
       START WITH 40; -- insert here a number more than max FOLDERID in PVSS2COOL_FOLDERS -- ;

update PVSS2COOL_FIELDS
	set newfieldname = fieldname
	where newfieldname is null
	and folderid in
		(select folderid from PVSS2COOL_FOLDERS_R2
			where foldername is not null)
	and fieldname is not null;		-- condition important for collection not created in R1 DB
update PVSS2COOL_FIELDS
	set fieldname = null
	where newfieldname is not null
	and folderid in
		(select folderid from PVSS2COOL_FOLDERS_R2
			where foldername is not null);

ALTER TABLE PVSS2COOL_FIELDS DROP
	CONSTRAINT PVSS2COOL_FIELDS_FID_FK;

update PVSS2COOL_LISTS
	set newdpname = dpname
	where folderid in
		(select folderid from PVSS2COOL_FOLDERS_R2
			where foldername is not null)
	  and dpname is not null;		-- also important for folders not created in COMP200

ALTER TABLE PVSS2COOL_LISTS DROP
	CONSTRAINT PVSS2COOL_LISTS_FID_FK;

commit;


