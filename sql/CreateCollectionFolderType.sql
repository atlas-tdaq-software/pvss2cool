-- Exercises of manual creating a collection folder(s)

alter table pvss2cool_fields
add CHNAME VARCHAR2(128);

select * from pvss2cool_fields;
select * from pvss2cool_folders;
select * from pvss2cool_lists;
-- Creating folder definition
insert into pvss2cool_folders (folderid, foldername, foldergroup, grouptable, dptinfo, uselist, dppattern, channelidtype, mechanism, updateinterval, statusreq, status, listchange)
                        values(11,      '/O11/DCS/COLL2/CHID',      0,'EVENT',      4,       1,      '*',             7,        -1,              1,         1,      0,         0);

update pvss2cool_folders set statusreq=1, status = 0 where folderid = 11;
-- Creating field definitions
update pvss2cool_fields set chname='*' where chname is null;
alter table pvss2cool_fields drop constraint pvss2cool_fields_pk;
alter table pvss2cool_fields add constraint pvss2cool_fields_pk PRIMARY KEY (folderid, position,chname);
-- for not existing folder
insert into pvss2cool_fields (folderid, position, DPELEMENTNAME,                newfieldname, coolfieldtype,pvssfieldtype,isid,isdyn,status,deadband,timeout,chname)
                      values (      11,        1, 'KHP2CTEST:APItest_DP1.value', 'dp1_value',       'float',     'NUMBER',   0,    0,     1,       0,      0,'22');
insert into pvss2cool_fields (folderid, position, DPELEMENTNAME,                newfieldname, coolfieldtype,pvssfieldtype,isid,isdyn,status,deadband,timeout,chname)
                      values (      11,        2, 'KHP2CTEST:APItest_DP2.value', 'dp2_value',       'float',     'NUMBER',   0,    0,     1,       0,      0,'22');
insert into pvss2cool_fields (folderid, position, DPELEMENTNAME,                newfieldname, coolfieldtype,pvssfieldtype,isid,isdyn,status,deadband,timeout,chname)
                      values (      11,        3, 'KHP2CTEST:APItest_DP3.value', 'dp3_value',       'float',     'NUMBER',   0,    0,     1,       0,      0,'22');
-- changes for existing folder
update pvss2cool_fields set fieldname=newfieldname,
                            status=0, newfieldname = null
                        where newfieldname is not null and folderid = 11;
update pvss2cool_fields set newfieldname=null where newfieldname is not null;

update pvss2cool_fields set CHNAME = '22' where folderid = 11 and CHNAME = 'col2Ch2';
-- creating list definition
insert into pvss2cool_lists (folderid, position, NEWDPNAME,     channelname, status)
                      values(      11,        1, '-',             'col2Ch1',         1);
update pvss2cool_lists set DPNAME='-' where folderid=10 and position=2;
insert into pvss2cool_lists (folderid, position, DPNAME, channelid, channelname, status)
                      values(      11,        1, '-',              21,     '21',         0);
update pvss2cool_lists set NEWDPNAME=null where folderid=10 and position=2;

update pvss2cool_lists set channelid = 22, channelname = '22' where folderid = 11 and channelname = 'col2Ch2';
update pvss2cool_lists set channelname = '21' where folderid = 11 and channelid = 21;
update pvss2cool_folders set uselist = 1, dppattern = '*' where folderid = 11;
update pvss2cool_folders set channelidtype = 7 where folderid = 11;
delete from pvss2cool_lists  where folderid = 11 and channelid = 22;
delete from pvss2cool_fields where folderid=11 and chname = '22';
