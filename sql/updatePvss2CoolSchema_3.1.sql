define USER = ATLAS_PVSSZDC -- name of the owner account
define VERSION = 3.1        -- schema version

-- Alter Fields table
ALTER TABLE PVSS2COOL_FIELDS ADD (
  DEADBAND       FLOAT,
  TIMEOUT        NUMBER(10)
);

-- Insert version information
UPDATE PVSS2COOL_DB_ATTRIBUTES
SET DB_ATTRIBUTE_VALUE = '&VERSION'
WHERE DB_ATTRIBUTE_NAME = 'SCHEMA_VERSION';
