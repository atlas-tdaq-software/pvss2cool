define USER = ATLAS_PVSSZDC -- name of the owner account
select 'CREATE SYNONYM &USER._W.' || object_name || ' FOR &USER..'|| object_name ||';'
  from ALL_OBJECTS
  where object_type in ('TABLE','VIEW','SEQUENCE','TYPE','PROCEDURE','PACKAGE','FUNCTION')
    and owner = '&USER'
    and object_name like 'PVSS2COOL%';

commit;

-- select * from PVSS2COOL_DB_ATTRIBUTES;
