-- DON'T FORGET EDIT the list of folders with MILLISECOND time precision
-- Script upgardes the p2c configuration schema to version 5.0 corresponding
-- to schema extension of LS1 (2013)
-- Written by SK, 01.10.13

define VERSION = 5.0        -- schema version

-- Extension of the PVSS2COOL_FOLDERS table
alter table pvss2cool_folders add (
	ALIASDPE       VARCHAR2(256),
	TIMEPRECISION  VARCHAR(64)
);

update pvss2cool_folders set TIMEPRECISION='Millisecond'
	where FOLDERNAME in ('');
update pvss2cool_folders set TIMEPRECISION='Second'
	where TIMEPRECISION is null;

-- Insert CHNAME column to PVSS2COOL_FIELDS table
alter table pvss2cool_fields
add CHNAME VARCHAR2(128); 
-- fill in CHNAME column
update pvss2cool_fields set CHNAME='*' where chname is null;
alter table pvss2cool_fields drop constraint pvss2cool_fields_pk keep index;
drop index pvss2cool_fields_pk; 
alter table pvss2cool_fields 
	add constraint pvss2cool_fields_pk PRIMARY KEY (folderid, position, chname);

-- Insert version information
UPDATE PVSS2COOL_DB_ATTRIBUTES
SET DB_ATTRIBUTE_VALUE = '&VERSION'
WHERE DB_ATTRIBUTE_NAME = 'SCHEMA_VERSION';

commit;
