alter table PVSS2COOL_folders_R2 modify FOLDERGROUP default 0;                  -- three "alter..." necessary for every schema
alter table PVSS2COOL_lists drop constraint PVSS2COOL_LISTS_FID_FK;             -- where a new folder is to be defined
alter table PVSS2COOL_fields drop constraint PVSS2COOL_FIELDS_FID_FK;

select * from all_constraints where owner = 'ATLAS_PVSSDCS' and constraint_name = 'PVSS2COOL_LISTS_FID_FK';

