--////////////////////////////////////////////////////////////////////////
-- Script copies the definition of a certain folder of Run1 to
-- the pvss2cool_folders_R2 table for Run2.
-- Must be followed by physycal creating that folder in new COOL DB
-- by 'pvss2cool configurePvssCool -d NEW_DB_NAME'
--
-- Before using, fill in the folder name at the end of 'insert' statement
--////////////////////////////////////////////////////////////////////////
--'MDT/DCS/PSV0SETPOINTS','MDT/DCS/PSV1SETPOINTS','MDT/DCS/PSLVCHSTATE','MDT/DCS/PSHVMLSTATE'
insert into pvss2cool_folders_R2
	(folderid,foldername,foldergroup,grouptable,dptypename, dpsubtypename, dptinfo,
	 usealias,uselist,dppattern,channelidtype,channelidinfo,listchange,aliasdpe,
	 timeprecision,intervalcount,updateinterval,mechanism)
	select folderid,foldername,foldergroup,grouptable,dptypename, dpsubtypename, dptinfo,
	 usealias,uselist,dppattern,channelidtype,channelidinfo,listchange,aliasdpe,
	 timeprecision,intervalcount,updateinterval,mechanism
	 from pvss2cool_folders where foldername = '/LUCID/DCS/LV';
update pvss2cool_fields
	set newfieldname = fieldname
	where folderid in
		(select folderid from pvss2cool_folders_R2
			where status is null)
	  and fieldname is not null;		-- condition important for collection not created in R1 DB
update pvss2cool_fields
	set fieldname = null
	where folderid in
		(select folderid from pvss2cool_folders_R2
			where status is null);
update pvss2cool_lists
	set newdpname = dpname
	where folderid in
		(select folderid from pvss2cool_folders_R2
			where status is null)
	  and dpname is not null;		-- also important for folders not created in COMP200

update pvss2cool_folders_R2
	set status = 0, statusreq = 1
	where status is null;
commit;
