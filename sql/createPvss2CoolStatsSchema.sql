define USER = ATLAS_PVSS2COOL -- name of the owner account

-- Create statistics table
CREATE TABLE PVSS2COOL_STATISTICS (
	FOLDERNAME            VARCHAR2(255) NOT NULL,
	IS_RUNNING            NUMBER(1),
	TOTAL_HOURS           FLOAT,
	LAST_UPDATE_TIME      TIMESTAMP(0),
	AVG_IOVS_PER_HOUR     NUMBER(10),
	MIN_IOVS_PER_HOUR     NUMBER(10),
	MAX_IOVS_PER_HOUR     NUMBER(10),
	AVG_REDTIME_FOR_HOUR  FLOAT,
	MIN_REDTIME_FOR_HOUR  FLOAT,
	MAX_REDTIME_FOR_HOUR  FLOAT,
	AVG_INSTIME_FOR_HOUR  FLOAT,
	MIN_INSTIME_FOR_HOUR  FLOAT,
	MAX_INSTIME_FOR_HOUR  FLOAT);

ALTER TABLE PVSS2COOL_STATISTICS ADD (
	CONSTRAINT PVSS2COOL_STATISTICS_PK PRIMARY KEY (FOLDERNAME));
	
-- Create Error Log table
CREATE TABLE PVSS2COOL_ERRORS (
	FOLDERNAME VARCHAR2(255) NOT NULL,
	TS         TIMESTAMP(3) NOT NULL,
	ERROR_TEXT VARCHAR2(4000)
);

ALTER TABLE PVSS2COOL_ERRORS ADD (
	CONSTRAINT PVSS2COOL_ERRORS_PK PRIMARY KEY (FOLDERNAME, TS)
);

-- Output grant statements required
select 'GRANT SELECT, INSERT, UPDATE, DELETE ON ' || object_name || ' TO &USER._W;'
  from ALL_OBJECTS
  where object_type in ('TABLE','VIEW')
    and owner = '&USER'
    and object_name like 'PVSS2COOL%'
union
select 'GRANT SELECT ON ' || object_name || ' TO &USER._W;'
  from ALL_OBJECTS
  where object_type in ('SEQUENCE')
    and owner = '&USER'
    and object_name like 'PVSS2COOL%'
union
select 'GRANT EXECUTE ON ' || object_name || ' TO &USER._W;'
  from ALL_OBJECTS
  where object_type in ('TYPE','PROCEDURE','PACKAGE','FUNCTION')
    and owner = '&USER'
    and object_name like 'PVSS2COOL%';
