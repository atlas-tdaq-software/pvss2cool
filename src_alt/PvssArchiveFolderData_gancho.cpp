// Include files
#include "CoralBase/Exception.h"
#include "CoolApplication/Application.h"
#include "RelationalAccess/IAuthenticationService.h"
#include "RelationalAccess/IAuthenticationCredentials.h"
#include "RelationalAccess/IConnectionService.h"
#include "RelationalAccess/ISessionProxy.h"
#include "RelationalAccess/IQuery.h"
#include "RelationalAccess/IRelationalService.h"
#include "RelationalAccess/IRelationalDomain.h"
#include "RelationalAccess/ITransaction.h"
#include "RelationalAccess/ISchema.h"
#include "RelationalAccess/ICursor.h"
// Commented moved to private headers 14.10.10 by SK
//#include "CoralBase/Attribute.h"
//#include "CoralBase/AttributeList.h"
//#include "CoralBase/TimeStamp.h"

#include "CoolKernel/ValidityKey.h"
#include "CoolKernel/types.h"
#include "CoolKernel/ChannelId.h"

#include <iostream>
#include <exception>

// Application Specific Include Files
// ----------------------------------
#include "globals.h"
#include "PvssArchiveNumber.h"
#include "PvssArchiveUtilities.h"
#include "CondDBUtilities.h"
#include "PvssArchiveFolderConfig.h"
#include "PvssCoolDefaultConnections.h"
#include "PvssArchiveFolderData.h"

//-----------------------------------------------------------------------------

	
PvssArchiveFolderData::PvssArchiveFolderData()
: m_session(0),
  m_query(0),
  m_cursor(0)
{
  return;
}

PvssArchiveFolderData::~PvssArchiveFolderData()
{
  return;
}

bool PvssArchiveFolderData::connect(coral::IConnectionService& connectionService,
                                    std::string serviceName)
{
  bool status = true;

  try {

    this->m_session = connectionService.connect(serviceName, coral::ReadOnly);

  }

  //==================
  // CATCH EXCEPTIONS
  //==================
  catch ( coral::Exception& e )
  {
    std::cerr << "CORAL exception : " << e.what() << std::endl;
    status = false;
  }
  catch ( std::exception& e )
  {
    std::cerr << "Standard C++ exception : " << e.what() << std::endl;
    status = false;
  }
  catch( ... )
  {
    std::cerr << "Unknown exception ..." << std::endl;
    status = false;
  }

  return (status);
}

bool PvssArchiveFolderData::disconnect()
{
  bool status = false;

  // Disconnecting
  if (this->m_session)
    delete this->m_session;

  return (status);
}

void PvssArchiveFolderData::cleanUp()
{
  // Delete the query to free up resources
  if (this->m_query) {
    delete this->m_query;
    this->m_query = 0;
  }
  this->m_session->transaction().commit();

  return;
}

coral::ICursor& PvssArchiveFolderData::getFolderData(FolderConfigurationInformation& folderConfig,
                                                     std::vector<FolderFieldConfiguration>& folderFields,
                                                     PvssArchiveNumber& archiveNumber,
                                                     int rowCacheSize,
                                                     double &readTime,
													 bool milliPrecision)
{

  char cpTableName[50];
  char cpPosition[10];

  std::size_t iPos;

  std::string idElementDpe;
  std::string outputString;
  std::string pattern;
  std::string bindName;
  std::string condition;
  std::string timeStringLength;
  std::string folderDataCondition;
  std::string dpSeparator;

  coral::TimeStamp tStart;
  coral::TimeStamp tEnd;

  try {

    // Set some basic information
    readTime = 0.0;
    if (folderConfig.dpTypeInfo() == FW_CONDDB_DPTINFO_FW)
      dpSeparator = "/";
    else
      dpSeparator = ".";

    // Get start and end times in required format
    std::string fromTime = archiveNumber.startTime();
    std::string toTime = archiveNumber.endTime();

    // Get time now
    tStart = CondDBUtilities::getTimeNow();
    
    // Create the query and subqueries to extract the actual data
    this->m_session->transaction().start(true);
    coral::ISchema& mySchema = this->m_session->nominalSchema();
    this->m_query = mySchema.newQuery();
    coral::IQueryDefinition& folderData = this->m_query->defineSubQuery("FOLDER_DATA");
    coral::IQueryDefinition& valueData = folderData.defineSubQuery("VALUE_DATA");
    coral::IQueryDefinition& dataElements = folderData.defineSubQuery("DATA_ELEMENTS");
    coral::IQueryDefinition& folderInfo = dataElements.defineSubQuery("FOLDER_INFO");
    coral::IQueryDefinition& stateInfo = dataElements.defineSubQuery("STATE_INFO");
    coral::IQueryDefinition& statusInfo = stateInfo.applySetOperation(coral::IQueryDefinition::Union);
    coral::IQueryDefinition& listInfo = folderInfo.defineSubQuery("LIST_INFO");

    // Set the lowest level subquery (only required if a list is being used, but not for FSM type)
    if (folderConfig.usesList() && (folderConfig.dpTypeInfo() != FW_CONDDB_DPTINFO_FSM)) {
      std::string elementName;
      elementName = "/*+ LEADING(L) */ L.DPNAME || NVL2(F.DPELEMENTNAME, '" + dpSeparator + "' || F.DPELEMENTNAME, '')";
      folderInfo.addToOutputList(elementName, "ELEMENT_NAME");
      folderInfo.addToOutputList("F.POSITION", "POSITION");

      if ((folderConfig.channelIdType() == FW_CONDDB_IDINFO_ALIAS) ||
          (folderConfig.channelIdType() == FW_CONDDB_IDINFO_ALIAS_STRING)) {
        folderInfo.addToOutputList("L.CHANNEL_ID", "CHANNEL_ID");
        if (folderConfig.channelIdType() == FW_CONDDB_IDINFO_ALIAS)
          listInfo.addToOutputList("TO_NUMBER(EL.ALIAS)", "CHANNEL_ID");
        else
          listInfo.addToOutputList("REGEXP_REPLACE(EL.ALIAS, '[^([:alpha:],[:digit:],_)]', '_')", "CHANNEL_ID");
        listInfo.addToOutputList("L.FOLDERID", "FOLDERID");
        listInfo.addToOutputList("L.DPNAME", "DPNAME");
        listInfo.addToTableList("ELEMENTS", "EL");
        listInfo.addToTableList(PVSS_COOL_FOLDER_LIST_CONFIGURATION_TABLE, "L");
        listInfo.addToTableList("DPT");
        listInfo.addToTableList(PVSS_COOL_FOLDER_CONFIGURATION_TABLE, "F");

        coral::AttributeList listBindList;
        listBindList.extend("folderId", "long long");
        listBindList["folderId"].setValue(folderConfig.folderId());
        listBindList.extend("dpSeparator", "string");
        listBindList["dpSeparator"].setValue(dpSeparator);
        listBindList.extend("channelIdElement", "string");
        listBindList["channelIdElement"].setValue(folderConfig.channelIdInfo());
        listInfo.setCondition("L.FOLDERID = :folderId AND F.FOLDERID = :folderId AND F.FOLDERID = L.FOLDERID AND EL.EVENT = 1 AND EL.ALIAS IS NOT NULL AND EL.ELEMENT_NAME = L.DPNAME || :dpSeparator || :channelIdElement AND DPT.DPTNAME = F.DPTYPENAME AND EL.SYS_ID = DPT.SYS_ID AND EL.DPT_ID = DPT.DPT_ID", listBindList);
        folderInfo.addToTableList("LIST_INFO", "L");
      } else {
        folderInfo.addToTableList(PVSS_COOL_FOLDER_LIST_CONFIGURATION_TABLE, "L");
        if (folderConfig.channelIdType() == FW_CONDDB_IDINFO_USER_DEFINED) {
          folderInfo.addToOutputList("TO_NUMBER(L.CHANNELID)", "CHANNEL_ID");
        } else if (folderConfig.channelIdType() == FW_CONDDB_IDINFO_USER_DEFINED_STRING) {
          folderInfo.addToOutputList("REGEXP_REPLACE(L.CHANNELNAME, '[^([:alpha:],[:digit:],_)]', '_')", "CHANNEL_ID");
        } else if (folderConfig.channelIdType() == FW_CONDDB_IDINFO_DPNAME) { // Use DP name for channel ID as string
          folderInfo.addToOutputList("REGEXP_REPLACE(L.DPNAME, '[^([:alpha:],[:digit:],_)]', '_')", "CHANNEL_ID");
        }
      }

      folderInfo.addToTableList(PVSS_COOL_FOLDER_FIELD_CONFIGURATION_TABLE, "F");
      coral::AttributeList folderList;
      folderList.extend("folderId", "long long");
      folderList["folderId"].setValue(folderConfig.folderId());
      folderInfo.setCondition("F.FOLDERID = :folderId AND L.FOLDERID = :folderId AND L.FOLDERID = F.FOLDERID", folderList);

      dataElements.addToOutputList("FI.CHANNEL_ID", "CHANNEL_ID");
    }

    // Now set up the query to get the actual element IDs
    dataElements.addToOutputList("EL.ELEMENT_ID", "EID");
    dataElements.addToOutputList("FI.POSITION", "FIELD_POSITION");
    dataElements.addToTableList("ELEMENTS", "EL");

    // Check if DP definitions use pattern or specified list
    if (folderConfig.usesList() && (folderConfig.dpTypeInfo() != FW_CONDDB_DPTINFO_FSM)) {

      // Get the data elements for the list
      dataElements.addToTableList("FOLDER_INFO", "FI");
      dataElements.setCondition("FI.ELEMENT_NAME = EL.ELEMENT_NAME", coral::AttributeList());

    } else if (folderConfig.dpTypeInfo() == FW_CONDDB_DPTINFO_FSM) { 	// FSM folder type, which is a special case
      stateInfo.addToTableList(PVSS_COOL_FOLDER_LIST_CONFIGURATION_TABLE, "L");
      statusInfo.addToTableList(PVSS_COOL_FOLDER_LIST_CONFIGURATION_TABLE, "L");
      if (folderConfig.channelIdType() == FW_CONDDB_IDINFO_USER_DEFINED) {
        stateInfo.addToOutputList("TO_NUMBER(L.CHANNELID)", "CHANNEL_ID");
        statusInfo.addToOutputList("TO_NUMBER(L.CHANNELID)", "CHANNEL_ID");
      } else {
        stateInfo.addToOutputList("REGEXP_REPLACE(L.CHANNELNAME, '[^([:alpha:],[:digit:],_)]', '_')", "CHANNEL_ID");
        statusInfo.addToOutputList("REGEXP_REPLACE(L.CHANNELNAME, '[^([:alpha:],[:digit:],_)]', '_')", "CHANNEL_ID");
      }
      stateInfo.addToOutputList("REGEXP_REPLACE(L.DPNAME, '\\|STATUS_', '|') || '.fsm.currentState'", "ELEMENT_NAME");
      stateInfo.addToOutputList("1", "POSITION");
      statusInfo.addToOutputList("L.DPNAME || '.fsm.currentState'", "ELEMENT_NAME");
      statusInfo.addToOutputList("2", "POSITION");
      coral::AttributeList bindList;
      bindList.extend("folderId", "long long");
      bindList["folderId"].setValue(folderConfig.folderId());
      stateInfo.setCondition("L.FOLDERID = :folderId", bindList);

      // Although the condition is exactly the same for the 2 queries being union-ed, using the
      // same AttributeList causes a segmentation fault, therefore need to declare another one
      coral::AttributeList bindList2;
      bindList2.extend("folderId", "long long");
      bindList2["folderId"].setValue(folderConfig.folderId());
      statusInfo.setCondition("L.FOLDERID = :folderId", bindList2);

      dataElements.addToTableList("STATE_INFO", "FI");
      dataElements.addToOutputList("FI.CHANNEL_ID", "CHANNEL_ID");
      dataElements.setCondition("FI.ELEMENT_NAME = EL.ELEMENT_NAME", coral::AttributeList());
    } else {  // This folder uses a pattern

      dataElements.addToTableList(PVSS_COOL_FOLDER_FIELD_CONFIGURATION_TABLE, "FI");

      // Use DP name for channel ID as string
      if (folderConfig.channelIdType() == FW_CONDDB_IDINFO_DPNAME) {
        dataElements.addToOutputList("REGEXP_REPLACE(DP.DPNAME, '[^([:alpha:],[:digit:],_)]', '_')", "CHANNEL_ID");
        dataElements.addToTableList("DPE");
        dataElements.addToTableList("DPT");
        dataElements.addToTableList("DP");
        coral::AttributeList bindList;
        bindList.extend("folderId", "long long");
        bindList["folderId"].setValue(folderConfig.folderId());
        bindList.extend("dptName", "string");
        bindList["dptName"].setValue(folderConfig.dpTypeName());
        condition = std::string("FI.FOLDERID = :folderId AND FI.DPELEMENTNAME = DPE.DPENAME AND EL.SYS_ID = DPE.SYS_ID AND EL.DPE_ID = DPE.DPE_ID AND EL.DP_ID = DPE.DP_ID AND EL.SYS_ID = DPT.SYS_ID AND EL.DPT_ID = DPT.DPT_ID AND EL.DP_ID = DP.DP_ID AND EL.SYS_ID = DP.SYS_ID AND DPT.DPTNAME = :dptName");

        // Check if the pattern is a single '*' (which means it is pointless to use it)
        pattern = folderConfig.pattern();
        if ((pattern.length() == 0) || ((pattern.length() == 1) && (pattern.compare("*") == 0))) {
        } else {
          bindList.extend("pattern", "string");
          iPos = pattern.find_first_of("*");
          while (iPos != std::string::npos) {
            pattern[iPos] = '%';
            iPos = pattern.find_first_of("*");
          }
          bindList["pattern"].setValue(pattern);
          condition += std::string(" AND DP.DPNAME LIKE :pattern");
        }
        dataElements.setCondition(condition, bindList);
      } else { // Must be using an alias as channel ID (as this is the only other choice with specifying a pattern)
        folderInfo.addToOutputList("DP.DPNAME");
        folderInfo.addToOutputList("F.FOLDERID");
        if (folderConfig.channelIdType() == FW_CONDDB_IDINFO_ALIAS)
          folderInfo.addToOutputList("TO_NUMBER(EL.ALIAS)", "CHANNEL_ID");
        else if (folderConfig.channelIdType() == FW_CONDDB_IDINFO_ALIAS_STRING)
          folderInfo.addToOutputList("REGEXP_REPLACE(EL.ALIAS, '[^([:alpha:],[:digit:],_)]', '_')", "CHANNEL_ID");
        folderInfo.addToTableList("DPE");
        folderInfo.addToTableList("DPT");
        folderInfo.addToTableList("DP");
        folderInfo.addToTableList("ELEMENTS", "EL");
        folderInfo.addToTableList(PVSS_COOL_FOLDER_CONFIGURATION_TABLE, "F");
        coral::AttributeList bindList;
        bindList.extend("folderId", "long long");
        bindList["folderId"].setValue(folderConfig.folderId());
        bindList.extend("dptName", "string");
        bindList["dptName"].setValue(folderConfig.dpTypeName());
        condition = std::string("F.FOLDERID = :folderId AND DPE.DPENAME = F.CHANNELIDINFO AND EL.SYS_ID = DPE.SYS_ID AND EL.DPE_ID = DPE.DPE_ID AND EL.DP_ID = DPE.DP_ID AND EL.SYS_ID = DPT.SYS_ID AND EL.DPT_ID = DPT.DPT_ID AND EL.DP_ID = DP.DP_ID AND EL.SYS_ID = DP.SYS_ID AND EL.EVENT = 1 AND EL.ALIAS IS NOT NULL AND DPT.DPTNAME = :dptName");

        // Check if the pattern is a single '*' (which means it is pointless to use it)
        pattern = folderConfig.pattern();
        if ((pattern.length() == 0) || ((pattern.length() == 1) && (pattern.compare("*") == 0))) {
        } else {
          bindList.extend("pattern", "string");
          iPos = pattern.find_first_of("*");
          while (iPos != std::string::npos) {
            pattern[iPos] = '%';
            iPos = pattern.find_first_of("*");
          }
          bindList["pattern"].setValue(pattern);
          condition += std::string(" AND DP.DPNAME LIKE :pattern");
        }
        folderInfo.setCondition(condition, bindList);

        // Set up the rest of the data elements query for pattern with ID as alias
        dataElements.addToOutputList("L.CHANNEL_ID", "CHANNEL_ID");
        dataElements.addToTableList("FOLDER_INFO", "L");
        coral::AttributeList folderList;
        folderList.extend("folderId", "long long");
        folderList["folderId"].setValue(folderConfig.folderId());
        dataElements.setCondition("FI.FOLDERID = :folderId AND L.FOLDERID = :folderId AND L.FOLDERID = FI.FOLDERID AND EL.ELEMENT_NAME = L.DPNAME || NVL2(FI.DPELEMENTNAME, '.' || FI.DPELEMENTNAME, '')", folderList);
      }
    }

    // Again for ALL folder types:
	
	// Set the data from EVENTHISTORY table
    valueData.addToOutputList("EH.ELEMENT_ID", "ELEMENT_ID");
    valueData.addToOutputList("EH.TS", "TS");
    valueData.addToOutputList("ROUND((TO_DATE(TO_CHAR(EH.TS, 'yyyymmddhh24miss'), 'yyyymmddhh24miss') - TO_DATE('19700101000000', 'yyyymmddhh24miss')) * 86400)", "SECS"); 	// !!!

// New additional code 21.01.10 (SK), 
	if((folderConfig.dpTypeInfo() == FW_CONDDB_DPTINFO_FSM)	// FSM folder
	|| milliPrecision)			{							// or milliPrecision specified explicitly
		valueData.addToOutputList("(TO_CHAR(EH.TS, 'yyyymmddhh24missff3') - (TO_CHAR(EH.TS, 'yyyymmddhh24miss') * 1000))", "MILLI"); 	// !!!
	}
// End of new code
	
    valueData.addToOutputList("EH.VALUE_NUMBER", "VALUE_NUMBER");
    valueData.addToOutputList("EH.VALUE_STRING", "VALUE_STRING");
    valueData.addToOutputList("EH.VALUE_TIMESTAMP", "VALUE_TIMESTAMP");
    sprintf(cpTableName, "%sHISTORY_%08ld", folderConfig.group().data(), archiveNumber.archiveNumber());
    valueData.addToTableList(cpTableName, "EH");

    // Set the main query
// Was in original code
//    folderData.addToOutputList("/*+ LEADING(DE EHV) USE_NL(DE EHV) */ CHANNEL_ID");
// New hint suggested by Gancho
//    folderData.addToOutputList("/*+ INDEX_RS_ACS("EHV"(ELEMENT_ID,TS)) NO_IDEX_SS(("EHV"(ELEMENT_ID,TS))) LEADING("DE" "EHV") USE_NL("DE" "EHV") */ CHANNEL_ID");
    folderData.addToOutputList("/*+ INDEX_RS_ASC(EHV(ELEMENT_ID,TS)) NO_INDEX_SS((EHV(ELEMENT_ID,TS))) LEADING(DE EHV) USE_NL(DE EHV) */ CHANNEL_ID");
// Was in original code
//    folderData.addToOutputList("(EHV.SECS * 1000)", "MILLISECS");
// New code 21.01.10 (SK)
	if((folderConfig.dpTypeInfo() == FW_CONDDB_DPTINFO_FSM)	// FSM folder
	|| milliPrecision)			{							// or milliPrecision specified explicitly
//    	std::cout << "Setting TS with msecs" << std::endl;
		folderData.addToOutputList("((EHV.SECS * 1000) + EHV.MILLI)", "MILLISECS");
	}
	else {
//		std::cout << "Setting TS WITHOUT msecs" << std::endl;
	    folderData.addToOutputList("(EHV.SECS * 1000)", "MILLISECS");
	}
// End of new code

// Was in original code
//    folderData.addToOutputList("TO_CHAR(EHV.TS, 'yyyymmddhh24miss')", "S_TIME");  // !!!

// New code 25.01.10 (SK) - Replaced 26.11.10, see below
//	if((folderConfig.dpTypeInfo() == FW_CONDDB_DPTINFO_FSM)	// FSM folder
//	|| milliPrecision)			{							// or milliPrecision specified explicitly
//	  folderData.addToOutputList("TO_CHAR(EHV.TS, 'yyyymmddhh24missff3')", "S_TIME");  // !!!
//	}
//	else
//    folderData.addToOutputList("TO_CHAR(EHV.TS, 'yyyymmddhh24miss')", "S_TIME"); 
// End of new code

// New code 26.11.10 (SK)
	folderData.addToOutputList("TO_CHAR(EHV.TS, 'yyyymmddhh24miss')", "S_TIME"); 
	folderData.addToOutputList("TO_CHAR(EHV.TS, 'yyyymmddhh24missff3')", "LONG_TIME");  
// End of new code

    for (std::vector<FolderFieldConfiguration>::iterator iIter1 = folderFields.begin(); iIter1 != folderFields.end(); iIter1++) {

      if (iIter1->pvssFieldType().compare("NUMBER") == 0) {
        outputString = "TO_NUMBER(";										// !! NEEDED ???
      } else if (iIter1->pvssFieldType().compare("TIMESTAMP") == 0) {				//		Removing of this TO_NUMBER
        outputString = "TO_CHAR(";													//		works well for good numbers
      } else {																		//		but still does NOT work for NaN
        outputString.erase();														//		because of TO_NUMBER in the first
      }																				//		select, which is NECESSARY for 
      sprintf(cpPosition, "%ld", iIter1->position());								//		sorting by TS within 1 sec
      outputString += "DECODE(DE.FIELD_POSITION, ";									//
      outputString += cpPosition;													//
      outputString += std::string(", EHV.VALUE_");									//
      outputString += iIter1->pvssFieldType();										//
      outputString += std::string(", NULL)");										//
																					//
      if (iIter1->pvssFieldType().compare("NUMBER") == 0) {							// 
        outputString += ")";												// Remove these together with upper ")"
      } else if (iIter1->pvssFieldType().compare("TIMESTAMP") == 0) {
        outputString += ", 'yyyymmddhh24missff3')";
      }

      folderData.addToOutputList(outputString, iIter1->fieldName());
    }

    folderData.addToTableList("VALUE_DATA", "EHV");
    folderData.addToTableList("DATA_ELEMENTS", "DE");

    coral::AttributeList timeBindList;
    timeBindList.extend("fromTime", "string");
    timeBindList.extend("toTime", "string");
    timeBindList["fromTime"].setValue(fromTime);
    timeBindList["toTime"].setValue(toTime);
    folderDataCondition = "EHV.TS >= TO_DATE(:fromTime, 'dd-mm-yyyy hh24:mi:ss') AND EHV.TS < TO_DATE(:toTime, 'dd-mm-yyyy hh24:mi:ss') AND EHV.ELEMENT_ID = DE.EID";
    folderData.setCondition(folderDataCondition, timeBindList);

    coral::AttributeList outputList;
    this->m_query->setDistinct();
    if ((folderConfig.channelIdType() == FW_CONDDB_IDINFO_ALIAS) || (folderConfig.channelIdType() == FW_CONDDB_IDINFO_USER_DEFINED)) {
      this->m_query->addToOutputList("CHANNEL_ID");
      outputList.extend<cool::ChannelId>("CHANNEL_ID");
    } else {
      this->m_query->addToOutputList("CHANNEL_ID");
      outputList.extend<std::string>("CHANNEL_ID");
    }
    this->m_query->addToOutputList("MILLISECS");
    outputList.extend<cool::ValidityKey>("MILLISECS");

    for (std::vector<FolderFieldConfiguration>::iterator iIter1 = folderFields.begin(); iIter1 != folderFields.end(); iIter1++) {

      if (iIter1->pvssFieldType().compare("NUMBER") == 0) {
        outputString = "TO_NUMBER(";
        outputList.extend(iIter1->fieldName(), iIter1->coolFieldType());
      } else if (iIter1->pvssFieldType().compare("TIMESTAMP") == 0) {
        outputString = "TO_CHAR(";
        outputList.extend(iIter1->fieldName(), "string");
      } else {
        outputString.erase();
        outputList.extend(iIter1->fieldName(), "string");
      }
      outputString += "SUBSTR(MAX(DECODE(FD.";
      outputString += iIter1->fieldName();
// Was in original code
//      outputString += std::string(", NULL, NULL, FD.S_TIME || FD.");
// New code 26.11.10 (SK)
      outputString += std::string(", NULL, NULL, FD.LONG_TIME || FD.");
// End of new code

      outputString += iIter1->fieldName();
// Was in original code
//      outputString += std::string(")) OVER (PARTITION BY FD.CHANNEL_ID ORDER BY FD.S_TIME), 15)");  /// !!!

// New code 25.01.10 (SK) - Replaced 26.11.10, see below
//	if((folderConfig.dpTypeInfo() == FW_CONDDB_DPTINFO_FSM)	// FSM folder
//	|| milliPrecision)			{							// or milliPrecision specified explicitly
//		outputString += std::string(")) OVER (PARTITION BY FD.CHANNEL_ID ORDER BY FD.S_TIME), 18)"); 
//	}
//	else
//		outputString += std::string(")) OVER (PARTITION BY FD.CHANNEL_ID ORDER BY FD.S_TIME), 15)");  
// End of new code
	
// New code 26.11.10 (SK)
	if((folderConfig.dpTypeInfo() == FW_CONDDB_DPTINFO_FSM)	// FSM folder
	|| milliPrecision)			{							// or milliPrecision specified explicitly
		outputString += std::string(")) OVER (PARTITION BY FD.CHANNEL_ID ORDER BY FD.LONG_TIME), 18)"); 
	}
	else
        outputString += std::string(")) OVER (PARTITION BY FD.CHANNEL_ID ORDER BY FD.S_TIME), 18)");  
// End of new code
	
      if (iIter1->pvssFieldType().compare("NUMBER") == 0) {
        outputString += ")";
      } else if (iIter1->pvssFieldType().compare("TIMESTAMP") == 0) {
        outputString += ", 'yyyymmddhh24missff3')";
      }
      this->m_query->addToOutputList(outputString, iIter1->fieldName());
    }

    this->m_query->addToTableList("FOLDER_DATA", "FD");
    this->m_query->addToOrderList("CHANNEL_ID");
    this->m_query->addToOrderList("MILLISECS");
    this->m_query->defineOutput(outputList);
    this->m_query->setRowCacheSize(rowCacheSize);
	
    this->m_cursor = &(this->m_query->execute());

    tEnd = CondDBUtilities::getTimeNow();
    readTime = CondDBUtilities::getTimeDifference(tStart, tEnd);
  }

  //==================
  // CATCH EXCEPTIONS
  //==================
  catch ( coral::Exception& e )
  {
    std::cout << "CORAL exception : " << e.what() << std::endl;
    throw;
  }
  catch ( std::exception& e )
  {
    std::cout << "Standard C++ exception : " << e.what() << std::endl;
    throw;
  }
  catch( ... )
  {
    std::cout << "Unknown exception ..." << std::endl;
    throw;
  }

  return (*(this->m_cursor));
}
