#!/bin/sh
# To run several ($num) times pvss2cool runOnce for the detector $1
# for the time period $period
#
# Works only when the environment is set BEFOREHAND

num=100
period=7200

SUBDET=$1
INTERLOCK_DIR="/det/dcs/pvss2cool/bufferInterlock"
SERVICE_DIR="/localdisk/PvssToCool/runControl"

source /sw/tdaq/setup/setup_tdaq-11-02-01.sh >> /dev/null        # Since 19.01.24 

cd /localdisk/PvssToCool/
export CORAL_AUTH_PATH='/localdisk/PvssToCool/W_private'		# W_private since 26.02.10
export CORAL_DBLOOKUP_PATH="/localdisk/PvssToCool/dblookup/${SUBDET}"

#echo "Environment set"

if test -z "$1"
then
	echo "No sub-detector information given"
else
	count=0
	while test $count -lt $num
	do
		echo ""
		echo "Running pvss2cool $count for $SUBDET"
		pvss2cool runOnce -s ${SUBDET} -d CONDBR2 -t ${period} -i 
		count=`expr $count + 1`
		sleep 2
	done
fi
