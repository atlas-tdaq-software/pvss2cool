#!/bin/sh

# Extended 02.12.10 by check the execution time of running
# application and killing it, if that time is more than a 
# allowed maximum (set here by $MAXTIME)
#
# Upgraded to SLC6 - OA 3.11 23.09.13 by SK
#

# Determine the ATLAS run number
if [ -z "$1" ]; then
  echo "ATLAS run number R1/R2 must be the first parameter. R2 is valid also for Run 3"
  exit 1
else
  if [ $1 == "R1" ]; then
  	run="-R 1"
	cooldb="COMP200"
  elif [ $1 == "R2" ]; then
  	run=""
	cooldb="CONDBR2"
  else
  	echo "Incorrect ATLAS run number"
	exit 1
  fi
fi

shift

# Check that at least one argument has been given, assumed to be the subdetector TLA
if [ -z "$1" ];	then
  echo "No sub-detector information given"
  exit 1
else
  SUBDET=$1
  INTERLOCK_DIR="/det/dcs/pvss2cool/bufferInterlock"
  SERVICE_DIR="/localdisk/PvssToCool/runControl"
# Timeout reduced from 40 min to 25 06.10.17 by SK on decision of DCS
  MAXTIME=25
fi

shift

controlExecutionTime()
{
	execTime=0
	startTimeRead=0
	myPid=0
	toKill=0
	SUBDET=$1
	present=1
	if [ -z $2 ]; then
		group=""
		fgroup=""
	else
		group=$3
		fgroup="$2 $3"
	fi

	currentTime=`date +%M`
	if [ -s "${SERVICE_DIR}/startTime_${SUBDET}${group}.log" ]; then
		startTimeRead=`awk '{ print $1 }' ${SERVICE_DIR}/startTime_${SUBDET}${group}.log`
	else
		toStart=0
 		echo "${SERVICE_DIR}/startTime_${SUBDET}${group}.log does not exist - INTERNAL ERROR!"
		return
	fi

	execTime=`expr ${currentTime} - ${startTimeRead}`  #!!!
	
	if [ ${execTime} -lt 0 ]; then
		execTime=`expr 60 + ${execTime}`
	fi
	
	if [ ${execTime} -lt ${MAXTIME} ]; then
		toStart=0
	else
		ps -f | grep -v grep | grep "pvss2cool runOnce -s ${SUBDET} ${fgroup}" > ${SERVICE_DIR}/psResult_${SUBDET}.txt
		myPid=`awk '{ print $2 }' ${SERVICE_DIR}/psResult_${SUBDET}.txt`
		echo "Killing p2c for ${SUBDET} ${fgroup} - seems to be hanging"
		kill -9 $myPid
		while [ ${present} -gt 0 ];  do
			sleep 0.1
			present=`ps -f | grep -v grep | grep -c "pvss2cool runOnce -s ${SUBDET} ${fgroup}"`
		done
		toStart=1
	fi
}


# Setup foldergroup if not NULL				     # Since 23.09.09
if [ -z "$1" ]; then
  fgroup=""
  group=""
else
  fgroup="-g $1"
  group=$1
fi
  
startTime=0
toStart=1
  
if [ "$(ls -A $INTERLOCK_DIR/$SUBDET)" ]; then
  echo "====================================="
  echo "Pvss2Cool for ${SUBDET} is interlocked by PVSS buffering, active interlocks are" `ls -A $INTERLOCK_DIR/$SUBDET`
  echo "==> Skipping running this time at " `date`
  echo "====================================="
  toStart=0
 # Check if any processes of this name are running
else 
	myCount=`ps -ef | grep -v grep | grep -c "pvss2cool runOnce -s ${SUBDET} ${fgroup}"`
	if [ $myCount -gt 0 ];	then 
		controlExecutionTime $SUBDET ${fgroup}
		if [ $toStart -eq 0 ];	then	
			echo "====================================="
			echo "Pvss2Cool for ${SUBDET} ${fgroup} is already running at" `date`
			echo "==> Skipping running this time"
			echo "====================================="
		fi
	fi	
fi

if [ ${toStart} -eq 1 ];  then
	# Setup environment for CORAL/COOL
	echo ""
	source /sw/tdaq/setup/setup_tdaq-12-00-00.sh >> /dev/null        # Since 21.01.25 
#	source /sw/tdaq/setup/setup_tdaq-11-02-01.sh >> /dev/null        # Since 28.02.24 
#	source /sw/tdaq/setup/setup_tdaq-11-02-00.sh >> /dev/null        # Since 19.01.24 
#	source /sw/tdaq/setup/setup_tdaq-09-04-00.sh >> /dev/null        # Since 13.05.22 
#	source /sw/tdaq/setup/setup_tdaq-09-03-00.sh >> /dev/null        # Since 30.11.21 
#	source /sw/tdaq/setup/setup_tdaq-09-02-01.sh >> /dev/null        # Since 08.04.21 
#	source /sw/tdaq/setup/setup_tdaq-09-01-00.sh >> /dev/null        # Since 09.10.20 
#	source /sw/tdaq/setup/setup_tdaq-07-01-00.sh >> /dev/null        # Since 21.03.17 
#	source /sw/tdaq/setup/setup_tdaq-07-00-00.sh >> /dev/null        # Since 24.01.17 
#	source /sw/tdaq/setup/setup_tdaq-06-01-01.sh >> /dev/null        # Since 12.02.16 
#	source /sw/tdaq/setup/setup_tdaq-06-01-00.sh >> /dev/null        # Since 11.01.16 
#	source /sw/tdaq/setup/setup_tdaq-05-05-00.sh >> /dev/null        # Since 06.02.15 
#	source /sw/tdaq/setup/setup_tdaq-05-03-00.sh >> /dev/null        # Since 27.03.14 
#	source /sw/tdaq/setup/setup_tdaq-05-02-00.sh >> /dev/null        # Since 06.12.13 
#	source /sw/tdaq/setup/setup_tdaq-05-01-00.sh >> /dev/null        # Since 25.10.13 
#	source /sw/tdaq/setup/setup_tdaq-05-00-01.sh >> /dev/null        # Since 23.09.13 
#	source /sw/tdaq/setup/setup_tdaq-03-00-01.sh >> /dev/null        # Since 13.01.11 
#	source /sw/tdaq/setup/setup_tdaq-02-00-03.sh >> /dev/null        # Since 24.08.09 
    
	# Set specific CORAL paths
	cd /localdisk/PvssToCool/
	export CORAL_AUTH_PATH='/localdisk/PvssToCool/W_private'		# W_private since 26.02.10
	export CORAL_DBLOOKUP_PATH="/localdisk/PvssToCool/dblookup/${SUBDET}"
	
	# Run process
	date +%M > ${SERVICE_DIR}/startTime_${SUBDET}${group}.log
	echo "Running PVSS2COOL for ${SUBDET} ${fgroup} at " `date`
# Replace local pvss2cool by executable from tdaq release.	09.10.20 SK
#	./bin/pvss2cool runOnce -s ${SUBDET} ${fgroup} -d ${cooldb} -t 7200 -i ${run} >> /localdisk/PvssToCool/logs/pvss2cool.${SUBDET}${group}.log
# This if commented with moving to tdaq-10-00-00 28.01.23
#    if [ $SUBDET == "PIX" ]; then
#		./bin/pvss2cool_220513 runOnce -s ${SUBDET} ${fgroup} -d ${cooldb} -t 7200 -i ${run} >> /localdisk/PvssToCool/logs/pvss2cool.${SUBDET}${group}.log
#	else 	# pvss2cool from tdaq release	
		pvss2cool runOnce -s ${SUBDET} ${fgroup} -d ${cooldb} -t 1800 -i ${run} >> /localdisk/PvssToCool/logs/pvss2cool.${SUBDET}${group}.log
#	fi
	echo "End PVSS2COOL for ${SUBDET} ${fgroup} at " `date`
fi
# End of main script
