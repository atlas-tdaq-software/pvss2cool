#!/bin/bash

INTERLOCK_DIR="/det/dcs/pvss2cool/bufferInterlock"
SUBDET="CIC"
# check if dir is empty
if [ "$(ls -A $INTERLOCK_DIR/$SUBDET)" ]; then
    echo "$INTERLOCK_DIR/$SUBDET has locks"
else
    echo "$INTERLOCK_DIR/$SUBDET has no locks"
fi
