#!/bin/sh
# To run several ($num) times pvss2cool runOnce for the detector $1
# for the time period $period
#
# Works only when the environment is set BEFOREHAND
# ?? Still DB is MONP200 in 2024 ??

num=10
period=36000

if test -z "$1"
then
	echo "No sub-detector information given"
else
	count=0
	SUBDET=$1
	while test $count -lt $num
	do
		echo "Running pvss2cool $count for $SUBDET"
		./bin/pvss2cool runOnce -s ${SUBDET} -d MONP200 -t ${period} -i 
		count=`expr $count + 1`
		sleep 2
	done
fi
