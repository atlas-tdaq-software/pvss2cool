#!/bin/sh
if [ -z "$1" ]; then
  echo "No sub-detector information given"
else
  SUBDET=$1
  shift
  myCount=`ps -ef | grep -v grep | grep -c 'pvss2cool runOnce -s '${SUBDET}`
  # if not found - equals 0, else it's running
  if [ $myCount -eq 0 ]; then
    echo "Pvss2Cool for ${SUBDET} not running"
  else
    #echo $myCount
    echo "Pvss2Cool for ${SUBDET} is running"
  fi
fi

