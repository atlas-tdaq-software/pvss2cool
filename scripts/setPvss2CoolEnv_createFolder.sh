#if test $# > 0; then
if [ -z "$1" ]; then
  echo "No sub-detector name given"
else
  SUBDET=$1
  shift
  echo "Setting up PVSS2COOL environment for ${SUBDET}"
  source /sw/tdaq/setup/setup_tdaq-11-02-01.sh

  cd /localdisk/PvssToCool
  export CORAL_AUTH_PATH='/localdisk/PvssToCool/private'		
  export CORAL_DBLOOKUP_PATH="/localdisk/PvssToCool/dblookup/${SUBDET}"
fi

unset SUBDET

