#!/bin/sh
if [ -z "$1" ]; then
  echo "No sub-detector name given"
else
  SUBDET=$1
  shift
  echo "Setting up PVSS2COOL environment for ${SUBDET}"
#  source /sw/tdaq/setup/setup_tdaq-06-01-01.sh
#  source /sw/tdaq/setup/setup_tdaq-07-00-00.sh		# since 24.01.17
#  source /sw/tdaq/setup/setup_tdaq-07-01-00.sh		# since 21.03.17
#  source /sw/tdaq/setup/setup_tdaq-09-01-00.sh 	# since 10.09.20
#  source /sw/tdaq/setup/setup_tdaq-09-02-01.sh 	# since 08.04.21
#  source /sw/tdaq/setup/setup_tdaq-09-03-00.sh 	# since 30.11.21
#  source /sw/tdaq/setup/setup_tdaq-09-04-00.sh 	# since 13.05.22
#  source /sw/tdaq/setup/setup_tdaq-10-00-00.sh 	# since 28.02.23
   source /sw/tdaq/setup/setup_tdaq-11-02-01.sh 	# since 13.04.24

  cd /localdisk/PvssToCool
  export CORAL_AUTH_PATH='/localdisk/PvssToCool/W_private'		# W-private since 26.02.10
  export CORAL_DBLOOKUP_PATH="/localdisk/PvssToCool/dblookup/${SUBDET}"
fi

unset SUBDET

