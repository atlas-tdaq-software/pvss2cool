# Check that at least one argument has been given, assumed to be the subdetector TLA
if [ -z "$1" ]; then
  echo "No sub-detector information given"
else
  SUBDET=$1
  shift

  # Setup foldergroup if not NULL				     # Since 23.09.09
  fgroup=""
  if [ -z "$1" ]; then
    fgroupMsg="ALL"
  else
    fgroup="-g $1"
    fgroupMsg="$1"
  fi
  
  myCount=`ps -ef | grep -v grep | grep -c "pvss2cool runOnce -s ${SUBDET} ${fgroup}"`
  
  # Check if any processes of this name are running
  if [ $myCount -gt 0 ]; then
    echo "Pvss2Cool for ${SUBDET} ${fgroup} is already running for Tier0 at" `date`
    echo "==> Skipping running this time"
  else
    echo ""
    echo "Running PVSS2COOL for ${SUBDET} at " `date`
    echo "Foldergroup: ${fgroupMsg}"

    # Setup environment for COOL
	source /sw/tdaq/setup/setup_tdaq-11-02-01.sh >> /dev/null        # Since 19.01.24 
#	source /sw/tdaq/setup/setup_tdaq-10-00-00.sh >> /dev/null        # Since 28.02.23 
#	source /sw/tdaq/setup/setup_tdaq-09-02-01.sh >> /dev/null        # Since 08.04.21 
#   source /sw/tdaq/setup/setup_tdaq-07-00-00.sh >> /dev/null        # Since 24.01.17 
#   source /sw/tdaq/setup/setup_tdaq-06-01-01.sh >> /dev/null        # Since 12.02.16 
#   source /sw/tdaq/setup/setup_tdaq-06-01-00.sh >> /dev/null        # Since 11.01.16 
#   source /sw/tdaq/setup/setup_tdaq-05-01-00.sh >> /dev/null        # Since 25.10.13 
#   source /sw/tdaq/setup/setup_tdaq-03-00-01.sh >> /dev/null        # Since 13.01.11 

    # Set specific CORAL paths
    cd /localscratch/PvssToCool/
    export CORAL_AUTH_PATH='/localscratch/PvssToCool/W_private'		# W_private since 26.02.10
    export CORAL_DBLOOKUP_PATH="/localscratch/PvssToCool/dblookup/${SUBDET}'"

    # Run process
    pvss2cool runOnce -s ${SUBDET} -d MONP200 -t 7200 ${fgroup} -i >> /localscratch/PvssToCool/logs/pvss2cool.${SUBDET}.log
    echo "End PVSS2COOL for ${SUBDET} ${fgroup} at " `date`
  fi
fi

unset SUBDET

