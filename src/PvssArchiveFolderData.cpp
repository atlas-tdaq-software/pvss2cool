// Include files
#include "CoralBase/Exception.h"
#include "CoolApplication/Application.h"
#include "RelationalAccess/IAuthenticationService.h"
#include "RelationalAccess/IAuthenticationCredentials.h"
#include "RelationalAccess/IConnectionService.h"
#include "RelationalAccess/ISessionProxy.h"
#include "RelationalAccess/IQuery.h"
#include "RelationalAccess/IRelationalService.h"
#include "RelationalAccess/IRelationalDomain.h"
#include "RelationalAccess/ITransaction.h"
#include "RelationalAccess/ISchema.h"
#include "RelationalAccess/ICursor.h"
// Commented moved to private headers 14.10.10 by SK
//#include "CoralBase/Attribute.h"
//#include "CoralBase/AttributeList.h"
//#include "CoralBase/TimeStamp.h"

#include "CoolKernel/ValidityKey.h"
#include "CoolKernel/types.h"
#include "CoolKernel/ChannelId.h"

#include <iostream>
#include <stdio.h>
#include <exception>
#include <algorithm>
#include <vector>

// Application Specific Include Files
// ----------------------------------
#include "globals.h"
#include "PvssArchiveNumber.h"
#include "PvssArchiveUtilities.h"
#include "CondDBUtilities.h"
#include "PvssArchiveFolderConfig.h"
#include "PvssCoolDefaultConnections.h"
#include "PvssArchiveFolderData.h"
//-----------------------------------------------------------------------------

	
PvssArchiveFolderData::PvssArchiveFolderData()
: m_session(0),
  m_query(0),
  m_cursor(0)
{
  return;
}

PvssArchiveFolderData::~PvssArchiveFolderData()
{
  return;
}

bool PvssArchiveFolderData::connect(coral::IConnectionService& connectionService,
                                    std::string serviceName)
{
  bool status = true;

  try {

    this->m_session = connectionService.connect(serviceName, coral::ReadOnly);

  }

  //==================
  // CATCH EXCEPTIONS
  //==================
  catch ( coral::Exception& e )
  {
    std::cout << "'PvssArchiveFolderData::connect()' CORAL exception : " << e.what() << std::endl;
    status = false;
  }
  catch ( std::exception& e )
  {
    std::cout << "'PvssArchiveFolderData::connect()' Standard C++ exception : " << e.what() << std::endl;
    status = false;
  }
  catch( ... )
  {
    std::cout << "'PvssArchiveFolderData::connect()' Unknown exception ..." << std::endl;
    status = false;
  }

  return (status);
}

bool PvssArchiveFolderData::disconnect()
{
  bool status = false;

  // Disconnecting
  if (this->m_session)
    delete this->m_session;

  return (status);
}

void PvssArchiveFolderData::cleanUp()
{
  // Delete the query to free up resources
  if (this->m_query) {
    delete this->m_query;
    this->m_query = 0;
  }
  this->m_session->transaction().commit();

  return;
}

// New function (27.10.11 SK) for check whether all necessary data being archived

int PvssArchiveFolderData::dpeNotArchived(std::string PVSS_COOL_FOLDER_CONFIGURATION_TABLE,
                                          FolderConfigurationInformation& folderConfig,
										  std::string dpSeparator, int rowCacheSize)
{
	int dpeNumberShouldBe = 0;
	int dpeNumberActual = 0;
	
	std::size_t iPos;

	std::string pattern;
	std::string condition;

	coral::ISchema& mySchema = this->m_session->nominalSchema();
	coral::AttributeList bindList;
	bindList.extend("folderId", "long");
	bindList["folderId"].setValue(folderConfig.folderId());
//	std::cerr << "Folder " << folderConfig.folderName() << std::endl;
	try {
		if(folderConfig.usesList() && (folderConfig.dpTypeInfo() != FW_CONDDB_DPTINFO_FSM)) {		//easy to find the number of channels
		    // Create the query to evaluate number of DPs (channels) multiplied by number of fields in the folder
			// This is the number of DPE to be sent to COOL in accordance with the folder configuration
		    this->m_session->transaction().start(true);
		    this->m_query = mySchema.newQuery();
			coral::AttributeList outputList;
			// The 'if' branch introduced for the Collection and AliasCollection foledr types
			// 17.06.13 by SK
			if( (folderConfig.dpTypeInfo() == FW_CONDDB_DPTINFO_COLL)
			 || (folderConfig.dpTypeInfo() == FW_CONDDB_DPTINFO_ALCOLL) ) {
			  this->m_query->addToOutputList("COUNT(DISTINCT FI.DPELEMENTNAME)");
			  this->m_query->addToTableList("PVSS2COOL_FIELDS", "FI");
			  this->m_query->setCondition("FI.FOLDERID = :folderId", bindList);
			}
			else {		// This is how it was before introducing Collection folders
		   	   // To be changed after improving (with change of schema) of fwCondDB package
				if(folderConfig.folderName() == "/PIXEL/DCS/TEMPERATURE")
					this->m_query->addToOutputList("COUNT(DISTINCT LI.DPNAME)");
				else
					this->m_query->addToOutputList("COUNT(DISTINCT LI.DPNAME)*COUNT(DISTINCT FI.DPELEMENTNAME)");
		       // End of the pice of code to be changed
				this->m_query->addToTableList("PVSS2COOL_LISTS", "LI");
				this->m_query->addToTableList("PVSS2COOL_FIELDS", "FI");

				this->m_query->setCondition("LI.FOLDERID = :folderId AND FI.FOLDERID = :folderId", bindList);
			}
			
			outputList.extend<int>("COUNT");
			this->m_query->defineOutput(outputList);
			this->m_query->setRowCacheSize(rowCacheSize);
			
			this->m_cursor = &(this->m_query->execute());
    		
			while (this->m_cursor->next()) {
      			const coral::AttributeList& row = this->m_cursor->currentRow();
      			dpeNumberShouldBe = row["COUNT"].data<int>();
//				std::cerr << "DPEs Should Be = " << dpeNumberShouldBe << std::endl;
			}
			this->cleanUp();
			
		    // Create the query to evaluate number of archived DPEs with the ELEMENTS table
		    this->m_session->transaction().start(true);
		    this->m_query = mySchema.newQuery();
			// First subquery
    		coral::IQueryDefinition& elementsList = this->m_query->defineSubQuery("LIST_DATA");
    		coral::IQueryDefinition& aliasInfo = this->m_query->defineSubQuery("LIST_ALIAS");
     		std::string elementName;
			if( (folderConfig.dpTypeInfo() == FW_CONDDB_DPTINFO_COLL)		// Collection folder
			 || (folderConfig.dpTypeInfo() == FW_CONDDB_DPTINFO_ALCOLL) )	// Alias Collection folder (added 17.06.13 by SK)
				elementName = "FI.DPELEMENTNAME"; 
			else			// Any with list, except Collection folder
				elementName = "L.DPNAME || NVL2(FI.DPELEMENTNAME, '" + dpSeparator + "' || FI.DPELEMENTNAME, '')";
			elementsList.addToOutputList(elementName, "ELEMENT_NAME");
			condition = "FI.FOLDERID = :folderId";
			elementsList.addToTableList("PVSS2COOL_FIELDS", "FI");
			if( (folderConfig.dpTypeInfo() != FW_CONDDB_DPTINFO_COLL)	
			 && (folderConfig.dpTypeInfo() != FW_CONDDB_DPTINFO_ALCOLL) ) {	 // Any with list, except Collection folder
				elementsList.addToTableList("PVSS2COOL_LISTS", "L");
				condition += " AND L.FOLDERID = :folderId";
			}
			else {
			  	// Building aliasInfo subquery
				aliasInfo.addToOutputList("EL.SYS_ID", "SYS_ID");
    		  	aliasInfo.addToOutputList("EL.ALIAS", "ALIAS");
				aliasInfo.addToOutputList("EL.ELEMENT_ID", "EID");
      			aliasInfo.addToOutputList("SY.SYS_NAME", "SYSNAME");
		
				aliasInfo.addToTableList("ELEMENTS", "EL");
				aliasInfo.addToTableList("SYSTEMS", "SY");
		
				aliasInfo.setCondition("EL.SYS_ID = SY.SYS_ID", coral::AttributeList());
			}
			// bindList is still alive from the previous query
			elementsList.setCondition(condition, bindList);
			// And now main query
			coral::AttributeList secondOutputList;
      		this->m_query->addToOutputList("COUNT(DISTINCT FE.ELEMENT_NAME)");
			secondOutputList.extend<int>("COUNT");
			this->m_query->addToTableList("LIST_DATA", "FE");
			if(folderConfig.dpTypeInfo() != FW_CONDDB_DPTINFO_ALCOLL) {
				this->m_query->addToTableList("ELEMENTS", "EL");
				this->m_query->setCondition("FE.ELEMENT_NAME = EL.ELEMENT_NAME", coral::AttributeList());
			}
			else {
				this->m_query->addToTableList("LIST_ALIAS", "LI");
				this->m_query->setCondition("FE.ELEMENT_NAME = LI.SYSNAME ||':&'||LI.ALIAS", coral::AttributeList());
			}
			this->m_query->defineOutput(secondOutputList);
			this->m_query->setRowCacheSize(rowCacheSize);
			
			this->m_cursor = &(this->m_query->execute());
    		
			while (this->m_cursor->next()) {
      			const coral::AttributeList& row = this->m_cursor->currentRow();
      			dpeNumberActual = row["COUNT"].data<int>();
//				std::cerr << "DPEs found in ELEMENTS = " << dpeNumberActual << std::endl;
			}
			this->cleanUp();
		}
		else {  // Much more sophisticated case when DP list is unknown and to be calculated by channel IDs

		    if ((folderConfig.channelIdType() == FW_CONDDB_IDINFO_DPNAME) && (folderConfig.dpTypeInfo() != FW_CONDDB_DPTINFO_FSM)) {
//				std::cerr << "This folder uses DPNAME for CHANNEL_ID" << std::endl;
  		   // Create the query and subqueries to evaluate number of DPE to be sent to COOL
				this->m_session->transaction().start(true);
				this->m_query = mySchema.newQuery();
				
				coral::IQueryDefinition& channelList = this->m_query->defineSubQuery("CHANNEL_LIST");
				channelList.setDistinct();
				channelList.addToOutputList("DPNAME", "CHANNEL_ID");
				channelList.addToTableList("DP", "DP");
				channelList.addToTableList("DPT", "DPT");
				channelList.addToTableList("ELEMENTS", "EL");
				bindList.extend("dptName", "string");
				bindList["dptName"].setValue(folderConfig.dpTypeName());
				condition = "DPT.DPTNAME = :dptName AND DP.DP_ID = EL.DP_ID AND DPT.DPT_ID = EL.DPT_ID AND EL.SYS_ID = DP.SYS_ID AND EL.SYS_ID = DPT.SYS_ID";
			// Check if the pattern is a single '*' (which means it is pointless to use it)
				pattern = folderConfig.pattern();
				if ((pattern.length() == 0) || ((pattern.length() == 1) && (pattern.compare("*") == 0))) {
				} else {
					iPos = pattern.find_first_of("*");
					while (iPos != std::string::npos) {
						pattern[iPos] = '%';
						iPos = pattern.find_first_of("*");
					}
					bindList.extend("pattern", "string");
					bindList["pattern"].setValue(pattern);
					condition += " AND DP.DPNAME LIKE :pattern";
				}
				channelList.setCondition(condition, bindList);
			
				// Main query now
				coral::AttributeList outputList;
      			this->m_query->addToOutputList("COUNT(DISTINCT CL.CHANNEL_ID)*DECODE(COUNT(DISTINCT FI.DPELEMENTNAME), 0, 1, COUNT(DISTINCT FI.DPELEMENTNAME))");
				outputList.extend<int>("COUNT");
				this->m_query->addToTableList("PVSS2COOL_FIELDS", "FI");
				this->m_query->addToTableList("CHANNEL_LIST", "CL");
			
				this->m_query->setCondition("FI.FOLDERID = :folderId", bindList);
				this->m_query->defineOutput(outputList);
				this->m_query->setRowCacheSize(rowCacheSize);
			
				this->m_cursor = &(this->m_query->execute());
    		
				while (this->m_cursor->next()) {
      				const coral::AttributeList& row = this->m_cursor->currentRow();
      				dpeNumberShouldBe = row["COUNT"].data<int>();
//					std::cerr << "DPEs Should Be = " << dpeNumberShouldBe << std::endl;
				}
				this->cleanUp();
			
		  	  // Create the query to evaluate number of archived DPEs with the ELEMENTS table
		  		this->m_session->transaction().start(true);
		 		this->m_query = mySchema.newQuery();
				
				// First subquery
    			coral::IQueryDefinition& elementsList = this->m_query->defineSubQuery("LIST_DATA");
				elementsList.setDistinct();
				elementsList.addToOutputList("EL.ELEMENT_NAME", "EID");
				elementsList.addToOutputList("FI.POSITION");
				elementsList.addToOutputList("REGEXP_REPLACE(DP.DPNAME, '[^([:alpha:],[:digit:],_)]', '_')", "CHANNEL_ID");
				elementsList.addToTableList("ELEMENTS", "EL");
				elementsList.addToTableList(PVSS_COOL_FOLDER_FIELD_CONFIGURATION_TABLE, "FI");
				elementsList.addToTableList("DPT");
				elementsList.addToTableList("DP");
				elementsList.addToTableList("DPE");
				condition = std::string("FI.FOLDERID = :folderId AND FI.DPELEMENTNAME = DPE.DPENAME AND EL.SYS_ID = DPE.SYS_ID AND EL.DPE_ID = DPE.DPE_ID AND EL.DP_ID = DPE.DP_ID AND EL.SYS_ID = DPT.SYS_ID AND EL.DPT_ID = DPT.DPT_ID AND EL.DP_ID = DP.DP_ID AND EL.SYS_ID = DP.SYS_ID AND DPT.DPTNAME = :dptName");
				// Pattern and bindList are ready from the previous query
				if ((pattern.length() != 0) && (pattern.compare("*") != 0)) 
					condition += " AND DP.DPNAME LIKE :pattern";
				elementsList.setCondition(condition, bindList);
				
				// And now main query				
				coral::AttributeList secondOutputList;
      			this->m_query->addToOutputList("COUNT(DISTINCT AEL.EID)");
				secondOutputList.extend<int>("COUNT");
				this->m_query->addToTableList("LIST_DATA", "AEL");
				this->m_query->defineOutput(secondOutputList);
				this->m_query->setRowCacheSize(rowCacheSize);
			
				this->m_cursor = &(this->m_query->execute());
    		
				while (this->m_cursor->next()) {
      				const coral::AttributeList& row = this->m_cursor->currentRow();
      				dpeNumberActual = row["COUNT"].data<int>();
//					std::cerr << "DPEs found in ELEMENTS = " << dpeNumberActual << std::endl;
				}
				this->cleanUp();
			}
			else {
				if(((folderConfig.channelIdType() == FW_CONDDB_IDINFO_ALIAS) || (folderConfig.channelIdType() == FW_CONDDB_IDINFO_ALIAS_STRING))
				&& (folderConfig.dpTypeInfo() != FW_CONDDB_DPTINFO_FSM)) {
//					std::cerr << "This folder uses ALIAS for CHANNEL_ID" << std::endl;
		  			this->m_session->transaction().start(true);
		 			this->m_query = mySchema.newQuery();
				  // Subquery for list of channels
					coral::IQueryDefinition& channelList = this->m_query->defineSubQuery("CHANNEL_LIST");
					channelList.setDistinct();
					channelList.addToOutputList("DP.DPNAME");
					channelList.addToOutputList("F.FOLDERID");
					if (folderConfig.channelIdType() == FW_CONDDB_IDINFO_ALIAS)
						channelList.addToOutputList("TO_NUMBER(EL.ALIAS)", "CHANNEL_ID");
					else  // if (folderConfig.channelIdType() == FW_CONDDB_IDINFO_ALIAS_STRING)
						channelList.addToOutputList("REGEXP_REPLACE(EL.ALIAS, '[^([:alpha:],[:digit:],_)]', '_')", "CHANNEL_ID");
					channelList.addToOutputList("F.CHANNELIDINFO");
					channelList.addToOutputList("EL.ELEMENT_NAME");
					channelList.addToTableList("DPT", "DPT");
					channelList.addToTableList("DP", "DP");
					channelList.addToTableList("DPE", "DPE");
					channelList.addToTableList("ELEMENTS", "EL");
					channelList.addToTableList(PVSS_COOL_FOLDER_CONFIGURATION_TABLE, "F");
					bindList.extend("dptName", "string");
					bindList["dptName"].setValue(folderConfig.dpTypeName());
					condition = "F.FOLDERID = :folderId AND DPT.DPTNAME = :dptName AND EL.SYS_ID = DPE.SYS_ID ";
					condition += "AND EL.DPE_ID = DPE.DPE_ID AND EL.DP_ID = DPE.DP_ID AND EL.SYS_ID = DPT.SYS_ID ";
					condition += "AND EL.DPT_ID = DPT.DPT_ID AND EL.DP_ID = DP.DP_ID AND EL.SYS_ID = DP.SYS_ID ";
					condition += "AND EL.ELEMENT_NAME = DP.DPNAME || NVL2(F.CHANNELIDINFO,'.'||F.CHANNELIDINFO,'') ";
					condition += "AND EL.EVENT = 1 AND EL.ALIAS IS NOT NULL";
				// Check if the pattern is a single '*' (which means it is pointless to use it)
					pattern = folderConfig.pattern();
					if ((pattern.length() == 0) || ((pattern.length() == 1) && (pattern.compare("*") == 0))) {
					} else {
						iPos = pattern.find_first_of("*");
						while (iPos != std::string::npos) {
							pattern[iPos] = '%';
							iPos = pattern.find_first_of("*");
						}
						bindList.extend("pattern", "string");
						bindList["pattern"].setValue(pattern);
						condition += " AND DP.DPNAME LIKE :pattern";
					}
					channelList.setCondition(condition, bindList);
					// Main query 
					coral::AttributeList outputList;
      				this->m_query->addToOutputList("COUNT(DISTINCT CL.CHANNEL_ID)*DECODE(COUNT(DISTINCT FI.DPELEMENTNAME), 0, 1, COUNT(DISTINCT FI.DPELEMENTNAME))");
					outputList.extend<int>("COUNT");
					this->m_query->addToTableList("PVSS2COOL_FIELDS", "FI");
					this->m_query->addToTableList("CHANNEL_LIST", "CL");
			
					this->m_query->setCondition("FI.FOLDERID = :folderId", bindList);
					this->m_query->defineOutput(outputList);
					this->m_query->setRowCacheSize(rowCacheSize);
			
					this->m_cursor = &(this->m_query->execute());
    		
					while (this->m_cursor->next()) {
      					const coral::AttributeList& row = this->m_cursor->currentRow();
      					dpeNumberShouldBe = row["COUNT"].data<int>();
//						std::cerr << "DPEs Should Be = " << dpeNumberShouldBe << std::endl;
					}
					this->cleanUp();
			
		  		  // Create the query to evaluate number of archived DPEs with the ELEMENTS table
		  			this->m_session->transaction().start(true);
		 			this->m_query = mySchema.newQuery();
					coral::IQueryDefinition& elementsList = this->m_query->defineSubQuery("LIST_DATA");
					coral::IQueryDefinition& folderInfo = elementsList.defineSubQuery("FOLDER_INFO");
				
				  // Subquery for folder Info
					folderInfo.addToOutputList("DP.DPNAME");
					folderInfo.addToOutputList("F.FOLDERID");
					if (folderConfig.channelIdType() == FW_CONDDB_IDINFO_ALIAS)
						folderInfo.addToOutputList("TO_NUMBER(EL.ALIAS)", "CHANNEL_ID");
					else  // if (folderConfig.channelIdType() == FW_CONDDB_IDINFO_ALIAS_STRING)
						folderInfo.addToOutputList("REGEXP_REPLACE(EL.ALIAS, '[^([:alpha:],[:digit:],_)]', '_')", "CHANNEL_ID");
					folderInfo.addToTableList("DPT", "DPT");
					folderInfo.addToTableList("DP", "DP");
					folderInfo.addToTableList("DPE", "DPE");
					folderInfo.addToTableList("ELEMENTS", "EL");
					folderInfo.addToTableList(PVSS_COOL_FOLDER_CONFIGURATION_TABLE, "F");
					// Full 'condition' and bindList are already there
					folderInfo.setCondition(condition, bindList);
				  // Subquery for ELEMENTS list for this folder
					elementsList.addToOutputList("EL.ELEMENT_ID", "EID");
					elementsList.addToOutputList("FI.POSITION", "FIELD_POSITION");
					elementsList.addToOutputList("L.CHANNEL_ID", "CHANNEL_ID"); 
					elementsList.addToTableList("ELEMENTS", "EL");
					elementsList.addToTableList(PVSS_COOL_FOLDER_FIELD_CONFIGURATION_TABLE, "FI");
					elementsList.addToTableList("FOLDER_INFO", "L");
					elementsList.setCondition("FI.FOLDERID = :folderId AND L.FOLDERID = :folderId AND EL.ELEMENT_NAME = L.DPNAME || NVL2(FI.DPELEMENTNAME,'.'||FI.DPELEMENTNAME,'')", bindList);
					
				  // And now main query				
					coral::AttributeList secondOutputList;
      				this->m_query->addToOutputList("COUNT(DISTINCT AEL.EID)");
					secondOutputList.extend<int>("COUNT");
					this->m_query->addToTableList("LIST_DATA", "AEL");
					this->m_query->defineOutput(secondOutputList);
					this->m_query->setRowCacheSize(rowCacheSize);
			
					this->m_cursor = &(this->m_query->execute());
    		
					while (this->m_cursor->next()) {
      					const coral::AttributeList& row = this->m_cursor->currentRow();
      					dpeNumberActual = row["COUNT"].data<int>();
//						std::cerr << "DPEs found in ELEMENTS = " << dpeNumberActual << std::endl;
					}
					this->cleanUp();
				}
				else  {			// Only FSM-type folder is not yet considered, it seems
				  if(folderConfig.dpTypeInfo() == FW_CONDDB_DPTINFO_FSM)	{
		  			this->m_session->transaction().start(true);
		 			this->m_query = mySchema.newQuery();
      				this->m_query->addToOutputList("COUNT(DISTINCT DPNAME)*2");
					coral::AttributeList outputList;
					outputList.extend<int>("COUNT");
					this->m_query->addToTableList(PVSS_COOL_FOLDER_LIST_CONFIGURATION_TABLE, "LI");
					this->m_query->setCondition("LI.FOLDERID = :folderId", bindList);
					this->m_query->defineOutput(outputList);
					this->m_query->setRowCacheSize(rowCacheSize);
			
					this->m_cursor = &(this->m_query->execute());
    		
					while (this->m_cursor->next()) {
      					const coral::AttributeList& row = this->m_cursor->currentRow();
      					dpeNumberShouldBe = row["COUNT"].data<int>();
//						std::cerr << "DPEs Should Be = " << dpeNumberShouldBe << std::endl;
					}
					this->cleanUp();

		  			this->m_session->transaction().start(true);
		 			this->m_query = mySchema.newQuery();
					coral::IQueryDefinition& elementsList = this->m_query->defineSubQuery("LIST_DATA");
					coral::IQueryDefinition& stateInfo = elementsList.defineSubQuery("STATE_INFO");
					coral::IQueryDefinition& statusInfo = stateInfo.applySetOperation(coral::IQueryDefinition::Union);
					
					stateInfo.addToTableList(PVSS_COOL_FOLDER_LIST_CONFIGURATION_TABLE, "L");
					statusInfo.addToTableList(PVSS_COOL_FOLDER_LIST_CONFIGURATION_TABLE, "L");
					if (folderConfig.channelIdType() == FW_CONDDB_IDINFO_USER_DEFINED) {
						stateInfo.addToOutputList("TO_NUMBER(L.CHANNELID)", "CHANNEL_ID");
						statusInfo.addToOutputList("TO_NUMBER(L.CHANNELID)", "CHANNEL_ID");
					} else {
						stateInfo.addToOutputList("REGEXP_REPLACE(L.CHANNELNAME, '[^([:alpha:],[:digit:],_)]', '_')", "CHANNEL_ID");
						statusInfo.addToOutputList("REGEXP_REPLACE(L.CHANNELNAME, '[^([:alpha:],[:digit:],_)]', '_')", "CHANNEL_ID");
					}
					stateInfo.addToOutputList("REPLACE(L.DPNAME, '|STATUS_', '|') || '.fsm.currentState'", "ELEMENT_NAME");
					stateInfo.addToOutputList("1", "POSITION");
					statusInfo.addToOutputList("L.DPNAME || '.fsm.currentState'", "ELEMENT_NAME");
					statusInfo.addToOutputList("2", "POSITION");
					stateInfo.setCondition("L.FOLDERID = :folderId", bindList);
					statusInfo.setCondition("L.FOLDERID = :folderId", bindList);
				  // Subquery for ELEMENTS list for this folder
					elementsList.addToOutputList("EL.ELEMENT_ID", "EID");
					elementsList.addToOutputList("FI.POSITION", "FIELD_POSITION");
					elementsList.addToTableList("ELEMENTS", "EL");
					elementsList.addToTableList("STATE_INFO", "FI");
					elementsList.setCondition("FI.ELEMENT_NAME = EL.ELEMENT_NAME", coral::AttributeList());
					
				  // And now main query				
					coral::AttributeList secondOutputList;
      				this->m_query->addToOutputList("COUNT(DISTINCT AEL.EID)");
					secondOutputList.extend<int>("COUNT");
					this->m_query->addToTableList("LIST_DATA", "AEL");
					this->m_query->defineOutput(secondOutputList);
					this->m_query->setRowCacheSize(rowCacheSize);
			
					this->m_cursor = &(this->m_query->execute());
    		
					while (this->m_cursor->next()) {
      					const coral::AttributeList& row = this->m_cursor->currentRow();
      					dpeNumberActual = row["COUNT"].data<int>();
//						std::cerr << "DPEs found in ELEMENTS = " << dpeNumberActual << std::endl;
					}
					this->cleanUp();
				  }
				  else {
				  	std::cout << "Folder " << folderConfig.folderName() << " is of UNKNOWN type: Control of archived DPE skipped" << std::endl;
				  }
				}
			}
		}
	}
			
			
  //==================
  // CATCH EXCEPTIONS
  //==================
  catch ( coral::Exception& e )
  {
    std::cout << "'PvssArchiveFolderData::dpeNotArchived()' CORAL exception : " << e.what() << std::endl;
  }
  catch ( std::exception& e )
  {
    std::cout << "'PvssArchiveFolderData::dpeNotArchived()' Standard C++ exception : " << e.what() << std::endl;
  }
  catch( ... )
  {
    std::cout << "'PvssArchiveFolderData::dpeNotArchived()' Unknown exception ..." << std::endl;
  }

	return(dpeNumberShouldBe - dpeNumberActual);
}


coral::ICursor& PvssArchiveFolderData::getFolderData(std::string PVSS_COOL_FOLDER_CONFIGURATION_TABLE,
                                                     FolderConfigurationInformation& folderConfig,
                                                     std::vector<FolderFieldConfiguration>& folderFields,
                                                     PvssArchiveNumber& archiveNumber,
                                                     int rowCacheSize,
                                                     double &readTime,
													 bool milliPrecision)
{

  char cpTableName[50];
  char cpPosition[10];

  std::size_t iPos;

  std::string idElementDpe;
  std::string outputString;
  std::string pattern;
  std::string bindName;
  std::string condition;
  std::string timeStringLength;
  std::string folderDataCondition;
  std::string dpSeparator;

  coral::TimeStamp tStart;
  coral::TimeStamp tEnd;
  
  int notArchived;
  std::string verb;

  try {

    // Set some basic information
    readTime = 0.0;
    if (folderConfig.dpTypeInfo() == FW_CONDDB_DPTINFO_FW)
      dpSeparator = "/";
    else
      dpSeparator = ".";
	
	// The function evaluates difference between the number of DPE necessary for a folder
	// and number of DPE archived for that folder
	// Introduced by SK 23.11.11
	notArchived = dpeNotArchived(PVSS_COOL_FOLDER_CONFIGURATION_TABLE, folderConfig, dpSeparator, rowCacheSize); 
	if( notArchived > 0) {
		if(notArchived == 1) verb = " is"; 
		else verb = " are";
		std::cout << "WARNING: " << notArchived << " DPE necessary for folder "
				  << folderConfig.folderName() << verb << " NOT being archived!" << std::endl;
	}
//	else
//		std::cout << "All DPE seem to be archived" << std::endl;
		
   // Get start and end times in required format
    std::string fromTime = archiveNumber.startTime();
    std::string toTime = archiveNumber.endTime();

    // Get time now
    tStart = CondDBUtilities::getTimeNow();
    
    // Create the query and subqueries to extract the actual data
    this->m_session->transaction().start(true);
    coral::ISchema& mySchema = this->m_session->nominalSchema();
    this->m_query = mySchema.newQuery();
    coral::IQueryDefinition& folderData = this->m_query->defineSubQuery("FOLDER_DATA");		
    coral::IQueryDefinition& valueData = folderData.defineSubQuery("VALUE_DATA");
    coral::IQueryDefinition& dataElements = folderData.defineSubQuery("DATA_ELEMENTS");
    coral::IQueryDefinition& folderInfo = dataElements.defineSubQuery("FOLDER_INFO");					// FI
	// New line as of 14.06.13
	coral::IQueryDefinition& aliasInfo = dataElements.defineSubQuery("ALIAS_INFO");						// ALI
    coral::IQueryDefinition& stateInfo = dataElements.defineSubQuery("STATE_INFO");
    coral::IQueryDefinition& statusInfo = stateInfo.applySetOperation(coral::IQueryDefinition::Union);
    coral::IQueryDefinition& listInfo = folderInfo.defineSubQuery("LIST_INFO");							// 

    // Set the lowest level subquery (only required if a list is being used, but not for FSM type)
    if (folderConfig.usesList() && (folderConfig.dpTypeInfo() != FW_CONDDB_DPTINFO_FSM)) {
      std::string elementName;
	  // under 'if - introduced 12.11.12 by SK for the Collection folder type
	  // under 'else' - standard way for "classical" folder types
	  if((folderConfig.dpTypeInfo() == FW_CONDDB_DPTINFO_COLL)
	  || (folderConfig.dpTypeInfo() == FW_CONDDB_DPTINFO_ALCOLL))
	    elementName = "F.DPELEMENTNAME";
	  else
        elementName = "/*+ LEADING(L) */ L.DPNAME || NVL2(F.DPELEMENTNAME, '" + dpSeparator + "' || F.DPELEMENTNAME, '')";
      folderInfo.addToOutputList(elementName, "ELEMENT_NAME");
      folderInfo.addToOutputList("F.POSITION", "POSITION");

      if ((folderConfig.channelIdType() == FW_CONDDB_IDINFO_ALIAS) ||
          (folderConfig.channelIdType() == FW_CONDDB_IDINFO_ALIAS_STRING)) {
        folderInfo.addToOutputList("L.CHANNEL_ID", "CHANNEL_ID");
        if (folderConfig.channelIdType() == FW_CONDDB_IDINFO_ALIAS)
          listInfo.addToOutputList("TO_NUMBER(EL.ALIAS)", "CHANNEL_ID");
        else
          listInfo.addToOutputList("REGEXP_REPLACE(EL.ALIAS, '[^([:alpha:],[:digit:],_)]', '_')", "CHANNEL_ID");
        listInfo.addToOutputList("L.FOLDERID", "FOLDERID");
        listInfo.addToOutputList("L.DPNAME", "DPNAME");
        listInfo.addToTableList("ELEMENTS", "EL");
        listInfo.addToTableList(PVSS_COOL_FOLDER_LIST_CONFIGURATION_TABLE, "L");
        listInfo.addToTableList("DPT");
        listInfo.addToTableList(PVSS_COOL_FOLDER_CONFIGURATION_TABLE, "F");

        coral::AttributeList listBindList;
        listBindList.extend("folderId", "long");
        listBindList["folderId"].setValue(folderConfig.folderId());
        listBindList.extend("dpSeparator", "string");
        listBindList["dpSeparator"].setValue(dpSeparator);
        listBindList.extend("channelIdElement", "string");
        listBindList["channelIdElement"].setValue(folderConfig.channelIdInfo());
        listInfo.setCondition("L.FOLDERID = :folderId AND F.FOLDERID = :folderId AND F.FOLDERID = L.FOLDERID AND EL.EVENT = 1 AND EL.ALIAS IS NOT NULL AND EL.ELEMENT_NAME = L.DPNAME || :dpSeparator || :channelIdElement AND DPT.DPTNAME = F.DPTYPENAME AND EL.SYS_ID = DPT.SYS_ID AND EL.DPT_ID = DPT.DPT_ID", listBindList);
        folderInfo.addToTableList("LIST_INFO", "L");
      } else {
        folderInfo.addToTableList(PVSS_COOL_FOLDER_LIST_CONFIGURATION_TABLE, "L");
        if (folderConfig.channelIdType() == FW_CONDDB_IDINFO_USER_DEFINED) {
          folderInfo.addToOutputList("TO_NUMBER(L.CHANNELID)", "CHANNEL_ID");
        } else if (folderConfig.channelIdType() == FW_CONDDB_IDINFO_USER_DEFINED_STRING) {
          folderInfo.addToOutputList("REGEXP_REPLACE(L.CHANNELNAME, '[^([:alpha:],[:digit:],_)]', '_')", "CHANNEL_ID");
        } else if (folderConfig.channelIdType() == FW_CONDDB_IDINFO_DPNAME) { 	// Use DP name for channel ID as string
          folderInfo.addToOutputList("REGEXP_REPLACE(L.DPNAME, '[^([:alpha:],[:digit:],_)]', '_')", "CHANNEL_ID");
        }
      }

      folderInfo.addToTableList(PVSS_COOL_FOLDER_FIELD_CONFIGURATION_TABLE, "F");
      coral::AttributeList folderList;
      folderList.extend("folderId", "long");
      folderList["folderId"].setValue(folderConfig.folderId());
 // Original code (single line) replaced below 13.11.12 by SK for 'Collection' folder type
 // folderInfo.setCondition("F.FOLDERID = :folderId AND L.FOLDERID = :folderId AND L.FOLDERID = F.FOLDERID", folderList);
	 // New code as of 13.11.12 by SK for 'Collection' folder type
	  condition = "F.FOLDERID = :folderId AND L.FOLDERID = :folderId AND L.FOLDERID = F.FOLDERID";
	  if((folderConfig.dpTypeInfo() == FW_CONDDB_DPTINFO_COLL)
	  || (folderConfig.dpTypeInfo() == FW_CONDDB_DPTINFO_ALCOLL)) {
	  	if(folderConfig.channelIdType() == FW_CONDDB_IDINFO_USER_DEFINED_STRING)
	  		condition += " AND L.CHANNELNAME = F.CHNAME";
		else		// means folderConfig.channelIdType() == FW_CONDDB_IDINFO_USER_DEFINED
	  		condition += " AND L.CHANNELID = F.CHNAME";
	  }   
	  folderInfo.setCondition(condition, folderList);
	 // End of new code of 13.11.12
	 
	 // New code as of 14.06.13 by SK for "AliasCollection' folder type
	  if(folderConfig.dpTypeInfo() == FW_CONDDB_DPTINFO_ALCOLL) {
	  	// Building aliasInfo subquery
		aliasInfo.addToOutputList("EL.SYS_ID", "SYS_ID");
      	aliasInfo.addToOutputList("EL.ALIAS", "ALIAS");
		aliasInfo.addToOutputList("EL.ELEMENT_ID", "EID");
      	aliasInfo.addToOutputList("SY.SYS_NAME", "SYSNAME");
// Added 05.06.18 two lines	
        aliasInfo.addToOutputList("EL.VALID_SINCE", "SINCE");
    	aliasInfo.addToOutputList("EL.VALID_TILL", "TILL");
		
		aliasInfo.addToTableList("ALIASES_ALL", "EL");
		aliasInfo.addToTableList("SYSTEMS", "SY");
		
		aliasInfo.setCondition("EL.SYS_ID = SY.SYS_ID", coral::AttributeList());
	  }
	 // End of new code of 14.06.13

      dataElements.addToOutputList("FI.CHANNEL_ID", "CHANNEL_ID");
    }

    // Now set up the query to get the actual element IDs
	// if/else introduced 14.06.13 by SK for AliasCollection folder type
	// before ONLY the part under 'if' was there
	if(folderConfig.dpTypeInfo() != FW_CONDDB_DPTINFO_ALCOLL) {
   		dataElements.addToOutputList("EL.ELEMENT_ID", "EID");
// Added 05.06.18		
		dataElements.addToOutputList("EL.VALID_SINCE", "SINCE");
		dataElements.addToOutputList("EL.VALID_TILL", "TILL");		
    	dataElements.addToTableList("ELEMENTS_ALL", "EL");
	}
	else {
   		dataElements.addToOutputList("ALI.EID", "EID");
// Added 05.06.18		
		dataElements.addToOutputList("ALI.SINCE", "SINCE");
		dataElements.addToOutputList("ALI.TILL", "TILL");		
		dataElements.addToTableList("ALIAS_INFO", "ALI");
	}	
    dataElements.addToOutputList("FI.POSITION", "FIELD_POSITION");

    // Check if DP definitions use pattern or specified list
    if (folderConfig.usesList() && (folderConfig.dpTypeInfo() != FW_CONDDB_DPTINFO_FSM)) {

      // Get the data elements for the list
      dataElements.addToTableList("FOLDER_INFO", "FI");
	// if/else introduced 14.06.13 by SK for AliasCollection folder type
	// before ONLY the part under 'if' was there
	if(folderConfig.dpTypeInfo() != FW_CONDDB_DPTINFO_ALCOLL)
    	dataElements.setCondition("FI.ELEMENT_NAME = EL.ELEMENT_NAME", coral::AttributeList());
	else
		dataElements.setCondition("FI.ELEMENT_NAME = ALI.SYSNAME ||':&'|| ALI.ALIAS", coral::AttributeList());
		
    } else if (folderConfig.dpTypeInfo() == FW_CONDDB_DPTINFO_FSM) { 	// FSM folder type, which is a special case
      stateInfo.addToTableList(PVSS_COOL_FOLDER_LIST_CONFIGURATION_TABLE, "L");
      statusInfo.addToTableList(PVSS_COOL_FOLDER_LIST_CONFIGURATION_TABLE, "L");
      if (folderConfig.channelIdType() == FW_CONDDB_IDINFO_USER_DEFINED) {
        stateInfo.addToOutputList("TO_NUMBER(L.CHANNELID)", "CHANNEL_ID");
        statusInfo.addToOutputList("TO_NUMBER(L.CHANNELID)", "CHANNEL_ID");
      } else {
        stateInfo.addToOutputList("REGEXP_REPLACE(L.CHANNELNAME, '[^([:alpha:],[:digit:],_)]', '_')", "CHANNEL_ID");
        statusInfo.addToOutputList("REGEXP_REPLACE(L.CHANNELNAME, '[^([:alpha:],[:digit:],_)]', '_')", "CHANNEL_ID");
      }
      stateInfo.addToOutputList("REPLACE(L.DPNAME, '|STATUS_', '|') || '.fsm.currentState'", "ELEMENT_NAME");
      stateInfo.addToOutputList("1", "POSITION");
      statusInfo.addToOutputList("L.DPNAME || '.fsm.currentState'", "ELEMENT_NAME");
      statusInfo.addToOutputList("2", "POSITION");
      coral::AttributeList bindList;
      bindList.extend("folderId", "long");
      bindList["folderId"].setValue(folderConfig.folderId());
      stateInfo.setCondition("L.FOLDERID = :folderId", bindList);

      // Although the condition is exactly the same for the 2 queries being union-ed, using the
      // same AttributeList causes a segmentation fault, therefore need to declare another one
      coral::AttributeList bindList2;
      bindList2.extend("folderId", "long");
      bindList2["folderId"].setValue(folderConfig.folderId());
      statusInfo.setCondition("L.FOLDERID = :folderId", bindList2);

      dataElements.addToTableList("STATE_INFO", "FI");
      dataElements.addToOutputList("FI.CHANNEL_ID", "CHANNEL_ID");
      dataElements.setCondition("FI.ELEMENT_NAME = EL.ELEMENT_NAME", coral::AttributeList());
    } else {  // This folder uses a pattern

      dataElements.addToTableList(PVSS_COOL_FOLDER_FIELD_CONFIGURATION_TABLE, "FI");

      // Use DP name for channel ID as string
      if (folderConfig.channelIdType() == FW_CONDDB_IDINFO_DPNAME) {
        dataElements.addToOutputList("REGEXP_REPLACE(DP.DPNAME, '[^([:alpha:],[:digit:],_)]', '_')", "CHANNEL_ID");
        dataElements.addToTableList("DPE");
        dataElements.addToTableList("DPT");
        dataElements.addToTableList("DP");
        coral::AttributeList bindList;
        bindList.extend("folderId", "long");
        bindList["folderId"].setValue(folderConfig.folderId());
        bindList.extend("dptName", "string");
        bindList["dptName"].setValue(folderConfig.dpTypeName());
        condition = std::string("FI.FOLDERID = :folderId AND FI.DPELEMENTNAME = DPE.DPENAME AND EL.SYS_ID = DPE.SYS_ID AND EL.DPE_ID = DPE.DPE_ID AND EL.DP_ID = DPE.DP_ID AND EL.SYS_ID = DPT.SYS_ID AND EL.DPT_ID = DPT.DPT_ID AND EL.DP_ID = DP.DP_ID AND EL.SYS_ID = DP.SYS_ID AND DPT.DPTNAME = :dptName");

        // Check if the pattern is a single '*' (which means it is pointless to use it)
        pattern = folderConfig.pattern();
        if ((pattern.length() == 0) || ((pattern.length() == 1) && (pattern.compare("*") == 0))) {
        } else {
          bindList.extend("pattern", "string");
          iPos = pattern.find_first_of("*");
          while (iPos != std::string::npos) {
            pattern[iPos] = '%';
            iPos = pattern.find_first_of("*");
          }
          bindList["pattern"].setValue(pattern);
          condition += std::string(" AND DP.DPNAME LIKE :pattern");
        }
        dataElements.setCondition(condition, bindList);
      } else { // Must be using an alias as channel ID (as this is the only other choice with specifying a pattern)
        folderInfo.addToOutputList("DP.DPNAME");
        folderInfo.addToOutputList("F.FOLDERID");
        if (folderConfig.channelIdType() == FW_CONDDB_IDINFO_ALIAS)
          folderInfo.addToOutputList("TO_NUMBER(EL.ALIAS)", "CHANNEL_ID");
        else if (folderConfig.channelIdType() == FW_CONDDB_IDINFO_ALIAS_STRING)
          folderInfo.addToOutputList("REGEXP_REPLACE(EL.ALIAS, '[^([:alpha:],[:digit:],_)]', '_')", "CHANNEL_ID");
        folderInfo.addToTableList("DPE");
        folderInfo.addToTableList("DPT");
        folderInfo.addToTableList("DP");
        folderInfo.addToTableList("ELEMENTS", "EL");
        folderInfo.addToTableList(PVSS_COOL_FOLDER_CONFIGURATION_TABLE, "F");
        coral::AttributeList bindList;
        bindList.extend("folderId", "long");
        bindList["folderId"].setValue(folderConfig.folderId());
        bindList.extend("dptName", "string");
        bindList["dptName"].setValue(folderConfig.dpTypeName());
        condition = std::string("F.FOLDERID = :folderId AND DPE.DPENAME = F.CHANNELIDINFO AND EL.SYS_ID = DPE.SYS_ID AND EL.DPE_ID = DPE.DPE_ID AND EL.DP_ID = DPE.DP_ID AND EL.SYS_ID = DPT.SYS_ID AND EL.DPT_ID = DPT.DPT_ID AND EL.DP_ID = DP.DP_ID AND EL.SYS_ID = DP.SYS_ID AND EL.EVENT = 1 AND EL.ALIAS IS NOT NULL AND DPT.DPTNAME = :dptName");

        // Check if the pattern is a single '*' (which means it is pointless to use it)
        pattern = folderConfig.pattern();
        if ((pattern.length() == 0) || ((pattern.length() == 1) && (pattern.compare("*") == 0))) {
        } else {
          bindList.extend("pattern", "string");
          iPos = pattern.find_first_of("*");
          while (iPos != std::string::npos) {
            pattern[iPos] = '%';
            iPos = pattern.find_first_of("*");
          }
          bindList["pattern"].setValue(pattern);
          condition += std::string(" AND DP.DPNAME LIKE :pattern");
        }
        folderInfo.setCondition(condition, bindList);

        // Set up the rest of the data elements query for pattern with ID as alias
        dataElements.addToOutputList("L.CHANNEL_ID", "CHANNEL_ID");
        dataElements.addToTableList("FOLDER_INFO", "L");
        coral::AttributeList folderList;
        folderList.extend("folderId", "long");
        folderList["folderId"].setValue(folderConfig.folderId());
        dataElements.setCondition("FI.FOLDERID = :folderId AND L.FOLDERID = :folderId AND L.FOLDERID = FI.FOLDERID AND EL.ELEMENT_NAME = L.DPNAME || NVL2(FI.DPELEMENTNAME, '.' || FI.DPELEMENTNAME, '')", folderList);
      }
    }

    // Again for ALL folder types:
	
	// Set the data from EVENTHISTORY table
    valueData.addToOutputList("EH.ELEMENT_ID", "ELEMENT_ID");
    valueData.addToOutputList("EH.TS", "TS");
    valueData.addToOutputList("ROUND((TO_DATE(TO_CHAR(EH.TS, 'yyyymmddhh24miss'), 'yyyymmddhh24miss') - TO_DATE('19700101000000', 'yyyymmddhh24miss')) * 86400)", "SECS"); 	// !!!

// New additional code 21.01.10 (SK), 
	if((folderConfig.dpTypeInfo() == FW_CONDDB_DPTINFO_FSM)	// FSM folder
	|| milliPrecision)			{							// or milliPrecision specified explicitly
		valueData.addToOutputList("(TO_CHAR(EH.TS, 'yyyymmddhh24missff3') - (TO_CHAR(EH.TS, 'yyyymmddhh24miss') * 1000))", "MILLI"); 	// !!!
	}
// End of new code
	
    valueData.addToOutputList("EH.VALUE_NUMBER", "VALUE_NUMBER");
    valueData.addToOutputList("EH.VALUE_STRING", "VALUE_STRING");
    valueData.addToOutputList("EH.VALUE_TIMESTAMP", "VALUE_TIMESTAMP");
    sprintf(cpTableName, "%sHISTORY_%08ld", folderConfig.group().data(), archiveNumber.archiveNumber());
    valueData.addToTableList(cpTableName, "EH");

    folderData.addToOutputList("/*+ LEADING(DE EHV) USE_NL(DE EHV) */ CHANNEL_ID");
// Was in original code
//    folderData.addToOutputList("(EHV.SECS * 1000)", "MILLISECS");
// New code 21.01.10 (SK)
	if((folderConfig.dpTypeInfo() == FW_CONDDB_DPTINFO_FSM)	// FSM folder
	|| milliPrecision)			{							// or milliPrecision specified explicitly
//    	std::cout << "Setting TS with msecs" << std::endl;
		folderData.addToOutputList("((EHV.SECS * 1000) + EHV.MILLI)", "MILLISECS");
	}
	else {
//		std::cout << "Setting TS WITHOUT msecs" << std::endl;
	    folderData.addToOutputList("(EHV.SECS * 1000)", "MILLISECS");
	}
// End of new code

// Was in original code
//    folderData.addToOutputList("TO_CHAR(EHV.TS, 'yyyymmddhh24miss')", "S_TIME");  // !!!

// New code 25.01.10 (SK) - Replaced 26.11.10, see below
//	if((folderConfig.dpTypeInfo() == FW_CONDDB_DPTINFO_FSM)	// FSM folder
//	|| milliPrecision)			{							// or milliPrecision specified explicitly
//	  folderData.addToOutputList("TO_CHAR(EHV.TS, 'yyyymmddhh24missff3')", "S_TIME");  // !!!
//	}
//	else
//    folderData.addToOutputList("TO_CHAR(EHV.TS, 'yyyymmddhh24miss')", "S_TIME"); 
// End of new code

// New code 26.11.10 (SK)
	folderData.addToOutputList("TO_CHAR(EHV.TS, 'yyyymmddhh24miss')", "S_TIME"); 
	folderData.addToOutputList("TO_CHAR(EHV.TS, 'yyyymmddhh24missff3')", "LONG_TIME");  
// End of new code

// New variable for the "collection" type folder
	std::vector<std::string> listOfFields;
	
    for (std::vector<FolderFieldConfiguration>::iterator iIter1 = folderFields.begin(); iIter1 != folderFields.end(); iIter1++) {
// New code for the "collection" type folder
	  std::vector<std::string>::const_iterator alreadyFound = std::find(listOfFields.begin(), listOfFields.end(), iIter1->fieldName());
	  if(alreadyFound == listOfFields.end()) {
//	    std::cout << "Field inserted " << iIter1->fieldName() << std::endl;
		listOfFields.push_back(iIter1->fieldName());
// End of new code
		sprintf(cpPosition, "%ld", iIter1->position());	
	  	if (iIter1->pvssFieldType().compare("NUMBER") == 0) {
// Old version   	     	outputString = "TO_NUMBER(";										
// New version with replacing NaN of Apr.2015
//			sprintf(cpPosition, "%ld", iIter1->position());		// Moved upper, before IF
//			outputString = "(CASE DECODE(DE.FIELD_POSITION,";
//			outputString += cpPosition;
//			outputString += ", EHV.VALUE_NUMBER) ";
//			outputString += "WHEN BINARY_DOUBLE_NAN THEN -999999 ELSE TO_NUMBER("; 
// End of new version of Apr.2015with replacing NaN (+ single line replaced below)

// Version with excluding also Infinity of 18.06.15
			outputString = "(CASE WHEN DECODE(DE.FIELD_POSITION,";
			outputString += cpPosition;
			outputString += ", EHV.VALUE_NUMBER) IS NAN THEN -999999 ";
			outputString += "WHEN DECODE(DE.FIELD_POSITION,";
			outputString += cpPosition;
			outputString += ", EHV.VALUE_NUMBER) IS INFINITE THEN -999999 ";
			outputString += "ELSE TO_NUMBER("; 
// End of version of 18.06.15
      	} else if (iIter1->pvssFieldType().compare("TIMESTAMP") == 0) {				
        	outputString = "TO_CHAR(";													
     	} else {																		
      	  outputString.erase();														
      	}																				
     	outputString += "DECODE(DE.FIELD_POSITION, ";									
      	outputString += cpPosition;													
     	outputString += std::string(", EHV.VALUE_");									
     	outputString += iIter1->pvssFieldType();										
      	outputString += std::string(", NULL)");										
																					
      	if (iIter1->pvssFieldType().compare("NUMBER") == 0) {							
//  replacing single line (for NaN/INF CASE)
//        	outputString += ")";												
        	outputString += ") END)";												
      	} else if (iIter1->pvssFieldType().compare("TIMESTAMP") == 0) {
       		outputString += ", 'yyyymmddhh24missff3')";
      	}
// 	 std::cout << "Adding to output list " << iIter1->fieldName() << std::endl;
      	folderData.addToOutputList(outputString, iIter1->fieldName());
	  }		// end } of new code above
    }

    folderData.addToTableList("VALUE_DATA", "EHV");
    folderData.addToTableList("DATA_ELEMENTS", "DE");

    coral::AttributeList timeBindList;
    timeBindList.extend("fromTime", "string");
    timeBindList.extend("toTime", "string");
    timeBindList["fromTime"].setValue(fromTime);
    timeBindList["toTime"].setValue(toTime);
    folderDataCondition = "EHV.TS >= TO_DATE(:fromTime, 'dd-mm-yyyy hh24:mi:ss') AND EHV.TS < TO_DATE(:toTime, 'dd-mm-yyyy hh24:mi:ss') AND EHV.ELEMENT_ID = DE.EID";
// Added 05.06.18	(if condition inserted 30.11.20 by SK from tag 05-06-01)
//  -- STRANGE! The condition is wrong/incorrect: removed 13.05.22.
// Due to this condition the /PIXEL/DCS/HV folder was filled with improper data after 26.01.21
//	if(folderConfig.dpTypeInfo() != FW_CONDDB_DPTINFO_ALCOLL)
		folderDataCondition = folderDataCondition + " AND ((EHV.TS BETWEEN DE.SINCE and DE.TILL) OR (EHV.TS >= DE.SINCE and DE.TILL is null))";
    folderData.setCondition(folderDataCondition, timeBindList);

    coral::AttributeList outputList;
    this->m_query->setDistinct();
    if ((folderConfig.channelIdType() == FW_CONDDB_IDINFO_ALIAS) || (folderConfig.channelIdType() == FW_CONDDB_IDINFO_USER_DEFINED)) {
      this->m_query->addToOutputList("CHANNEL_ID");
      outputList.extend<cool::ChannelId>("CHANNEL_ID");
    } else {
      this->m_query->addToOutputList("CHANNEL_ID");
      outputList.extend<std::string>("CHANNEL_ID");
    }
    this->m_query->addToOutputList("MILLISECS");
    outputList.extend<cool::ValidityKey>("MILLISECS");
	

// New single line as of 13.11.12 by SK (might be removed together with new code below)
    listOfFields.erase(listOfFields.begin(), listOfFields.end());
	for (std::vector<FolderFieldConfiguration>::iterator iIter1 = folderFields.begin(); iIter1 != folderFields.end(); iIter1++) {
// New code for the "collection" type folder
// This new code can be removed, because later the change in readFolderFieldConfigurationTable()
// has been made 16.11.12 in such a way that any duplication of the folder field name IS EXCLUDED
	  std::vector<std::string>::const_iterator alreadyFound = std::find(listOfFields.begin(), listOfFields.end(), iIter1->fieldName());
	  if(alreadyFound == listOfFields.end()) {
//	    std::cout << "Field inserted " << iIter1->fieldName() << std::endl;
		listOfFields.push_back(iIter1->fieldName());
// End of new code (still '}' see below)

        if (iIter1->pvssFieldType().compare("NUMBER") == 0) {
          outputString = "TO_NUMBER(";
          outputList.extend(iIter1->fieldName(), iIter1->coolFieldType());
        } else if (iIter1->pvssFieldType().compare("TIMESTAMP") == 0) {
          outputString = "TO_CHAR(";
          outputList.extend(iIter1->fieldName(), "string");
        } else {
          outputString.erase();
          outputList.extend(iIter1->fieldName(), "string");
        }
        outputString += "SUBSTR(MAX(DECODE(FD.";
        outputString += iIter1->fieldName();
// Was in original code
//      outputString += std::string(", NULL, NULL, FD.S_TIME || FD.");
// New code 26.11.10 (SK)
        outputString += std::string(", NULL, NULL, FD.LONG_TIME || FD.");
// End of new code

        outputString += iIter1->fieldName();
// Was in original code
//      outputString += std::string(")) OVER (PARTITION BY FD.CHANNEL_ID ORDER BY FD.S_TIME), 15)");  

// New code 25.01.10 (SK) - Replaced 26.11.10, see below
//	if((folderConfig.dpTypeInfo() == FW_CONDDB_DPTINFO_FSM)	// FSM folder
//	|| milliPrecision)			{							// or milliPrecision specified explicitly
//		outputString += std::string(")) OVER (PARTITION BY FD.CHANNEL_ID ORDER BY FD.S_TIME), 18)"); 
//	}
//	else
//		outputString += std::string(")) OVER (PARTITION BY FD.CHANNEL_ID ORDER BY FD.S_TIME), 15)");  
// End of new code
	
// New code 26.11.10 (SK)
  	    if((folderConfig.dpTypeInfo() == FW_CONDDB_DPTINFO_FSM)	// FSM folder
	    || milliPrecision)			{							// or milliPrecision specified explicitly
		  outputString += std::string(")) OVER (PARTITION BY FD.CHANNEL_ID ORDER BY FD.LONG_TIME), 18)"); 
	    }
	    else
          outputString += std::string(")) OVER (PARTITION BY FD.CHANNEL_ID ORDER BY FD.S_TIME), 18)");  
// End of new code
	
        if (iIter1->pvssFieldType().compare("NUMBER") == 0) {
          outputString += ")";
        } else if (iIter1->pvssFieldType().compare("TIMESTAMP") == 0) {
          outputString += ", 'yyyymmddhh24missff3')";
        }
        this->m_query->addToOutputList(outputString, iIter1->fieldName());
      }		// end '}' of new code as of 13.11.12 above
	}


    this->m_query->addToTableList("FOLDER_DATA", "FD");
    this->m_query->addToOrderList("CHANNEL_ID");
    this->m_query->addToOrderList("MILLISECS");
    this->m_query->defineOutput(outputList);
    this->m_query->setRowCacheSize(rowCacheSize);
	
    this->m_cursor = &(this->m_query->execute());

    tEnd = CondDBUtilities::getTimeNow();
    readTime = CondDBUtilities::getTimeDifference(tStart, tEnd);
  }

  //==================
  // CATCH EXCEPTIONS
  //==================
  catch ( coral::Exception& e )
  {
    std::cout << "'PvssArchiveFolderData::getFolderData()' CORAL exception : " << e.what() << std::endl;
    throw;
  }
  catch ( std::exception& e )
  {
    std::cout << "'PvssArchiveFolderData::getFolderData()' Standard C++ exception : " << e.what() << std::endl;
    throw;
  }
  catch( ... )
  {
    std::cout << "'PvssArchiveFolderData::getFolderData()' Unknown exception ..." << std::endl;
    throw;
  }

  return (*(this->m_cursor));
}
