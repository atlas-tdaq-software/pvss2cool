#ifndef __PVSS_ARCHIVE_NUMBERS_H__  // Gate keeper code
#define __PVSS_ARCHIVE_NUMBERS_H__

// Include file for class used to store PVSS archive numbers with start and
// end times
//
// Created: 27.03.2007
// Author:  Jim Cook

// Include Files
// -------------
#include <string>

// Class Definitions
// -----------------
class PvssArchiveNumber {
  public:
    PvssArchiveNumber();
    PvssArchiveNumber(long arcNumber,
                       std::string timeStart,
                       std::string timeEnd = std::string(""));

    ~PvssArchiveNumber() {;}

    PvssArchiveNumber(const PvssArchiveNumber &old);
    PvssArchiveNumber& operator=(const PvssArchiveNumber &rhs);
    bool operator==(const PvssArchiveNumber &rhs);

    // Getters
    inline long archiveNumber() {return (m_arcNumber);}
    inline std::string startTime() {return (m_startTime);}
    inline std::string endTime() {return (m_endTime);}

    // Setters
    inline void startTime(std::string timeStart) {m_startTime = timeStart;}
    inline void endTime(std::string timeEnd) {m_endTime = timeEnd;}

  private:
    long m_arcNumber;

    std::string m_startTime;
    std::string m_endTime;
};

#endif // End of gate keeper code '__PVSS_ARCHIVE_NUMBERS_H__'
