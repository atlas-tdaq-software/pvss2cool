#ifndef __PVSS_COOL_DEFAULT_CONNECTIONS_H__ // Gate keeper code
#define __PVSS_COOL_DEFAULT_CONNECTIONS_H__

#define PVSS_COOL_PVSS_CONNECTION "PVSSForCondDBConnection"
#define PVSS_COOL_COOL_CONNECTION "COOLForCondDBConnection"
#define PVSS_COOL_CONFIGURATION_CONNECTION "PVSSCOOLForConfigConnection"
#define PVSS_COOL_STATISTICS_CONNECTION "PVSSCOOLForStatsConnection"

#endif // End of gate keeper code '__PVSS_COOL_DEFAULT_CONNECTIONS_H__'
