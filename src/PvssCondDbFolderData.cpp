// Include files
#include "CoolKernel/Exception.h"
#include "CoolKernel/IDatabaseSvc.h"
#include "CoolKernel/IDatabase.h"
#include "CoolKernel/IFolder.h"
#include "CoolKernel/IObject.h"
#include "CoolKernel/IObjectIterator.h"
#include "CoolKernel/RecordSpecification.h"
#include "CoolKernel/Record.h"
#include "CoolKernel/ChannelSelection.h"
//#include "CoolKernel/Time.h"
#include "CoolApplication/Application.h"
#include "RelationalAccess/IAuthenticationService.h"
#include "RelationalAccess/IAuthenticationCredentials.h"
#include "RelationalAccess/ICursor.h"

// Commented moved to private headers 14.10.10 by SK
//#include "CoralBase/Attribute.h"
//#include "CoralBase/AttributeList.h"
//#include "CoralBase/TimeStamp.h"

#include <iostream>
#include <exception>
#include <list>

// Application Specific Include Files
// ==================================
#include "globals.h"
#include "PvssCoolDefaultConnections.h"
#include "PvssCondDbFolderData.h"
#include "CondDBUtilities.h"

#ifdef PVSS2COOL_CLIENT_TIMING
  #include <math.h>
#endif

// Message output
#define LOG std::cout << "__PvssCondDbFolderData "

//-----------------------------------------------------------------------------

PvssCondDbFolderData::PvssCondDbFolderData()
{
  return;
}

PvssCondDbFolderData::~PvssCondDbFolderData()
{
  return;
}

bool PvssCondDbFolderData::connect(cool::IDatabaseSvc &coolService,
                                   std::string serviceName,
                                   std::string dbName)
{
  bool status = true;

  std::string connectionString = serviceName + std::string("(") + FW_CONDDB_WRITER + std::string (")/") + dbName;

  try {

    this->m_connection = coolService.openDatabase(connectionString, false);

  }

  //==================
  // CATCH EXCEPTIONS
  //==================
  catch( cool::Exception& e ) 
  {
    LOG << "'connect()' COOL exception: '" << e.what() << "'" << std::endl;
    status = false;
  }
  catch( std::exception& e )
  {
    LOG << "'connect()' Standard C++ exception: '" << e.what() << "'" << std::endl;
    status = false;
  }
  catch( ... ) 
  {
    LOG << "'connect()' Unknown exception caught" << std::endl;
    status = false;
  }

  return (status);
}

bool PvssCondDbFolderData::disconnect()
{
  cool::IDatabasePtr dbNull;
  this->m_connection = dbNull;
  return (true);
}

bool PvssCondDbFolderData::writeFolder(std::string folderName,
                                       coral::ICursor &pvssData,
                                       long channelIdType,
                                       int &iovCount,
                                       double &insertTime)
{
  bool status = true;
  bool haveValues = false;
  bool channelIdString = false;
  bool newChannel = true;

  cool::ChannelId lastChannelId = 0;
  cool::ChannelId channelId = 0;
  cool::ChannelId channelNumber = 0;

  std::string sChannelId = "";
  std::string sLastChannelId = "";
  std::string sTemp;

  cool::Record lastValues;

  cool::ValidityKey tempTime;
  cool::ValidityKey sinceTime = 0;
  cool::ValidityKey untilTime = 0;
  cool::ValidityKey lastUpdate = 0;
  cool::ValidityKey vkLast = cool::ValidityKeyMax - 1;

  cool::IObjectPtr lastObject;

  coral::TimeStamp tStart;
  coral::TimeStamp tEnd;
	
  // Trying to write to COOL (SK)
//  std::cout << "Trying to write to COOL (" << folderName << ")" << std::endl;
  try {
    iovCount = 0;
    insertTime = 0.0;

    tStart = CondDBUtilities::getTimeNow();
    std::cout << "Entering data into COOL folder " << folderName << ", time is " << CondDBUtilities::formatTime(tStart, false) << std::endl;

    // Get first row (if there are any)
    if (pvssData.next()) {
	  // Data row found (SK)
//	  std::cerr << "Data row found" << std::endl;
      const coral::AttributeList& firstRow = pvssData.currentRow();

      // Get information from COOL folder
      cool::IFolderPtr folderPtr = this->m_connection->getFolder(folderName);

      if ((channelIdType == FW_CONDDB_IDINFO_DPNAME) ||
          (channelIdType == FW_CONDDB_IDINFO_COMMENT) ||
          (channelIdType == FW_CONDDB_IDINFO_ALIAS_STRING) ||
          (channelIdType == FW_CONDDB_IDINFO_USER_DEFINED_STRING)) {
        channelIdString = true;
        std::vector<cool::ChannelId> folderChannels = folderPtr->listChannels();
        cool::ChannelId tempChannel = 0;

        // Find the maximum channel number currently used
        for (std::vector<cool::ChannelId>::iterator iIter = folderChannels.begin(); iIter != folderChannels.end(); iIter++) {
          if (*iIter > tempChannel)
            tempChannel = *iIter;
        }

        // Ensure the 'next' channel ID/number used is greater than the maximum found
        channelNumber = tempChannel + 1;
      }

      // Get attribute list specification and set up storage buffer
      cool::Record payload(folderPtr->payloadSpecification());
      folderPtr->setupStorageBuffer();

      // Set known data
      if (channelIdString) {
        sChannelId = firstRow["CHANNEL_ID"].data<std::string>();

        // Get last values (if there are any)
        if (folderPtr->existsChannel(sChannelId)) {
          channelId = folderPtr->channelId(sChannelId);
          haveValues = true;
        } else {
          // Need to add channel
          channelId = channelNumber;
          channelNumber++;
          folderPtr->createChannel(channelId, sChannelId);
          haveValues = false;
        }
      } else {
        channelId = firstRow["CHANNEL_ID"].data<cool::ChannelId>();
        if (folderPtr->existsChannel(channelId))
          haveValues = true;
        else
          haveValues = false;
      }

      // Get last values (if there are any)
      if (haveValues) {
        try {
          lastObject = folderPtr->findObject(vkLast, channelId);
          lastValues = lastObject->payload();
          lastUpdate = lastObject->since();
        }
        catch (cool::ObjectNotFound& e) { // Check for object not found, which just means there isn't an open IOV
          lastUpdate = 0;
        }
        catch( ... ) { // Pass on any other exception to higher level to be handled there
          throw;
        }
      } else {
        lastUpdate = 0;
      }
      lastChannelId = channelId;
      sLastChannelId = sChannelId;
      tempTime = firstRow["MILLISECS"].data<cool::ValidityKey>();
      sinceTime = tempTime * 1000000;

      // Set the rest of the payload data to be stored
      CondDBUtilities::copyRow(payload, firstRow, lastValues, lastUpdate);
      iovCount++;

#ifdef PVSS2COOL_CLIENT_TIMING
      std::cout << " About to enter loop over rows - " << CondDBUtilities::formatTime(CondDBUtilities::getTimeNow(), true) << std::endl;
#endif
	  // Looking for next row (SK)
      while (pvssData.next()) {
//        std::cerr << "Next row found" << std::endl;
		const coral::AttributeList& row = pvssData.currentRow();
        if (channelIdString) {
          sChannelId = row["CHANNEL_ID"].data<std::string>();
          if (sChannelId == sLastChannelId)
            newChannel = false;
          else
            newChannel = true;
        } else {
          channelId = row["CHANNEL_ID"].data<cool::ChannelId>();
          if (channelId == lastChannelId)
            newChannel = false;
          else
            newChannel = true;
        }

        if (newChannel) {
          // New channel ID. Check if any data has been entered in the folder yet
          if (channelIdString) {
            sChannelId = row["CHANNEL_ID"].data<std::string>();

            // Get last values (if there are any)
            if (folderPtr->existsChannel(sChannelId)) {
              channelId = folderPtr->channelId(sChannelId);
              haveValues = true;
            } else {
              // Need to add channel
              channelId = channelNumber;
              channelNumber++;
              folderPtr->createChannel(channelId, sChannelId);
              haveValues = false;
            }
          } else {
            channelId = row["CHANNEL_ID"].data<cool::ChannelId>();
            if (folderPtr->existsChannel(channelId))
              haveValues = true;
            else
              haveValues = false;
          }

          if (newChannel && haveValues) {
            try {
              lastObject = folderPtr->findObject(vkLast, channelId);
              lastValues = lastObject->payload();
              lastUpdate = lastObject->since();
            }
            catch (cool::ObjectNotFound& e) { // Check for object not found, which just means there isn't an open IOV
              lastUpdate = 0;
            }
            catch( ... ) { // Pass on any other exception to higher level to be handled there
              throw;
            }
          } else if (newChannel && !haveValues) {
            lastUpdate = 0;
          }

          // Set the until time as 'open' (as the last row found was the last for that channelId)
          untilTime = cool::ValidityKeyMax;
        } else {
          tempTime = row["MILLISECS"].data<cool::ValidityKey>();
          untilTime = tempTime * 1000000;
          lastUpdate = sinceTime;
        }

        // Store this row (which contains data from the 'last' result row)
        folderPtr->storeObject(sinceTime, untilTime, payload, lastChannelId);
        lastChannelId = channelId;
        sLastChannelId = sChannelId;
        lastValues = payload;

        // Now get data for this row
        tempTime = row["MILLISECS"].data<cool::ValidityKey>();
        sinceTime = tempTime * 1000000;
        CondDBUtilities::copyRow(payload, row, lastValues, lastUpdate);
        iovCount++;

#ifdef PVSS2COOL_CLIENT_TIMING
        if (fmod((float)iovCount, 1000.0) == 0.0)
          std::cout << " Processed " << iovCount << " rows - " << CondDBUtilities::formatTime(CondDBUtilities::getTimeNow(), true) << std::endl;
#endif
      } // End of while loop over rows

      // Store last row found (with 'open' until time) and flush buffer
      folderPtr->storeObject(sinceTime, cool::ValidityKeyMax, payload, channelId);
#ifdef PVSS2COOL_CLIENT_TIMING
      std::cout << " Flushing storage buffer - " << CondDBUtilities::formatTime(CondDBUtilities::getTimeNow(), true) << std::endl;
#endif
      folderPtr->flushStorageBuffer();
#ifdef PVSS2COOL_CLIENT_TIMING
      std::cout << " Storage buffer flushed - " << CondDBUtilities::formatTime(CondDBUtilities::getTimeNow(), true) << std::endl;
#endif

    } // End if for any data

    tEnd = CondDBUtilities::getTimeNow();
    insertTime = CondDBUtilities::getTimeDifference(tStart, tEnd);
  }

  //==================
  // CATCH EXCEPTIONS
  //==================
  catch( cool::Exception& e ) 
  {
    LOG << "'writeFolder()' COOL exception: '" << e.what() << "'" << std::endl;
    status = false;
  }
  catch( std::exception& e )
  {
    LOG << "'writeFolder()' Standard C++ exception: '" << e.what() << "'" << std::endl;
    status = false;
  }
  catch( ... ) 
  {
    LOG << "'writeFolder()' Unknown exception caught" << std::endl;
    status = false;
  }

  return (status);
}

bool PvssCondDbFolderData::writeFolderSmoothed(std::string folderName,
                                               coral::ICursor &pvssData,
                                               std::vector<FolderFieldConfiguration>& folderFields,
                                               long channelIdType,
                                               int &iovCountStored,
                                               double &insertTime)
{
  bool status = true;
  bool haveValues = false;
  bool channelIdString = false;
  bool newChannel = true;
  bool firstTime = true;
  bool storeRow = false;
  bool bufferEmpty = true;

  cool::ChannelId lastChannelId = 0;
  cool::ChannelId channelId = 0;
  cool::ChannelId channelNumber = 0;

  std::string sChannelId = "";
  std::string sLastChannelId = "";
  std::string sTemp;

  cool::Record lastValues;

  cool::ValidityKey tempTime;
  cool::ValidityKey sinceTime = 0;
  cool::ValidityKey untilTime = 0;
  cool::ValidityKey vkLast = cool::ValidityKeyMax - 1;
  cool::ValidityKey lastUpdate = 0;

  cool::IObjectPtr lastObject;

  coral::TimeStamp tStart;
  coral::TimeStamp tEnd;
  
  // Debug print
//  std::cerr << "Folder fields' size of " << folderName << " = " << folderFields.size() << std::endl;
//  for(int i=0; i < folderFields.size(); i++)
//  	folderFields[i].printtoerr();

  // Trying to write to COOL (SK comment)
  try {
    int iovCount = 0;
    iovCountStored = 0;
    insertTime = 0.0;

    tStart = CondDBUtilities::getTimeNow();
//    std::cout << "Entering data into COOL folder" << folderName << " , time is " + CondDBUtilities::formatTime(tStart, false) << std::endl;

    // Get information from COOL folder
    cool::IFolderPtr folderPtr = this->m_connection->getFolder(folderName);

    if ((channelIdType == FW_CONDDB_IDINFO_DPNAME) ||
        (channelIdType == FW_CONDDB_IDINFO_COMMENT) ||
        (channelIdType == FW_CONDDB_IDINFO_ALIAS_STRING) ||
        (channelIdType == FW_CONDDB_IDINFO_USER_DEFINED_STRING)) {
      channelIdString = true;
      std::vector<cool::ChannelId> folderChannels = folderPtr->listChannels();
	  
//	  std::cout << "Folder has " << folderChannels.size() << " channels" << std::endl;
	  
      cool::ChannelId tempChannel = 0;

      // Find the maximum channel number currently used
      for (std::vector<cool::ChannelId>::iterator iIter = folderChannels.begin(); iIter != folderChannels.end(); iIter++) {
        if (*iIter > tempChannel)
          tempChannel = *iIter;
      }

      // Ensure the 'next' channel ID/number used is greater than the maximum found
      channelNumber = tempChannel + 1;
    }

    // Get attribute list specification and set up storage buffer
    cool::Record payload(folderPtr->payloadSpecification());
    folderPtr->setupStorageBuffer();

#ifdef PVSS2COOL_CLIENT_TIMING
      std::cout << " About to enter loop over rows - " << CondDBUtilities::formatTime(CondDBUtilities::getTimeNow(), true) << std::endl;
#endif
	// DBG count variable
	int dcsDataCount = 0;
    // Get row (if there are any)
    while (pvssData.next()) {
	  // Data row found (SK)
	  // Dbg count
	  dcsDataCount++;
	  
//	  std::cerr << "Data row found" << std::endl;
      const coral::AttributeList& row = pvssData.currentRow();

      // Check for new channel ID/name
      if (channelIdString) {
        sChannelId = row["CHANNEL_ID"].data<std::string>();
        if (!firstTime) {
          if (sChannelId == sLastChannelId)
            newChannel = false;
          else
            newChannel = true;
        }

        if (newChannel) {
          // Check if channel exists (meaning there is existing data)
          if (folderPtr->existsChannel(sChannelId)) {
            channelId = folderPtr->channelId(sChannelId);
            haveValues = true;
          } else {
            // Need to add channel
            channelId = channelNumber;
            channelNumber++;
            folderPtr->createChannel(channelId, sChannelId);
            haveValues = false;
          }
        }
      } else {
        channelId = row["CHANNEL_ID"].data<cool::ChannelId>();
        if (!firstTime) {
          if (channelId == lastChannelId)
            newChannel = false;
          else
            newChannel = true;
        }
        if (newChannel) {
          // Check if channel exists (meaning there is existing data)
          if (folderPtr->existsChannel(channelId))
            haveValues = true;
          else
            haveValues = false;
        }
      }

      // Get last values (if there are any)
      if (newChannel && haveValues) {
        try {
          lastObject = folderPtr->findObject(vkLast, channelId);
          lastValues = lastObject->payload();
          lastUpdate = lastObject->since();
        }
        catch (cool::ObjectNotFound& e) { // Check for object not found, which just means there isn't an open IOV
          lastUpdate = 0;
        }
        catch( ... ) { // Pass on any other exception to higher level to be handled there
          throw;
        }
      } else if (newChannel && !haveValues) {
        lastUpdate = 0;
      }

//      std::cerr << "First time = " << firstTime << ", " << bufferEmpty << ", " << newChannel << ", " << storeRow << ", " << lastUpdate << std::endl;

      if (firstTime) {
        firstTime = false;

        // Check current row to see if it should be stored
// Debug msg. 01.06.10
//		std::cerr << "1: check currentRow " << std::endl;
		
        storeRow = CondDBUtilities::doStore(row, lastValues, folderFields, lastUpdate);
// Debug msg. 01.06.10
//		std::cerr << "Should be stored = ";
//		if(storeRow) std::cerr << "TRUE";
//		else std::cerr << "FALSE";
//		std::cerr << std:: endl;
				
      } else {
        if (newChannel && !bufferEmpty) {
          // This is when we need to store the last row of the last channel found
          // Set the until time as 'open' (as the last row found was the last for that channelId)
          untilTime = cool::ValidityKeyMax;

          // We now need to know whether to store the current row (which is the first of a
          // different/new channel)
		
          storeRow = CondDBUtilities::doStore(row, lastValues, folderFields, lastUpdate);
// Debug msg. 01.06.10
//		std::cerr << "Should be stored = ";
//		if(storeRow) std::cerr << "TRUE";
//		else std::cerr << "FALSE";
//		std::cerr << std:: endl;
				
        } else {
          // Check current row to see if it should be stored
// Debug msg. 01.06.10
//		std::cerr << "3: check currentRow " << std::endl;
		
          storeRow = CondDBUtilities::doStore(row, lastValues, folderFields, lastUpdate);
// Debug msg. 01.06.10
//		std::cerr << "Should be stored = ";
//		if(storeRow) std::cerr << "TRUE";
//		else std::cerr << "FALSE";
//		std::cerr << std:: endl;
				
          if (storeRow) {
            if (bufferEmpty) {
              // This is the first set of values for this channel, so we need a since time
              tempTime = row["MILLISECS"].data<cool::ValidityKey>();
              sinceTime = tempTime * 1000000;

              // We need another row for this channel, or a new channel before the data is stored
              untilTime = 0;
            } else {
              // Set until time
              tempTime = row["MILLISECS"].data<cool::ValidityKey>();
              untilTime = tempTime * 1000000;
            }
          } else {
            untilTime = 0;
          }
        }

// Debug msg. 01.06.10
//        std::cout << storeRow << " 4 , Last Update " << lastUpdate << std::endl;

        // Store 'last' row if necessary
        if (untilTime > 0) {
          // This row needs to be stored
          folderPtr->storeObject(sinceTime, untilTime, payload, lastChannelId);
          bufferEmpty = true;
          iovCountStored++;
        }
      }

//      row.toOutputStream(std::cout);
//      std::cout << std::endl;

      if (storeRow) {
// Debug msg. 02.06.10
//		std::cerr << "This row will be stored" << std::endl;
// 3 lines Debug msg. 02.06.10
//    	std::cerr << "payload: " << std::endl;
//		payload.print(std::cerr);
//   	std::cerr << std::endl;

		CondDBUtilities::copyRow(payload, row, lastValues, lastUpdate);
		
// Debug msg. 02.06.10
//		std::cerr << "after copyRow" << std::endl;
//        std::cerr << "payload: " << std::endl;
//		payload.print(std::cerr);
//        std::cerr << std::endl;
        bufferEmpty = false;
        tempTime = row["MILLISECS"].data<cool::ValidityKey>();
        sinceTime = tempTime * 1000000;
        lastUpdate = sinceTime;
        lastValues = payload;
      }
	  else {
//	  	std::cerr << "This row will NOT be stored " << std::endl;
	  }
      iovCount++;
      lastChannelId = channelId;
      sLastChannelId = sChannelId;

#ifdef PVSS2COOL_CLIENT_TIMING
        if (fmod((float)iovCount, 1000.0) == 0.0)
          std::cout << " Processed " << iovCount << " rows - " << CondDBUtilities::formatTime(CondDBUtilities::getTimeNow(), true) << std::endl;
#endif
    } // End of while loop over rows

    // Store last row found (with 'open' until time) and flush buffer
    if (!bufferEmpty) {
      folderPtr->storeObject(sinceTime, cool::ValidityKeyMax, payload, channelId);
      iovCountStored++;
    }

#ifdef PVSS2COOL_CLIENT_TIMING
    std::cout << " Flushing storage buffer - " << CondDBUtilities::formatTime(CondDBUtilities::getTimeNow(), true) << std::endl;
#endif
    folderPtr->flushStorageBuffer();
#ifdef PVSS2COOL_CLIENT_TIMING
    std::cout << " Storage buffer flushed - " << CondDBUtilities::formatTime(CondDBUtilities::getTimeNow(), true) << std::endl;
#endif
	// Dbg msg
	// std::cerr << dcsDataCount << " rows of DCS data handled" << std::endl;
	
    tEnd = CondDBUtilities::getTimeNow();
    insertTime = CondDBUtilities::getTimeDifference(tStart, tEnd);
  }

  //==================
  // CATCH EXCEPTIONS
  //==================
  catch( cool::Exception& e ) 
  {
    LOG << "'writeFolderSmoothed()' COOL exception: '" << e.what() << "'" << std::endl;
    status = false;
  }
  catch( std::exception& e )
  {
    LOG << "'writeFolderSmoothed()' Standard C++ exception: '" << e.what() << "'" << std::endl;
    status = false;
  }
  catch( ... ) 
  {
    LOG << "'writeFolderSmoothed()' Unknown exception caught" << std::endl;
    status = false;
  }

  return (status);
}

bool PvssCondDbFolderData::closeIOVs(std::string folderName,
                                     std::string time,
                                     long &iovsClosed)
{
  bool status = true;

  cool::ValidityKey vkLast = cool::ValidityKeyMax - 1;

  try {

    iovsClosed = 0;

    // Get information from COOL folder
    cool::IFolderPtr folderPtr = this->m_connection->getFolder(folderName);

    // Get last values of all channels
    cool::IObjectIteratorPtr lastObjects = folderPtr->findObjects(vkLast, cool::ChannelSelection::all());

    // Check at least one object has been returned
    if (!lastObjects->isEmpty()) {

      // Setup storage buffer and get the correct ValidityKey valid of the end time
      folderPtr->setupStorageBuffer();
      cool::ValidityKey closeTime = CondDBUtilities::convertTime(time);

      // Loop through all channels that have open IOVs
      while (lastObjects->goToNext()) {
        const cool::IObject& object = lastObjects->currentRef();
        cool::ValidityKey sinceToUse;
        cool::ValidityKey untilToUse;
        // Check that requested 'close' time is valid
        if (closeTime <= object.since()) {
          // Just add one second to make the IOV valid
          sinceToUse = object.since() + 1000000000;
        } else {
          sinceToUse = closeTime;
        }
        // Make the until time 1 second greater than the since time
        untilToUse = sinceToUse + 1000000000;
        folderPtr->storeObject(sinceToUse, untilToUse, object.payload(), object.channelId());
        iovsClosed++;
      }

      // Flush all data to the COOL folder
      folderPtr->flushStorageBuffer();
    }
    lastObjects->close();
  }

  //==================
  // CATCH EXCEPTIONS
  //==================
  catch( cool::Exception& e ) 
  {
    LOG << "'closeIOVs()' COOL exception: '" << e.what() << "'" << std::endl;
    status = false;
  }
  catch( std::exception& e )
  {
    LOG << "'closeIOVs()' Standard C++ exception: '" << e.what() << "'" << std::endl;
    status = false;
  }
  catch( ... ) 
  {
    LOG << "'closeIOVs()' Unknown exception caught" << std::endl;
    status = false;
  }

  return (status);
}
