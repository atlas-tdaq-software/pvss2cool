// ----------------------------------------------------------------
// File:				  ArgParser.cpp
//
// Description:	  This file contains code defining a C++ class that
//						    can be used for translating a set of command line
//						    arguments into various different usable formats.
//
// Author:			  J.R.Cook
// Creation Date:	26/02/2001
// ----------------------------------------------------------------
// Revision History
// ================
//  Version | Inits |    Date    |           Comment
// ---------|-------|------------|---------------------------------
//     A    |  JRC  | 26/02/2001 | Created
//     B    |  JRC  | 01/08/2006 | Updated to use std::string instead
//          |       |            |   of char

// Include Files
// -------------
#include <iostream>
#include "ArgParser.h"

// ----------------------------------------------------------------
// Function:		ArgParser
//
// Description:	Constructor for argument parser class. Initialises
//						  member variables to known values and optionally
//						  enters information into the class.
//
// Returns:			None
// ----------------------------------------------------------------
ArgParser::ArgParser(std::string validFlags) /* = std::string("") */
: m_status(uninitialised)
{
// Local Variables
// ---------------
// None

// Executable Code
// ---------------
	// Attempt to add values to list
	this->setFlags(validFlags);

	// Set internal status if not already altered
	if (this->m_status == uninitialised) {
		this->m_status = initialised;
	}

	// Return to calling routine
	return;
}

// ----------------------------------------------------------------
// Function:		~ArgParser
//
// Description:	Destructor for argument parser class
//
// Returns:			None
// ----------------------------------------------------------------
ArgParser::~ArgParser()
{
// Local Variables
// ---------------
// None

// Executable Code
// ---------------
	// Uninitialise the status (even though it isn't really necessary)
	this->m_status = uninitialised;

	// Return to calling routine
	return;
}

// ----------------------------------------------------------------
// Function:		setFlags
//
// Description:	Sets values in Argument list to contain the flag
//						  identifiers given.
//							  First argument is a comma separated list of
//							   the flags required.
//							  Second argument is the expected (and maximum)
//							   number of argument elements to add.
//						  The function deletes any list that exists before
//						  the new values are inserted. This means that any
//              old list will be overwritten without warning.
//
// Returns:			number of argument elements actually added (may
//						  be zero)
//						  The function also sets the internal status to
//						  error if memory could not be allocated.
// ----------------------------------------------------------------
int ArgParser::setFlags(std::string flagList)
{
// Local Variables
// ---------------
	int iCount = 0;

  std::size_t i = 0;
  std::size_t iPos = 0;

#ifdef PVSSCOOL_ARG_CHAR
  ARG_TYPE newFlag;
  typedef std::pair <ARG_TYPE, Argument> Arg_Pair;
#else
  std::string newFlag;
  typedef std::pair <std::string, Argument> Arg_Pair;
#endif

// Executable Code
// ---------------
  // Clear the map
  this->m_mapArguments.clear();

  // Check some flags have been given 
  if (!flagList.empty()) {
    // Loop through string finding each flag
    while (iPos != std::string::npos) {
      iPos = flagList.find(",", i);
      if (iPos != std::string::npos) {
#ifdef PVSSCOOL_ARG_CHAR
        newFlag = flagList[i];
#else
        newFlag = flagList.substr(i, iPos - i);
#endif
        i = iPos + 1;
        iCount++;
      } else {
#ifdef PVSSCOOL_ARG_CHAR
        newFlag = flagList[i];
#else
        newFlag = flagList.substr(i);
#endif
        iCount++;
      }

#ifdef PVSSCOOL_ARG_CHAR
      if (newFlag != 0)
#else
      if (!newFlag.empty())
#endif
        this->m_mapArguments.insert(Arg_Pair(newFlag, Argument(newFlag)));
    }

//    for (std::map<std::string, Argument>::iterator iIter = this->m_mapArguments.begin(); iIter != this->m_mapArguments.end(); iIter++)
//      std::cout << "Flag '" << iIter->first << " contains the flag '" << iIter->second.flag() << "'" << std::endl;
  }

	// Return to calling routine
	return (iCount);
}

// ----------------------------------------------------------------
// Function:		setArgument
//
// Description:	Sets the value of the argument that correspoonds
//  						to the flag identifier given. If a value for the
//	  					argument is already held, the old value is
//		  				overwritten without warning.
//
// Returns:			true if argument stored successfully
//			  			false otherwise
// ----------------------------------------------------------------
#ifdef PVSSCOOL_ARG_CHAR
bool ArgParser::setArgument(std::string newArgument,
                            ARG_TYPE flagID)
#else
bool ArgParser::setArgument(std::string newArgument,
                            std::string flagID)
#endif
{
// Local Variables
// ---------------
	bool bStatus = false;

// Executable Code
// ---------------
  // Get pointer to flag
#ifdef PVSSCOOL_ARG_CHAR
  std::map<ARG_TYPE, Argument>::iterator iIter = this->m_mapArguments.find(flagID);
#else
  std::map<std::string, Argument>::iterator iIter = this->m_mapArguments.find(flagID);
#endif
  if (iIter != this->m_mapArguments.end()) {
    bStatus = true;
    iIter->second.argument(newArgument);
  }

	// Return to calling routine
	return (bStatus);
}


// ----------------------------------------------------------------
// Function:		processArguments
//
// Description:	Compares each argument flag in turn and if found
//						  in list, enters the corresponding argument value
//  						into the correct location.
//	  					The function uses the argc/argv parameters that
//		  				are given into main.
//
// Returns:			true if arguments processed successfully
//			  			false otherwise
// ----------------------------------------------------------------
void ArgParser::processArguments(char **argcpArgumentList,
                                 int argiArgumentCount)
{
// Local Variables
// ---------------
	int iLength = 0;
	int iIndex = 0;

#ifdef PVSSCOOL_ARG_CHAR
  ARG_TYPE newFlag = 0;
#else
  std::string newFlag;
#endif
  std::string newArgument;
// 1 line added for slc5-gcc43 15.04.10
  std::string stringArg;

	enum ProcessState {processStart, processEnd, flagFind, argFind} state;

// Executable Code
// ---------------
	// Check the argument given is valid
	if ((argcpArgumentList != 0) && (argiArgumentCount > 1)) {

		// Loop through all elements in argument list
		state = processStart;
		while (state != processEnd) {

			// Check at least one character exists in current element
			stringArg = argcpArgumentList[iIndex];
// Corrected 15.05.10 for slc5-gcc43
//			if ((iLength = strlen(argcpArgumentList[iIndex])) > 0) {
			if ((iLength = stringArg.size()) > 0) {

				switch (state) {
					case processStart:
						state = flagFind;	// Fall through
					case flagFind:

						// First character should be a hyphen '-'
						if (argcpArgumentList[iIndex][0] == '-') {

							// Store the flag text
#ifdef PVSSCOOL_ARG_CHAR
              newFlag = argcpArgumentList[iIndex][1];
#else
              newFlag = &argcpArgumentList[iIndex][1];
#endif
							state = argFind;
						}
						break;
					case argFind:

						// Check if another flag has been entered
						if (argcpArgumentList[iIndex][0] == '-') {

							// Set flag as found with no argument
              this->setArgument(std::string(""), newFlag);

							// Store the flag text
#ifdef PVSSCOOL_ARG_CHAR
              newFlag = argcpArgumentList[iIndex][1];
#else
              newFlag = &argcpArgumentList[iIndex][1];
#endif
						} else {

							// Store the argument text
              newArgument = argcpArgumentList[iIndex];

							// Enter the argument into the list
							this->setArgument(newArgument, newFlag);
							state = flagFind;
						}
						break;
					default:
						// An error must have occurred, so exit
						state = processEnd;
						break;
				}

				// Set flag as being found
				if (state == argFind)
          this->setArgument(std::string(""), newFlag);

				// Increment the index
				iIndex++;

				// Check for end of arguments
				if (iIndex >= argiArgumentCount)
					state = processEnd;
			}
		}
	}

	// Return to calling routine
	return;
}


// ----------------------------------------------------------------
// Function:		argumentFor
//
// Description:	Gives the argument stored for the flag ID specified
//
// Returns:			reference to string containing argument if found
//						  empty string if flag identifier or argument not stored
// ----------------------------------------------------------------
#ifdef PVSSCOOL_ARG_CHAR
Argument::FlagStatus ArgParser::argumentFor(ARG_TYPE flagID,
                                            std::string &theArgument)
#else
Argument::FlagStatus ArgParser::argumentFor(std::string flagID,
                                            std::string &theArgument)
#endif
{
// Local Variables
// ---------------
// None

// Executable Code
// ---------------
#ifdef PVSSCOOL_ARG_CHAR
  std::map<ARG_TYPE, Argument>::iterator iIter = this->m_mapArguments.find(flagID);
#else
  std::map<std::string, Argument>::iterator iIter = this->m_mapArguments.find(flagID);
#endif
  if (iIter != this->m_mapArguments.end()) {
    theArgument = iIter->second.argument();
    return (iIter->second.status());
  } else {
    theArgument.erase();
    return (Argument::unknown);
  }
}

// ----------------------------------------------------------------
// Function:		flagGiven
//
// Description:	Indicates whether flag identifier has been found
//						  on command line, irrespective of whether an
//						  argument was given with it.
//
// Returns:			true if flag found on command line
//						  false otherwise
// ----------------------------------------------------------------
#ifdef PVSSCOOL_ARG_CHAR
Argument::FlagStatus ArgParser::flagGiven(ARG_TYPE flagID)
#else
Argument::FlagStatus ArgParser::flagGiven(std::string flagID)
#endif
{
// Local Variables
// ---------------
// None

// Executable Code
// ---------------
#ifdef PVSSCOOL_ARG_CHAR
  std::map<ARG_TYPE, Argument>::iterator iIter = this->m_mapArguments.find(flagID);
#else
  std::map<std::string, Argument>::iterator iIter = this->m_mapArguments.find(flagID);
#endif
  if (iIter != this->m_mapArguments.end())
    return (iIter->second.status());
  else
    return (Argument::unknown);
}
