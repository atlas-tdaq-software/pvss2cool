#ifndef __PVSS_ARCHIVE_UTILITIES_H__  // Gate keeper code
#define __PVSS_ARCHIVE_UTILITIES_H__

// Include file for class giving various utilities to aid with extracting data
// from the PVSS Oracle archive.
//
// Created: 27.03.2007
// Author:  Jim Cook

// Include Files
// -------------
#include "CoralBase/Attribute.h"
#include "CoralBase/AttributeList.h"
#include "CoralBase/TimeStamp.h"

#include <vector>
#include <string>

// Forward Declarations
// --------------------
class PvssArchiveNumber;
// Commented 14.10.10 by SK
//class coral::ISessionProxy;

// Class Definitions
// -----------------
class PvssArchiveUtilities {
  public:
    PvssArchiveUtilities();
    ~PvssArchiveUtilities();

    static bool getArchiveNumbers(coral::ISessionProxy* connection,
                                  std::vector<PvssArchiveNumber> &archiveNumbers,
                                  std::string groupName,
                                  std::string timeStart,
                                  std::string timeEnd = std::string(""));

    static PvssArchiveNumber getCurrentArchive(coral::ISessionProxy* connection,
                                               std::string groupName);

    static long getElementId(coral::ISessionProxy* connection,
                                  long sysId,
                                  long dpId,
                                  long dpeId);
};

#endif // End of gate keeper code '__PVSS_ARCHIVE_UTILITIES_H__'
