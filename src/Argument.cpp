// ----------------------------------------------------------------
// File:				  Argument.cpp
//
// Description:	  This file contains code defining a C++ class that
//						    is used to store a flag identifier for a command
//						    line argument, and can hold a value of the
//						    corresponding argument.
//
// Author:			  J.R.Cook
// Creation Date:	02/03/2001
// ----------------------------------------------------------------
// Revision History
// ================
//  Version | Inits |    Date    |           Comment
// ---------|-------|------------|---------------------------------
//     A    |  JRC  | 02/03/2001 | Created
//     B    |  JRC  | 01/08/2006 | Updated to use std::string instead
//          |       |            |   of char

// Include Files
// -------------
#include "Argument.h"

// ----------------------------------------------------------------
// Function:		Argument
//
// Description:	Constructor for argument class. Initialises all
//						  member variables to known default values.
//
// Returns:			None
// ----------------------------------------------------------------
Argument::Argument()
: m_status(unknown)
{
  this->m_argument.erase();
#ifdef PVSSCOOL_ARG_CHAR
  this->m_flagID = 0;
#else
  this->m_flagID.erase();
#endif
	return;
}

// ----------------------------------------------------------------
// Function:		Argument
//
// Description:	Constructor for argument class. Initialises all
//						  member variables to known default values.
//
// Returns:			None
// ----------------------------------------------------------------
#ifdef PVSSCOOL_ARG_CHAR
Argument::Argument(ARG_TYPE flagID,
                   std::string newArgument) /* = std::string("") */
: m_status(unknown)
{
// Local Variables
// ---------------
//	None

// Executable Code
// ---------------
  this->m_argument.erase();
  this->m_flagID = 0;

  if (flagID != 0) {
    this->m_flagID = flagID;
    if (!newArgument.empty()) {
      this->m_argument = newArgument;
      this->m_status = stored;
    }
  }

	// Return to calling routine
	return;
}
#else
Argument::Argument(std::string flagID,
                   std::string newArgument) /* = std::string("") */
: m_status(unknown)
{
// Local Variables
// ---------------
//	None

// Executable Code
// ---------------
  this->m_argument.erase();
  this->m_flagID.erase();

  if (!flagID.empty()) {
    this->m_flagID = flagID;
    if (!newArgument.empty()) {
      this->m_argument = newArgument;
      this->m_status = stored;
    }
  }

	// Return to calling routine
	return;
}
#endif

// ----------------------------------------------------------------
// Function:		~Argument
//
// Description:	Destructor for argument class. Checks all values
//						  of member variables and frees memory as necessary.
//
// Returns:			None
// ----------------------------------------------------------------
Argument::~Argument()
{
// Local Variables
// ---------------
//	None

// Executable Code
// ---------------

	// Return to calling routine
	return;
}

// ----------------------------------------------------------------
// Function:		copy constructor
//
// Description:	Constructor to create a new instance using values
//              from an existing instance
//
// Returns:			None
// ----------------------------------------------------------------
Argument::Argument(const Argument &old)
{
  this->m_argument = old.m_argument;
	this->m_flagID = old.m_flagID;
	this->m_status = old.m_status;
  return;
}

// ----------------------------------------------------------------
// Function:		assignment operator
//
// Description:	Assignment operator to assign values of an existing
//              instance to a new instance
//
// Returns:			None
// ----------------------------------------------------------------
Argument& Argument::operator=(const Argument &rhs)
{
  this->m_argument = rhs.m_argument;
	this->m_flagID = rhs.m_flagID;
	this->m_status = rhs.m_status;
  return (*this);
}

// ----------------------------------------------------------------
// Function:		equivalence operator
//
// Description:	Checks to see if current instance has same flag ID
//              as that given
//
// Returns:			true if flag IDs are identical
//              false otherwise
// ----------------------------------------------------------------
#ifdef PVSSCOOL_ARG_CHAR
bool Argument::operator==(const ARG_TYPE myFlagID)
{
  if (this->m_flagID == myFlagID)
    return (true);
  else
    return (false);
}
#else
bool Argument::operator==(const std::string &myFlagID)
{
  if (this->m_flagID.compare(myFlagID) == 0)
    return (true);
  else
    return (false);
}
#endif

// ----------------------------------------------------------------
// Function:		argument
//
// Description:	Sets value of argument into member variable and
//              updates the status.
//
// Returns:			None
// ----------------------------------------------------------------
void Argument::argument(std::string newArgument)
{
// Local Variables
// ---------------
// None

// Executable Code
// ---------------
	// Check if an argument has been given
	if (newArgument.empty()) {
		// Set status to indicate flag has been found on command line but
		// no value given
		this->m_status = found;
    this->m_argument.erase();
	} else {
		// Copy the argument given
    this->m_argument = newArgument;

		// Set the status to indicate value is stored
		this->m_status = stored;
	}

	// Return status to calling routine
	return;
}
