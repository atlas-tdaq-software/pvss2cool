#ifndef __ARGPARSER_H__	// Gate-keeper code
#define __ARGPARSER_H__
// ----------------------------------------------------------------
// File:				  ArgParser.h
//
// Description:	  Include file for ArgParser class definition
//
// Author:			  J.R.Cook
// Creation Date:	26/02/2001
// ----------------------------------------------------------------
// Revision History
// ================
//  Version | Inits |    Date    |           Comment
// ---------|-------|------------|---------------------------------
//     A    |  JRC  | 26/02/2001 | Created
//     B    |  JRC  | 01/08/2006 | Updated to use std::string instead
//          |       |            |   of char

// Include Files
// -------------
#include <map>
#include <string>
#include "Argument.h"

// ArgParser class
// ---------------
class ArgParser {

public:
  ArgParser(std::string validFlags = std::string(""));
	~ArgParser();
	int setFlags(std::string flagList);	                // Initialises flag list with those given
	void processArguments(char **argcpArgumentList, int argiArgumentCount); // Processes argument list using flags stored

	// Functions to obtain class information
#ifdef PVSSCOOL_ARG_CHAR
  Argument::FlagStatus argumentFor(ARG_TYPE flagID, std::string &theArgument); // Gets argument stored for flag ID given
	Argument::FlagStatus flagGiven(ARG_TYPE flagID);                             // Indicates whether flag ID was given on command line
#else
  Argument::FlagStatus argumentFor(std::string flagID, std::string &theArgument); // Gets argument stored for flag ID given
	Argument::FlagStatus flagGiven(std::string flagID);                             // Indicates whether flag ID was given on command line
#endif

private:
	// enum used for internal error checking
	enum ArgParserStatus {uninitialised, initialised, success, error};

	// Member functions
#ifdef PVSSCOOL_ARG_CHAR
	bool setArgument(std::string newArgument, ARG_TYPE flagID);	// Sets argument for flag ID given
#else
	bool setArgument(std::string newArgument, std::string flagID);	// Sets argument for flag ID given
#endif

	// Member variables
#ifdef PVSSCOOL_ARG_CHAR
  std::map <ARG_TYPE, Argument> m_mapArguments;
#else
  std::map <std::string, Argument> m_mapArguments;
#endif
	ArgParserStatus m_status;
};

#endif // End of Gate-keeper code '__ARGPARSER_H__'
