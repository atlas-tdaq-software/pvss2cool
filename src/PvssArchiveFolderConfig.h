#ifndef __PVSS_ARCHIVE_FOLDER_CONFIG_H__  // Gate keeper code
#define __PVSS_ARCHIVE_FOLDER_CONFIG_H__

// Include file for class which extracts and stores required information
// needed for the configuration of the conditions database folders from
// the PVSS Oracle archive
//
// Created: 15.07.2005
// Author:  Jim Cook

// Include Files
// -------------
#include <string>
#include <vector>
#include "RelationalAccess/IConnectionService.h"
#include "RelationalAccess/ISessionProxy.h"
#include "RelationalAccess/ITableDescription.h"
#include "RelationalAccess/TableDescription.h"
#include "FolderConfigurationInformation.h"
#include "FolderFieldConfiguration.h"

// Constant Definitions
// --------------------
#define PVSS_COOL_ALL_FOLDERS -1
#define PVSS_COOL_FOLDER_CONFIGURATION_TABLE_R1 "PVSS2COOL_FOLDERS"
#define PVSS_COOL_FOLDER_CONFIGURATION_TABLE_R2 "PVSS2COOL_FOLDERS_R2"
#define PVSS_COOL_FOLDER_FIELD_CONFIGURATION_TABLE "PVSS2COOL_FIELDS"
#define PVSS_COOL_FOLDER_LIST_CONFIGURATION_TABLE "PVSS2COOL_LISTS"
#define PVSS_COOL_ATTRIBUTES_TABLE "PVSS2COOL_DB_ATTRIBUTES"
#define PVSS_COOL_FOLDER_DESC_SINGLE_CHANNEL_ZERO "<timeStamp>time</timeStamp><addrHeader><address_header service_type=\"71\" clid=\"40774348\" /></addrHeader><typeName>AthenaAttributeList</typeName>"
#define PVSS_COOL_FOLDER_DESC_MULTI_CHANNEL "<timeStamp>time</timeStamp><addrHeader><address_header service_type=\"71\" clid=\"1238547719\" /></addrHeader><typeName>CondAttrListCollection</typeName>"

#define TIME_PRECISION_SECONDS "Second"

// Class Definitions
// -----------------
class PvssArchiveFolderConfig {
  public:
    PvssArchiveFolderConfig();
    ~PvssArchiveFolderConfig();

    inline std::vector<FolderConfigurationInformation>& folderConfiguration() {return (m_folderConfiguration);}
    inline std::vector<FolderFieldConfiguration>& folderFieldConfiguration() {return (m_folderFieldConfiguration);}
    inline float schemaVersion() {return (m_schemaVersion);}

    bool connect(coral::IConnectionService& connectionService, std::string serviceName, bool useOwner = false);
    void disconnect();

    std::vector<FolderConfigurationInformation>& readFolderConfigurationTable(std::string folderConfigurationTable, long folderGroup = PVSS_COOL_ALL_FOLDERS, bool ignoreUpdateInterval = false, long withState = FW_CONDDB_FOLDERSTAT_EXISTS);
    std::vector<FolderFieldConfiguration>& readFolderFieldConfigurationTable(long folderId, long statusReq = FW_CONDDB_STATUS_ANY);

    void markAsRemoved(std::string folderConfigurationTable, 
					   std::vector<FolderConfigurationInformation>& toBeRemoved, bool remove = true);
    void changeStatus(std::string folderConfigurationTable, long folderId, long newStatus);
    void updateFields(long folderId);
    void updateLists(long folderId);

    bool updateLastUpdateTime(std::string folderConfigurationTable, long folderId, std::string updateTime, bool before = true);
    void decrementIntervalCounters(std::string folderConfigurationTable);
    void resetIntervalCounters(std::string folderConfigurationTable, bool forceReset = false);

  private:
    float m_schemaVersion;

    std::vector<FolderConfigurationInformation> m_folderConfiguration;
    std::vector<FolderFieldConfiguration> m_folderFieldConfiguration;

    coral::ISessionProxy *m_session;

//    void insertFoldersIntoTables(coral::ISessionProxy* connection, std::vector<FolderConfigurationInformation>& newFolders, std::vector<FolderConfigurationInformation>& toBeCreated, std::vector<FolderConfigurationInformation>& notToBeCreated);
    float readSchemaVersion();
// Commented 30.11.12 by SK to remove 'interval'
//    bool updateFolderTable(long folderId, bool useList, long interval, long channelIdType, std::string channelIdInfo, std::string dpPattern);
    bool updateFolderTable(std::string folderConfigurationTable, long folderId, bool useList, long channelIdType, std::string channelIdInfo, std::string dpPattern);
};

#endif // End of gate keeper code '__PVSS_ARCHIVE_FOLDER_CONFIG_H__'
