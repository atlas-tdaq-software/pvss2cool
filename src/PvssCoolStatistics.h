#ifndef __PVSS_COOL_STATISTICS_H__  // Gate keeper code
#define __PVSS_COOL_STATISTICS_H__

// Include file for class which calculates and stores statistical information about
// the PVSS-COOL process's performance.
//
// Created: 05.06.2008
// Author:  Jim Cook

// Include Files
// -------------
#include <string>
#include "RelationalAccess/IConnectionService.h"
#include "RelationalAccess/ISessionProxy.h"
#include "CoralBase/TimeStamp.h"

// Constant Definitions
// --------------------
#define PVSS_COOL_STATISTICS_TABLE "PVSS2COOL_STATISTICS"

// Class Definitions
// -----------------
class PvssCoolStatistics {
  public:
    PvssCoolStatistics();
    ~PvssCoolStatistics();

    void resetStatistics(coral::IConnectionService& connectionService,
                         std::string folderName);
    void setRunning(coral::IConnectionService& connectionService,
                    std::string folderName,
                    bool isRunning = true);
    void updateStatistics(coral::IConnectionService& connectionService,
                          std::string folderName,
                          int iovCount,
                          double readTime,
                          double insertTime,
                          int timeSpan,
                          coral::TimeStamp lastUpdate);
    void updateStatistics(coral::IConnectionService& connectionService,
                          std::string folderName,
                          int iovCount,
                          double readTime,
                          double insertTime,
                          int timeSpan,
                          std::string lastUpdate);

  private:
    // Member variables
    bool m_isRunning;
    bool m_isExisting;

    int m_avgIovsPerHour;
    int m_minIovsPerHour;
    int m_maxIovsPerHour;

    double m_avgReadTimeForHour;
    double m_minReadTimeForHour;
    double m_maxReadTimeForHour;
    double m_avgInsertTimeForHour;
    double m_minInsertTimeForHour;
    double m_maxInsertTimeForHour;
    double m_totalHours;

    std::string m_folderName;

    coral::TimeStamp m_lastUpdate;

    coral::ISessionProxy *m_session;

    // Member functions
    bool connect(coral::IConnectionService& connectionService);
    void disconnect();
    void initialise(std::string folderName = std::string(""));
    void readStatistics(std::string folderName);
    void writeStatistics();
    double calculateNewAverage(double currentAvg, double totalHours, double newCount, double timeSpan);
    double calculateNewAverage(double currentAvg, double totalHours, double newCount, int timeSpan);
};

#endif // End of gate keeper code '__PVSS_COOL_STATISTICS_H__'
