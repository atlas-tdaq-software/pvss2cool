#ifndef __PVSS_CONDDB_FOLDER_CONFIG_H__  // Gate keeper code
#define __PVSS_CONDDB_FOLDER_CONFIG_H__

// Include file for class which creates any necessary configuration in
// the conditions database before writing and data
//
// Created: 15.07.2005
// Author:  Jim Cook

// Include Files
// -------------
#include <list>
//#include "RelationalAccess/ISession.h"
#include "RelationalAccess/ISessionProxy.h"
#include "RelationalAccess/ISessionProperties.h"
#include "PvssArchiveFolderConfig.h"

// Class Definitions
// -----------------
class PvssCondDbFolderConfig {
  public:
    PvssCondDbFolderConfig();
    ~PvssCondDbFolderConfig();

    bool connect(cool::IDatabaseSvc &coolService, std::string serviceName, std::string dbName, bool readOnly = true);
    bool getFolders(std::vector<std::string> &folders);
    bool existsFolderAndIsDifferent(FolderConfigurationInformation folderInfo, std::vector<FolderFieldConfiguration>& folderFields, std::vector<std::string>& removed, std::vector<std::string>& valid, std::vector<std::string>& differentTypes, std::vector<std::string>& newField);
    bool createFolder(std::string folderName, std::string folderDescription, std::vector<FolderFieldConfiguration>& folderFields);
    bool dropFolder(std::string folderName);
    bool disconnect();

  private:

    cool::IDatabasePtr m_connection;
};

#endif // End of gate keeper code '__PVSS_CONDDB_FOLDER_CONFIG_H__'
