// Include files
#include "CoolKernel/Exception.h"
// Commented moved to private headers 14.10.10 by SK
//#include "CoralBase/Attribute.h"
//#include "CoralBase/AttributeList.h"
//#include "CoralBase/TimeStamp.h"
#include "CoralBase/VersionInfo.h"

//#include "CoralBase/Exception.h"
#include "CoolKernel/IRecordSpecification.h"
#include "SealBaseTime.h"

#include <iostream>
#include <stdio.h>
#include <exception>
#include <math.h>

#include "CondDBUtilities.h"
#include "FolderFieldConfiguration.h"

//-----------------------------------------------------------------------------

CondDBUtilities::CondDBUtilities()
{
  return;
}

CondDBUtilities::~CondDBUtilities()
{
  return;
}

//coral::TimeStamp CondDBUtilities::getTimeNow(bool isLocal /*= false*/)  replaced 29.09.14
coral::TimeStamp CondDBUtilities::getTimeNow()
{
  return coral::TimeStamp::now(); // CORAL default is also UTC (local=false)
}

std::string CondDBUtilities::addSeconds(std::string timeStart,
                                        int &timeSpan /* = 0 */)
{
  std::string timeEnd;

  // Use UTC times everywhere
// 29.09.14  bool isLocal = false;

  // Get the current UTC time
  coral::TimeStamp timeNow = coral::TimeStamp::now();

  // Subtract 1 minute from current time as PVSS Oracle archive may be lagging a bit behind real current time
#if CORAL_VERSIONINFO_RELEASE_MAJOR < 3
  timeNow = coral::TimeStamp(timeNow.time() - boost::posix_time::minutes(1));
//  std:: cerr << "Old-fashion timestamp" << std:: endl;
#else
  timeNow = coral::TimeStamp(timeNow.time() - std::chrono::duration<int>(60));  // -60 seconds
//  std:: cerr << "New-fashion timestamp" << std:: endl;
#endif  
  // Check if time span has been given
  if (timeSpan <= 0) {
    // Set end time to current time
    timeEnd = CondDBUtilities::formatTime(timeNow);
  } else {
    // Get time object from text
    coral::TimeStamp timeObject = CondDBUtilities::getTimeStamp(timeStart);
    // Add the timeSpan to the time object from text
#if CORAL_VERSIONINFO_RELEASE_MAJOR < 3
    coral::TimeStamp newTime(timeObject.time() + boost::posix_time::seconds(timeSpan));
#else
    coral::TimeStamp newTime(timeObject.time() + std::chrono::duration<int>(timeSpan));
#endif  
    // Check if the time would go into the future, and ensure it doesn't
    if (newTime.time() > timeNow.time()) {
	   newTime = timeNow;
	}
    // Set end time to smallest of current time and computed time
    timeEnd = CondDBUtilities::formatTime(newTime);
//    std::cerr << "timeEnd = " << timeEnd << std::endl;													
  }

  // Get actual time span (which could be different to that requested)
  timeSpan = (int)CondDBUtilities::getTimeDifference(CondDBUtilities::getTimeStamp(timeStart),
                                                     CondDBUtilities::getTimeStamp(timeEnd));

  return (timeEnd);
}

coral::TimeStamp CondDBUtilities::getTimeStamp(std::string time)
{
  int year, month, day, hour, min, sec;
  sscanf(time.data(), "%d-%d-%d %d:%d:%d", &day, &month, &year, &hour, &min, &sec); // month in [1,12]
  coral::TimeStamp tTime(year, month, day, hour, min, sec, 0l /*,false*/ ); // coral::TimeStamp uses month in [1,12] and UTC by default
  return (tTime);
}

double CondDBUtilities::getTimeDifference(coral::TimeStamp startTime,
                                          coral::TimeStamp endTime)
{
  double diffSeconds;
  // SEAL-free implementation 
#if CORAL_VERSIONINFO_RELEASE_MAJOR < 3
  boost::posix_time::time_duration tDiff = endTime.time() - startTime.time(); 
  diffSeconds = tDiff.total_nanoseconds() / 1000000000.0; // WARNING: total_nanoseconds() is a long, not a long long
#else 
  std::chrono::nanoseconds tDiff = endTime.time() - startTime.time();
//  if ( tDiff.is_negative() ) tDiff = tDiff.invert_sign();		// No need in that. P2C excludes negative intervals itself.
  diffSeconds = tDiff.count() / 1000000000.0; 
#endif

  return ( diffSeconds);
  
// Below is SEAL-based implementation. It gives incorrect result during moving 
// to Summer time. Commented and replaced by above 04.04.2012 by SK
/*
  double diffSeconds = 0.0;
  seal::Time tStart(startTime.year(), startTime.month() - 1, startTime.day(),
                    startTime.hour(), startTime.minute(), startTime.second(), startTime.nanosecond());
// Debug for DST
	std::cout << "time start: " << tStart << std::endl;
// End Debug for DST
  seal::Time tEnd(endTime.year(), endTime.month() - 1, endTime.day(),
                  endTime.hour(), endTime.minute(), endTime.second(), endTime.nanosecond());
// Debug for DST
	std::cout << "time end: " << tEnd << std::endl;
// End Debug for DST
  seal::TimeSpan tDiff = tEnd - tStart;
  diffSeconds = tDiff.ns() / 1000000000.0;
  return (diffSeconds);
*/
}

cool::ValidityKey CondDBUtilities::convertTime(coral::TimeStamp time)
{
  seal::Time tTime(time.year(), time.month() - 1, time.day(),
                   time.hour(), time.minute(), time.second(), time.nanosecond());
  return (tTime.ns());
}

cool::ValidityKey CondDBUtilities::convertTime(std::string time)
{
  coral::TimeStamp timeObject = CondDBUtilities::getTimeStamp(time);
  cool::ValidityKey convertedTime = CondDBUtilities::convertTime(timeObject);
  return (convertedTime);
}

std::string CondDBUtilities::formatTime(coral::TimeStamp time,
                                        bool milli /* = false */)
{
  std::string timeFormatted;
  char myTime[30];
  char myMilli[5];

  sprintf(myTime, "%02d-%02d-%04d %02d:%02d:%02d",
          time.day(), time.month(), time.year(),
          time.hour(), time.minute(), time.second());
  timeFormatted = myTime;
  
  if (milli) {
    sprintf(myMilli, ".%03d", (int)(time.nanosecond() / 1000000l));
    timeFormatted += myMilli;
  }

  return (timeFormatted);
}


void CondDBUtilities::copyRow(cool::Record& copyTo,
                              const coral::AttributeList& copyFrom,
                              const cool::Record& defaultValues,
                              cool::ValidityKey& lastUpdate)
{

  cool::UInt32 index;

  try {
    const cool::IRecordSpecification& spec = copyTo.specification();

    for (index = 0; index < copyTo.size(); index++) {

      const std::string& name = spec[index].name();
      const std::type_info& type = spec[index].storageType().cppType();
	  float fieldValue;
	  int intValue;
// Debug msg. 02.06.10
//	std::cerr << "name in copyRow = " << name << std::endl;

      if (!copyFrom[name].isNull()) {
// Debug msg. 02.06.10
//		std::cerr << "copyRow: NOT copyFrom[" << name << "].isNull()" << std::endl;
        if (type == typeid(float)) {
		  fieldValue = copyFrom[name].data<float>();
//		  std::cerr << "copyFrom[name].data<float> = " << fieldValue << std::endl;
		  if(fieldValue == -999999) {
//		    std::cerr << "Replacing NaN in " << name << std::endl;
		  	copyTo[name].setNull();
		  }
		  else
            copyTo[name].setValue(copyFrom[name].data<float>());
        } else if (type == typeid(int)) {
		  intValue = copyFrom[name].data<int>();
//		  std::cerr << "copyFrom[name].data<int> = " << intValue << std::endl;
		  if(intValue == -999999) {
//		    std::cerr << "Replacing NaN in " << name << std::endl;
		  	copyTo[name].setNull();
		  }
		  else
            copyTo[name].setValue(copyFrom[name].data<int>());
        } else if (type == typeid(unsigned int)) {
          copyTo[name].setValue(copyFrom[name].data<unsigned int>());
        } else if (type == typeid(std::string)) {
          copyTo[name].setValue(copyFrom[name].data<std::string>());
        } else if (type == typeid(bool)) {
          copyTo[name].setValue(copyFrom[name].data<bool>());
        } else {
          std::cout << "Unhandled type '" << type.name() << "' for field '" << name << std::endl;
        }
      } else if ((lastUpdate > 0) && !defaultValues[name].isNull()) {
// Debug msg. 02.06.10
//		std::cerr << "copyRow: copyFrom[" << name << "].isNull()" << std::endl;
        if (type == typeid(float)) {
          copyTo[name].setValue(defaultValues[name].data<float>());
        } else if (type == typeid(int)) {
          copyTo[name].setValue(defaultValues[name].data<int>());
        } else if (type == typeid(unsigned int)) {
          copyTo[name].setValue(defaultValues[name].data<unsigned int>());
        } else if (type == typeid(std::string)) {
          copyTo[name].setValue(defaultValues[name].data<std::string>());
        } else if (type == typeid(bool)) {
          copyTo[name].setValue(defaultValues[name].data<bool>());
        } else {
          std::cout << "Unhandled type '" << type.name() << "' for field '" << name << std::endl;
        }
      } else {
        	copyTo[name].setNull();
      }
    }
  }

  //==================
  // CATCH EXCEPTIONS
  //==================
  catch( cool::Exception& e ) 
  {
    std::cerr << "'CondDBUtilities::copyRow()' COOL exception : " << e.what() << std::endl;
    throw;
  }
  catch ( coral::Exception& e )
  {
    std::cerr << "'CondDBUtilities::copyRow()' CORAL exception : " << e.what() << std::endl;
    throw;
  }
  catch( std::exception& e )
  {
    std::cerr << "'CondDBUtilities::copyRow()' C++ exception : " << e.what() << std::endl;
    throw;
  }
  catch( ... ) 
  {
    std::cerr << "'CondDBUtilities::copyRow()' Unknown exception : " << std::endl;
    throw;
  }

  return;
}

bool CondDBUtilities::doStore(const coral::AttributeList& currentRow,
                              cool::Record& lastValues,
                              std::vector<FolderFieldConfiguration>& folderFields,
                              cool::ValidityKey& lastUpdate)
{

  bool storeRecord = false;
  long smoothSeconds = 0;
  double deadband = 0.0;
  long timeDiff = 0;
  cool::UInt32 index;

  try {

    // Get time difference between current row and last update in seconds (if we have a last update)
    if (lastUpdate > 0)
      timeDiff = (long)(currentRow["MILLISECS"].data<cool::ValidityKey>() / 1000) - (long)(lastUpdate / 1000000000);   // !!!
    else
      return (true);

    // If we reach this point, we now we have the last values, and therefore the folder specification
    // is held in the lastValues argument
    const cool::IRecordSpecification& spec = lastValues.specification();

    for (index = 0; (index < lastValues.size()) && !storeRecord; index++) {

      const std::string& name = spec[index].name();
      const std::type_info& type = spec[index].storageType().cppType();

      if (!currentRow[name].isNull()) {	/// !!! Change/cancel this for empty string?

        // Check for types that can have deadband and timeout
        if ((type == typeid(float)) || (type == typeid(int)) || (type == typeid(unsigned int))) {

          // Smoothing only makes sense if the 'last' value is not NULL
          if (lastValues[name].isNull()) {
            storeRecord = true;
          } else {
            // Values may be compared for smoothing, so get any deadband and timeout
			 std::vector<FolderFieldConfiguration>::iterator iIter = std::find(folderFields.begin(),folderFields.end(),name);
/*            std::vector<FolderFieldConfiguration>::iterator iIter;
			for(iIter = folderFields.begin(); iIter != folderFields.end(); iIter++) {
				if(iIter->fieldName() == name)
					break;
			}		// this can replace previous std::find
*/
            if (iIter != folderFields.end()) {
              smoothSeconds = iIter->timeout();
              deadband = iIter->deadband();
            } else {
              smoothSeconds = 0;
              deadband = 0.0;
            }

            if (deadband > 0) {
              // Check if timeout given and compare
              if ((smoothSeconds > 0) && (timeDiff > smoothSeconds)) {
                storeRecord = true;
              } else {
                // We only get here if deadband given and either no timeout given, or timeout still
                // within limits. Therefore, need to check for exceeding limits.
                if (type == typeid(float)) {
                  if (fabs(currentRow[name].data<float>() - lastValues[name].data<float>()) >= deadband)
                    storeRecord = true;
                } else if (type == typeid(int)) {
                  if (fabs((float)currentRow[name].data<int>() - (float)lastValues[name].data<int>()) >= deadband)
                    storeRecord = true;
                } else {
                  if (fabs((float)currentRow[name].data<unsigned int>() - (float)lastValues[name].data<unsigned int>()) >= deadband)
                    storeRecord = true;
                }
              }
            } else {
              // Deadband = 0, so record must be stored
              storeRecord = true;
            }
          }
        } else {
          // If any string or bool value exists in row, just store record
          storeRecord = true;
        }
//        /* JRC CHANGE could do old/new comparison like below, but care needed with timeout
//       } else 
//			if ((type == typeid(std::string)) && (copyFrom[name].data<std::string>() != lastValues[name].data<std::string>())) {
//         		storeRecord = true;
//			
//            } 
//			else 
//		  		if ((type == typeid(bool)) && (copyFrom[name].data<bool>() != lastValues[name].data<bool>())) {
//         			storeRecord = true;
//        		} 
//				else {
//          			std::cout << "Unhandled type '" << type.name() << "' for field '" << name << std::endl;
//        		}	
//        */
      }
      // New code introduced by SK for debugging empty string 02.06.10
//	  else {
//	  	std::cerr << "currentRow[name].isNull() is TRUE" << std::endl;
//		if (type == typeid(std::string)) {
//			if(currentRow[name].data<std::string>() != lastValues[name].data<std::string>()) {
//	      		storeRecord = true;
//				std::cerr << "Storing string " << std::endl;
// 		    }
//		} 
//		else 
//	  		if (type == typeid(bool)) {
//				if(currentRow[name].data<bool>() != lastValues[name].data<bool>()) {
//       				storeRecord = true;
//       		} 
//			}
//			else {
//      			std::cout << "Unhandled type '" << type.name() << "' for field '" << name << std::endl;
//       	}	
//	  }
	 // End of new code 
    }
  }

  //==================
  // CATCH EXCEPTIONS
  //==================
  catch( cool::Exception& e ) 
  {
    std::cerr << "'CondDBUtilities::doStore()' COOL exception : " << e.what() << std::endl;
    throw;
  }
  catch ( coral::Exception& e )
  {
    std::cerr << "'CondDBUtilities::doStore()' CORAL exception : " << e.what() << std::endl;
    throw;
  }
  catch( std::exception& e )
  {
    std::cerr << "'CondDBUtilities::doStore()' C++ exception : " << e.what() << std::endl;
    throw;
  }
  catch( ... ) 
  {
    std::cerr << "'CondDBUtilities::doStore()' Unknown exception : " << std::endl;
    throw;
  }

  return (storeRecord);
}

// Not used, it seems
//bool CondDBUtilities::copyRow(coral::AttributeList& copyTo,
//                              const coral::AttributeList& copyFrom,
//                              coral::AttributeList& lastValues)
//{
//
//  bool storeRecord = false;
//  unsigned long smoothSeconds = 600;
//  float deadband = 2.0;
//  cool::UInt32 index;
//
//  try {
//
//    // First check whether timeout has expired, in which case, the row will be stored whether
//    // the deadband has been exceeded or not.
//    if (lastValues["MILLISECS"].isNull()) {
//      storeRecord = true;
//    } else {
//      cool::ValidityKey timeDiff = copyFrom["MILLISECS"].data<cool::ValidityKey>() - lastValues["MILLISECS"].data<cool::ValidityKey>();
//      if (timeDiff >= (smoothSeconds * 1000)) {
//        storeRecord = true;
//      }
//    }
//
//    for (index = 0; index < copyTo.size(); index++) {
//
//      const std::string& name = copyTo[index].specification().name();
//      const std::type_info& type = copyTo[index].specification().type();
//
//      if (!copyFrom[name].isNull()) {
//        if (!storeRecord) {
//          if ((type == typeid(float)) || (type == typeid(int)) || (type == typeid(unsigned int))) {
//            // Values may be compared for smoothing
//            if (type == typeid(float)) {
//              if (fabs(copyFrom[name].data<float>() - lastValues[name].data<float>()) >= deadband)
//                storeRecord = true;
//            } else if (type == typeid(int)) {
//              if (fabs((float)copyFrom[name].data<int>() - (float)lastValues[name].data<int>()) >= deadband)
//                storeRecord = true;
//            } else {
//              if (fabs((float)copyFrom[name].data<unsigned int>() - (float)lastValues[name].data<unsigned int>()) >= deadband)
//                storeRecord = true;
//            }
//          }
//        }
//        copyTo[name].setNull(false);
//        copyTo[name] = copyFrom[name];
//      } else if (!lastValues[name].isNull()) {
//        copyTo[name].setNull(false);
//        copyTo[name] = lastValues[name];
//      } else {
//        copyTo[name].setNull();
//      }
//    }
//
//    if (storeRecord) {
//      lastValues = copyTo;
//    }
//  }
//
  //==================
  // CATCH EXCEPTIONS
  //==================
//  catch( cool::Exception& e ) 
//  {
//    throw;
//  }
//  catch ( coral::Exception& e )
//  {
//    throw;
//  }
//  catch( std::exception& e )
//  {
//    throw;
//  }
//  catch( ... ) 
//  {
//    throw;
//  }
//
//  return (storeRecord);
//}
///////////////
