// Include files
// -------------
#include "CoralBase/Exception.h"
#include "CoolKernel/IDatabaseSvc.h"
#include "CoolKernel/Exception.h"
#include "RelationalAccess/IDatabaseServiceSet.h"
#include "RelationalAccess/IDatabaseServiceDescription.h"
#include "CoolApplication/Application.h"
#include "RelationalAccess/IRelationalService.h"
#include "RelationalAccess/IAuthenticationService.h"
#include "RelationalAccess/IAuthenticationCredentials.h"
#include "RelationalAccess/ILookupService.h"
#include "RelationalAccess/IConnectionService.h"
#include "RelationalAccess/IQuery.h"
#include "RelationalAccess/ICursor.h"
// Commented moved to private headers 14.10.10 by SK
//#include "CoralBase/Attribute.h"
//#include "CoralBase/AttributeList.h"
//#include "CoralBase/TimeStamp.h"
#include "CoralBase/MessageStream.h"

#include <iostream>
#include <exception>
#include <list>
#include <vector>
#include <fstream>

// Application Specific Include Files
// ----------------------------------
#include "globals.h"
#include "PvssArchiveFolderConfig.h"
#include "PvssArchiveFolderData.h"
#include "PvssCoolDefaultConnections.h"
#include "PvssCondDbFolderConfig.h"
#include "PvssCondDbFolderData.h"
#include "ArgParser.h"

#include "PvssArchiveNumber.h"
#include "PvssArchiveUtilities.h"
#include "CondDBUtilities.h"
#include "PvssCoolStatistics.h"

// Version
#define PVSSCOOL_VERSION "2.1.4"

// Command Function prototypes (same names as command argument given)
void configurePvssCool(std::string folderConfigTable, cool::IDatabaseSvc& coolSvc, coral::IConnectionService& connSvc, std::string configSvcName, std::string coolSvcName, std::string coolDbName, long folderGroup);
void verifyCondDbWithCool(std::string folderConfigTable, coral::IConnectionService& connSvc, cool::IDatabaseSvc& coolSvc, std::string dbName, long folderGroup);
void runOnce(std::string folderConfigTable, cool::IDatabaseSvc& coolSvc, coral::IConnectionService& connSvc, std::string dbName, long folderGroup, int rowCacheSize, int timeSpan = 0, bool showInfo = false);
bool dropCoolDatabase(cool::IDatabaseSvc& coolSvc, std::string serviceName, std::string dbName);
bool dropCoolFolder(cool::IDatabaseSvc& coolSvc, std::string serviceName, std::string dbName, std::string folderName);

// Debug functions
void readData(std::string folderConfigTable, coral::IConnectionService& connSvc, long folderGroup, int rowCacheSize, int iMaxRows, int timeSpan = 0);
void readFolders(std::string folderConfigTable, coral::IConnectionService& connSvc, long folderGroup, bool ignoreRemoved);
void updateTime(std::string folderConfigTable, coral::IConnectionService& connSvc, long folderGroup = PVSS_COOL_ALL_FOLDERS, std::string folderName = "", std::string time = "");
void closeIOVs(std::string folderConfigTable, cool::IDatabaseSvc& coolSvc, coral::IConnectionService& connSvc, std::string coolDbName, std::string folderName, std::string time);

// Other Function prototypes
bool createCOOLDatabase(cool::IDatabaseSvc& coolSvc, std::string serviceName, std::string dbName);
bool configure(std::string folderConfigTable, cool::IDatabaseSvc& coolSvc, coral::IConnectionService& connSvc, std::string configServiceName, std::string coolServiceName, std::string coolDbName, long folderGroup);
bool transferToCool(PvssArchiveFolderConfig& paFolderConfig, cool::IDatabaseSvc& coolSvc, coral::IConnectionService& connSvc, std::string dbName, long folderGroup, int rowCacheSize, bool firstTime, int timeSpan = 0, bool showInfo = false);
void showHelp(std::string forCommand = std::string(""));

// Two functions, introduced 21.06.10 by SK
// Commented 13.02.17
//void initiateMilliFolderList(std::vector<std::string> &list);
//bool isMilliFolder(std::vector<std::string> list, std::string folderName);

// Two functions, introduced 24.10.11 by SK
void createDbSyncFile(long folderId);
void removeDbSyncFile(long folderId);
bool checkDbSyncFile(long folderId);



/*
// Example for a thread
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

char message[] = "Hello World";

void *thread_function(void *arg) {
    printf("thread_function started. Arg was %s\n", (char *)arg);
    sleep(3);
    strcpy(message, "Bye!");
    pthread_exit("See Ya");
}

int main() {
    int res;
    pthread_t a_thread;
    void *thread_result;
    res = pthread_create(&a_thread, NULL, thread_function, (void*)message);
    if (res != 0) {
        perror("Thread creation failed");
        exit(EXIT_FAILURE);
    }
    printf("Waiting for thread to finish...\n");
    res = pthread_join(a_thread, &thread_result);
    if (res != 0) {
        perror("Thread join failed");
        exit(EXIT_FAILURE);
    }
    printf("Thread joined, it returned %s\n", (char *)thread_result);
    printf("Message is now %s\n", message);
    exit(EXIT_SUCCESS);
}
*/

/*
#include <unistd.h>
#include <stdio.h>
#include <signal.h>

//  The intrpt function reacts to the signal passed in the parameter signum.
//  This function is called when a signal occurs.
//  A message is output, then the signal handling for SIGINT is reset
//  (by default generated by pressing CTRL-C) back to the default behavior.
void intrpt(int signum)
{
    printf("I got signal %d\n", signum);
    (void) signal(SIGINT, SIG_DFL);
}

//  main intercepts the SIGINT signal generated when Ctrl-C is input.
//  Otherwise, sits in an infinite loop, printing a message once a second.
int main()
{
    (void) signal(SIGINT, intrpt);

    while(1) {
        printf("Hello World!\n");
        sleep(1);
    }
}
*/

coral::MessageStream *error = 0;

//-----------------------------------------------------------------------------

int main ( int argc, char* argv[] )
{
  bool bValid = true;
  bool ignoreRemoved = true;
  bool showInfo = false;

  int status = -1;
  int rowCacheSize = FW_CONDDB_DEFAULT_ROWCACHE;
  int iMaxRows = 100;
  int timeSpan = 0;

  long folderGroup = PVSS_COOL_ALL_FOLDERS;
  
  std::string folderConfigurationTable; 	// Introduced in 2014 for distinguishing 
  											// between Run1 and Run 2 folders.
											// The -r 1| 2 command line parameter introduced for that
  std::string argument;
  std::string command;
  std::string myAnswer;
  std::string coolDbName = "";
  std::string folderName = "";
  std::string time = "";
  std::string subDetector;
  std::string atlasRunNumber = "2";

  std::vector<std::string> commands;
  
  ArgParser apParser;

  // Set up valid commands
  commands.push_back("verifyCondDbWithCool");
  commands.push_back("configurePvssCool");
  commands.push_back("runOnce");
  commands.push_back("closeIOVs");
  commands.push_back("readData");
  commands.push_back("readFolders");
  commands.push_back("updateTime");
  commands.push_back("dropCoolFolder");
  commands.push_back("dropCoolDatabase");
#ifdef INCLUDE_TEST
  commands.push_back("test");
#endif

  // Set up valid argument flags and process command line
  apParser.setFlags(std::string("h,v,d,r,t,m,g,f,a,s,i,R"));
  apParser.processArguments(argv, argc);

  // Complete a first pass at command/arguments given
  if (argc > 1) {
    command = argv[1];
    std::vector<std::string>::iterator iCommand = std::find(commands.begin(), commands.end(), command);
    if (iCommand != commands.end()) {
      if (apParser.flagGiven('h') == Argument::found) {
        showHelp(command);
        return (0);
      }
    } else if (apParser.flagGiven('v') == Argument::found) {
      std::cout << std::endl << " PVSS-COOL process version is " << PVSSCOOL_VERSION << std::endl << std::endl;
      return (0);
    } else if (apParser.flagGiven('h') == Argument::found) {
      showHelp();
      return (0);
    } else {
      std::cout << " Unknown or incorrect command given" << std::endl << std::endl;
      showHelp();
      return (0);
    }
  } else {
    showHelp();
    return (0);
  }

  //================
  // MAIN TRY BLOCK
  //================
  try {

    // Create a COOL Application
    cool::Application app;

    // Get a handle to the COOL database service (from the application context)
    cool::IDatabaseSvc& coolSvc = app.databaseService();
    coral::IConnectionService& connSvc = app.connectionSvc();  

    error = new coral::MessageStream("pvss2cool main:");
//    coral::MessageStream::setMsgVerbosity(coral::Error);
	
	// Introduced 23.04.14 for providing work either with Run1 or Run 2
	// COOL folders
	if(apParser.argumentFor('R', argument) == Argument::stored) {
		atlasRunNumber = argument;
		if(atlasRunNumber == "1")
			folderConfigurationTable = PVSS_COOL_FOLDER_CONFIGURATION_TABLE_R1;
		else
			folderConfigurationTable = PVSS_COOL_FOLDER_CONFIGURATION_TABLE_R2;
	}
	else	// default is Run 2
		folderConfigurationTable = PVSS_COOL_FOLDER_CONFIGURATION_TABLE_R2;
//	std::cerr << "Control: folderConfigurationTabel is set by " << folderConfigurationTable << std::endl;
	// end of new code of 23.04.14
	
    if (command.compare("verifyCondDbWithCool") == 0) {

      if (apParser.argumentFor('d', argument) == Argument::stored) {
        coolDbName = argument;
      } else {
        bValid = false;
        std::cout << "COOL database name not given" << std::endl;
      }
      if (apParser.argumentFor('g', argument) == Argument::stored) {
        folderGroup = atol(argument.data());
      } else {
        folderGroup = PVSS_COOL_ALL_FOLDERS;
      }
      if (bValid)
        verifyCondDbWithCool(folderConfigurationTable, connSvc, coolSvc, coolDbName, folderGroup);

    } else if (command.compare("configurePvssCool") == 0) {

      if (apParser.argumentFor('d', argument) == Argument::stored) {
        coolDbName = argument;
      } else {
        bValid = false;
        std::cout << "COOL database name not given" << std::endl;
      }
      if (apParser.argumentFor('g', argument) == Argument::stored) {
        folderGroup = atol(argument.data());
      } else {
        folderGroup = PVSS_COOL_ALL_FOLDERS;
      }
      if (bValid) {
        configurePvssCool(folderConfigurationTable, coolSvc, connSvc,
                          PVSS_COOL_CONFIGURATION_CONNECTION,
                          PVSS_COOL_COOL_CONNECTION,
                          coolDbName,
                          folderGroup);
      }

    } else if (command.compare("dropCoolFolder") == 0) {

      if (apParser.argumentFor('d', argument) == Argument::stored) {
        coolDbName = argument;
      } else {
        bValid = false;
        std::cout << "COOL database name not given" << std::endl;
      }
      if (apParser.argumentFor('f', argument) == Argument::stored) {
        folderName = argument;
      } else {
        bValid = false;
        std::cout << "Folder name not given" << std::endl;
      }
      if (bValid) {
        dropCoolFolder(coolSvc, PVSS_COOL_COOL_CONNECTION, coolDbName, folderName);
      }

    } else if (command.compare("dropCoolDatabase") == 0) {

      if (apParser.argumentFor('d', argument) == Argument::stored) {
        coolDbName = argument;
      } else {
        bValid = false;
        std::cout << "COOL database name not given" << std::endl;
      }
      if (bValid) {
        dropCoolDatabase(coolSvc, PVSS_COOL_COOL_CONNECTION, coolDbName);
      }

    } else if (command.compare("runOnce") == 0) {

      if (apParser.argumentFor('d', argument) == Argument::stored) {
        coolDbName = argument;
      } else {
        bValid = false;
        *error << "COOL database name not given" << coral::MessageStream::endmsg;
      }
      if (apParser.argumentFor('r', argument) == Argument::stored) {
        rowCacheSize = atoi(argument.data());
      } else {
        rowCacheSize = FW_CONDDB_DEFAULT_ROWCACHE;
      }
      if (apParser.argumentFor('s', argument) == Argument::stored) {
        // JRC this is currently not used (only for process checking for cron job) but could be
        //     used in the future to set some of the required environment for different sub-detectors
        subDetector = argument;
      }
      if (apParser.argumentFor('g', argument) == Argument::stored) {
        folderGroup = atol(argument.data());
      } else {
        folderGroup = PVSS_COOL_ALL_FOLDERS;
      }
      if (apParser.argumentFor('t', argument) == Argument::stored) {
        timeSpan = atoi(argument.data());
      } else {
        timeSpan = 0;
      }
      if (apParser.flagGiven('i') == Argument::found)
        showInfo = true;
      if (bValid)
        runOnce(folderConfigurationTable, coolSvc, connSvc, coolDbName, folderGroup, rowCacheSize, timeSpan, showInfo);

    } else if (command.compare("readData") == 0) {

      if (apParser.argumentFor('r', argument) == Argument::stored) {
        rowCacheSize = atoi(argument.data());
      } else {
        rowCacheSize = FW_CONDDB_DEFAULT_ROWCACHE;
      }
      if (apParser.argumentFor('g', argument) == Argument::stored) {
        folderGroup = atol(argument.data());
      } else {
        folderGroup = PVSS_COOL_ALL_FOLDERS;
      }
      if (apParser.argumentFor('t', argument) == Argument::stored) {
        timeSpan = atoi(argument.data());
      } else {
        timeSpan = 0;
      }
      if (apParser.argumentFor('m', argument) == Argument::stored) {
        iMaxRows = atoi(argument.data());
      } else {
        iMaxRows = 100;
      }
      if (bValid)
        readData(folderConfigurationTable, connSvc, folderGroup, rowCacheSize, iMaxRows, timeSpan);

    } else if (command.compare("readFolders") == 0) {

      if (apParser.argumentFor('g', argument) == Argument::stored) {
        folderGroup = atol(argument.data());
      } else {
        folderGroup = PVSS_COOL_ALL_FOLDERS;
      }
      if (apParser.flagGiven('a') == Argument::found)
        ignoreRemoved = false;

      readFolders(folderConfigurationTable, connSvc, folderGroup, ignoreRemoved);

    } else if (command.compare("updateTime") == 0) {

      if (apParser.argumentFor('g', argument) == Argument::stored) {
        folderGroup = atol(argument.data());
      } else {
        folderGroup = PVSS_COOL_ALL_FOLDERS;
      }
      if (apParser.argumentFor('f', argument) == Argument::stored) {
        folderName = argument;
      } else {
        folderName = "";
      }
      if (apParser.argumentFor('t', argument) == Argument::stored) {
        time = argument.data();
      } else {
        time = "";
      }
      updateTime(folderConfigurationTable, connSvc, folderGroup, folderName, time);

    } else if (command.compare("closeIOVs") == 0) {

      if (apParser.argumentFor('d', argument) == Argument::stored) {
        coolDbName = argument;
      } else {
        bValid = false;
        std::cout << "COOL database name not given" << std::endl;
      }
      if (apParser.argumentFor('f', argument) == Argument::stored) {
        folderName = argument;
      } else {
        bValid = false;
        std::cout << "COOL folder name not given" << std::endl;
      }
      if (apParser.argumentFor('t', argument) == Argument::stored) {
        time = argument.data();
      } else {
        time = "";
      }
      if (bValid)
        closeIOVs(folderConfigurationTable, coolSvc, connSvc, coolDbName, folderName, time);

#ifdef INCLUDE_TEST
    } else if (command.compare("test") == 0) {

      int timeSpan = 3600;
      std::cout << CondDBUtilities::addSeconds("19-08-2008 13:20:00", timeSpan) << std::endl;
      std::cout << timeSpan << std::endl;

      std::cout << CondDBUtilities::getTimeDifference(CondDBUtilities::getTimeStamp("19-08-2008 12:20:00"), CondDBUtilities::getTimeNow()) << std::endl;
      std::cout << CondDBUtilities::getTimeDifference(CondDBUtilities::getTimeStamp("19-08-2008 14:20:00"), CondDBUtilities::getTimeNow()) << std::endl;


      std::cout << CondDBUtilities::formatTime(CondDBUtilities::getTimeNow()) << std::endl;
//     std::cout << CondDBUtilities::formatTime(CondDBUtilities::getTimeNow(true)) << std::endl;
      /*
      PvssCoolStatistics myStats;
      myStats.resetStatistics(connSvc, "/JIM/TEST");
      myStats.setRunning(connSvc, "/JIM/TEST");
      //                                             IOVs read write span
      myStats.updateStatistics(connSvc, "/JIM/TEST", 6430,  4.0,   9.0,  1804, "05-06-2008 12:15:04");
      myStats.updateStatistics(connSvc, "/JIM/TEST", 1996,  4.0,   4.0,   900, "05-06-2008 12:30:04");
      myStats.updateStatistics(connSvc, "/JIM/TEST", 1759,  5.0,   4.0,   907, "05-06-2008 12:45:11");
      myStats.updateStatistics(connSvc, "/JIM/TEST", 2420,  2.0,   5.0,   893, "05-06-2008 13:00:04");
      myStats.updateStatistics(connSvc, "/JIM/TEST", 2714,  3.0,   5.0,   900, "05-06-2008 13:15:04");
      myStats.updateStatistics(connSvc, "/JIM/TEST", 2644,  3.0,   5.0,   900, "05-06-2008 13:30:04");
      myStats.updateStatistics(connSvc, "/JIM/TEST", 1818,  5.0,   4.0,   900, "05-06-2008 13:45:04");
      myStats.updateStatistics(connSvc, "/JIM/TEST", 1935,  8.0,   4.0,   902, "05-06-2008 14:00:06");
      myStats.setRunning(connSvc, "/JIM/TEST", false);
      */
      /*
      PvssCondDbFolderConfig pcFolderConfig;

      if (apParser.argumentFor('d', argument) == Argument::stored) {
        coolDbName = argument;

        if (pcFolderConfig.connect(coolSvc, PVSS_COOL_COOL_CONNECTION, coolDbName)) {
          pcFolderConfig.getFolders();
          pcFolderConfig.disconnect();
        }
      } else {
        bValid = false;
        std::cout << "COOL database name not given" << std::endl;
      }
      */
      /*
      PvssArchiveFolderData paFolderData;
      if (paFolderData.connect(connSvc, PVSS_COOL_PVSS_CONNECTION)) {
        std::vector<PvssArchiveNumber> arcNumbers;
        PvssArchiveUtilities::getArchiveNumbers(paFolderData.connection(),
                                                arcNumbers,
                                                std::string("EVENT"),
                                                std::string("10-02-2007 12:00:00"));//,
                                                //std::string("17-02-2007 12:00:00"));
        for (std::vector<PvssArchiveNumber>::iterator iIter = arcNumbers.begin(); iIter != arcNumbers.end(); iIter++)
          std::cout << iIter->archiveNumber() << ", " << iIter->startTime() << ", " << iIter->endTime() << std::endl;
        paFolderData.disconnect();
      }
      */
#endif
    }
  }

  //==================
  // CATCH EXCEPTIONS
  //==================
  catch ( coral::Exception& e )
  {
    std::cerr << "'main()' CORAL exception : " << e.what() << std::endl;
    status = 1;
  }
  catch ( std::exception& e )
  {
    std::cerr << "'main()' Standard C++ exception : " << e.what() << std::endl;
    status = 1;
  }
  catch ( std::string &str )
  {
    std::cerr << "'main()' Exception Raised : " << str << std::endl;
    status = 1;
  }
  catch( ... )
  {
    std::cerr << "'main()' Unknown exception ..." << std::endl;
    status = 1;
  }

  // Free up any allocated memory
  delete(error);

  return status;
}

void showHelp(std::string forCommand /* = std::string("") */)
{
  if (forCommand.empty()) {
    std::cout << " Possible commands are:" << std::endl << std::endl;
    std::cout << "\tverifyCondDbWithCool" << std::endl;
    std::cout << "\tconfigurePvssCool" << std::endl;
    std::cout << "\trunOnce" << std::endl;
    std::cout << "\tcloseIOVs" << std::endl;
    std::cout << "\treadData" << std::endl;
    std::cout << "\treadFolders" << std::endl;
    std::cout << "\tupdateTime" << std::endl;
    std::cout << std::endl;
    std::cout << "Please note that operation commands are case sensitive" << std::endl;
  } else {
    if (forCommand.compare("verifyCondDbWithCool") == 0) {
      std::cout << "\tverifyCondDbWithCool - verifies persistent information is consistent with COOL folders" << std::endl;
      std::cout << "\t\t  Possible arguments:" << std::endl;
      std::cout << "\t\t  -d <COOL DB Name>" << std::endl;
      std::cout << "\t\t  -g <Folder Group> (optional, default is for ALL folders in the database)" << std::endl;
      std::cout << std::endl;
    } else if (forCommand.compare("configurePvssCool") == 0) {
      std::cout << "\tconfigurePvssCool - configures the PVSS to COOL process" << std::endl;
      std::cout << "\t\t  Possible arguments:" << std::endl;
      std::cout << "\t\t  -d <COOL DB Name>" << std::endl;
      std::cout << "\t\t  -g <Folder Group> (optional, default is for ALL folders in the database)" << std::endl;
      std::cout << std::endl;
    } else if (forCommand.compare("runOnce") == 0) {
      std::cout << "\trunOnce - runs the process once (can be used as cron job)" << std::endl;
      std::cout << "\t\t  Possible arguments:" << std::endl;
      std::cout << "\t\t  -d <COOL DB Name>" << std::endl;
      std::cout << "\t\t  -r <Row Cache Size> (optional, default is " << FW_CONDDB_DEFAULT_ROWCACHE << ")" << std::endl;
      std::cout << "\t\t  -g <Folder Group> (optional, default is for ALL folders in the database)" << std::endl;
      std::cout << "\t\t  -t <Time Span (s)> (optional, default is 'Until current time')" << std::endl;
      std::cout << "\t\t  -i                 (optional with no parameter, default is not to output info)" << std::endl;
      std::cout << "\t\t                       if given, basic information about folder transfer is output" << std::endl;
      std::cout << std::endl;
    } else if (forCommand.compare("closeIOVs") == 0) {
      std::cout << "\tcloseIOVs - closes the IOVs for all channels in the folder given" << std::endl;
      std::cout << "\t\t  Possible arguments:" << std::endl;
      std::cout << "\t\t  -d <COOL DB Name>" << std::endl;
      std::cout << "\t\t  -f <COOL Folder Name (including full path)>" << std::endl;
      std::cout << "\t\t  -t <New Time> (optional, default is last update time)" << std::endl;
      std::cout << "\t\t                if specified, the format must be dd-mm-yyyy_hh:mm:ss" << std::endl;
      std::cout << std::endl;
    } else if (forCommand.compare("readData") == 0) {
      std::cout << "\treadData - reads data over the time period and outputs to screen" << std::endl;
      std::cout << "\t           This does not update the timestamp of the 'last update'" << std::endl;
      std::cout << "\t\t  Possible arguments:" << std::endl;
      std::cout << "\t\t  -m <Max Rows to display> (optional, default is 100)" << std::endl;
      std::cout << "\t\t  -r <Row Cache Size> (optional, default is " << FW_CONDDB_DEFAULT_ROWCACHE << ")" << std::endl;
      std::cout << "\t\t  -g <Folder Group> (optional, default is for ALL folders in the database)" << std::endl;
      std::cout << "\t\t  -t <Time Span (s)> (optional, default is 'Until current time')" << std::endl;
      std::cout << std::endl;
    } else if (forCommand.compare("dropCoolFolder") == 0) {
      std::cout << "\tdropCoolFolder - drops the COOL folder with the full path specified" << std::endl;
      std::cout << "\t                 USE WITH CAUTION!!!!" << std::endl;
      std::cout << "\t\t  Possible arguments:" << std::endl;
      std::cout << "\t\t  -d <COOL DB Name>" << std::endl;
      std::cout << "\t\t  -f <COOL Folder Name (including full path)>" << std::endl;
      std::cout << std::endl;
    } else if (forCommand.compare("dropCoolDatabase") == 0) {
      std::cout << "\tdropCoolDatabase - drops the COOL database of the name specified" << std::endl;
      std::cout << "\t                   USE WITH CAUTION!!!!" << std::endl;
      std::cout << "\t\t  Possible arguments:" << std::endl;
      std::cout << "\t\t  -d <COOL DB Name>" << std::endl;
      std::cout << std::endl;
    } else if (forCommand.compare("readFolders") == 0) {
      std::cout << "\treadFolders - displays the folder names and last update times of folders configured in CondDB" << std::endl;
      std::cout << "\t\t  Possible arguments:" << std::endl;
      std::cout << "\t\t  -g <Folder Group> (optional, default is for ALL folders in the database)" << std::endl;
      std::cout << "\t\t  -a                 (optional with no parameter, default is only those not removed)" << std::endl;
      std::cout << "\t\t                       if given all folders are shown, including those marked as removed" << std::endl;
      std::cout << std::endl;
    } else if (forCommand.compare("updateTime") == 0) {
      std::cout << "\tupdateTime - Changes the timestamp of the 'last update'" << std::endl;
      std::cout << "\t             USE WITH CAUTION!!!!" << std::endl;
      std::cout << "\t\t  Possible arguments:" << std::endl;
      std::cout << "\t\t  -g <Folder Group> (optional, default is for ALL folders in the database)" << std::endl;
      std::cout << "\t\t  -f <COOL Folder Name (including full path)> (optional, default is all folders [in system if given])" << std::endl;
      std::cout << "\t\t  -t <New Time> (optional, default is current time)" << std::endl;
      std::cout << "\t\t                if specified, the format must be dd-mm-yyyy_hh:mm:ss" << std::endl;
      std::cout << std::endl;
    }
  }
  return;
}


// Additional code. Introduced 24.10.11 by SK
// The file is being created just after successful writing to COOL
// in order to be used next pvss2cool cycle, if connection to Online DB 
// has been lost just after that (i.e., if in archive the 'endTime' field
// in the pvss2cool_folders table has not been updated)
void createDbSyncFile(long folderId)   
{
	char syncFilePath[128];
	sprintf(syncFilePath, "%s/Folder_%d_LastUpdate.sync", getenv((const char*)"CORAL_DBLOOKUP_PATH"), (int)folderId);
	
	std::ofstream lastUpdateTime(syncFilePath);
	if(!lastUpdateTime.good()) {
		std::cerr << "Impossible to create file " << syncFilePath << std::endl;
		return;
	}
//	std::cerr << syncFilePath << " has been created" << std::endl;
}

void removeDbSyncFile(long folderId)   
{
	int err;
	char syncFilePath[128];
	sprintf(syncFilePath, "%s/Folder_%d_LastUpdate.sync", getenv((const char*)"CORAL_DBLOOKUP_PATH"), (int)folderId);
	
	err = remove(syncFilePath);
	if(err)
		std::cerr << "Could not remove " << syncFilePath << std::endl;
//	else
//		std::cerr << syncFilePath << " has been removed" << std::endl;
}

bool checkDbSyncFile(long folderId)
{
	int err;
	char syncFilePath[128];
	sprintf(syncFilePath, "%s/Folder_%d_LastUpdate.sync", getenv((const char*)"CORAL_DBLOOKUP_PATH"), (int)folderId);
	
	std::ifstream lastUpdateStored(syncFilePath);
	if(!lastUpdateStored.good()) {
//		std::cerr << "File " << syncFilePath << " does not exist, this is good" << std::endl;
		return(false);
	}
	else {
		std::cerr << "Sync file " << syncFilePath << " does exist" << std::endl;
		err = remove(syncFilePath);
		if(err)
			std::cerr << "Could not remove " << syncFilePath << std::endl;
//		else
//			std::cerr << syncFilePath << " has been removed" << std::endl;
		return(true);
	}
}

// Additional code. Introduced 21.06.10 by SK
// Commented 13.02.17 after deploying usage of TIMEPRECISION from pvss2cool_folders
/* void initiateMilliFolderList(std::vector<std::string> &list)
{
	std::string folderListPath;
	std::string configBuf;
	std::string folderName;
	// Check whether CORAL_DBLOOKUP_PATH is defined
	// One has to say, that there is NO need in this check,
	// because otherwise connection to DB would be impossible!
	// As soon as we are already there - the environment is Ok
	bool env_error = false;

	std::string	dblookupPath;
	if(getenv((const char*)"CORAL_DBLOOKUP_PATH") == 0) 
		env_error = true;
	else {
		dblookupPath = getenv((const char*)"CORAL_DBLOOKUP_PATH");
		if(dblookupPath.size() == 0) 
			env_error = true;
	}
	if(env_error) {
		std::cerr << "CORAL_DBLOOKUP_PATH is not defined: Exiting" << std::endl;
		exit(1);
	}
	// End of redundant code
	
	folderListPath = dblookupPath+"/MilliFolders.lst";
	
	std::ifstream folderList(folderListPath.c_str());
	if(!folderList.good()) {
//		std::cout << "No definition of millisecond precision files" << std::endl;
		return;
	}
	
	while(!folderList.eof()) {
		getline(folderList, configBuf);
//		std::cerr << "Line read: " << configBuf << std::endl;
		if((configBuf[0] == '#')
		|| ((configBuf[0] == '/') && (configBuf[1] == '/')))
			continue;
		else {
			if(configBuf.find_first_of("/") != std::string::npos) {
				configBuf = configBuf.substr(configBuf.find_first_of("/"));
				if(configBuf.find_first_of(" ") != std::string::npos)
					configBuf = configBuf.substr(0, configBuf.find_first_of(" "));
			}
			else
				continue;
		}
//		std::cerr << "Line taken: " << configBuf << std::endl;
		list.push_back(configBuf);
	}	
}

bool isMilliFolder(std::vector<std::string> list, std::string folderName)
{
    for(std::vector<std::string>::iterator iFolder = list.begin(); iFolder != list.end(); iFolder++) 
		if(*iFolder == folderName)
			return(true);
	return(false);
}

// End of new code as of 21.06.10
*/

bool createCOOLDatabase(cool::IDatabaseSvc& coolSvc,
                        std::string serviceName,
                        std::string dbName)
{
  bool databaseExists = false;

  std::string connectionString = serviceName + std::string("(") + FW_CONDDB_UPDATER + std::string (")/") + dbName;

  try {
    cool::IDatabasePtr myConnection = coolSvc.openDatabase(connectionString);

    // Opening database has been successful, therefore it must exist
    databaseExists = true;
//    myConnection.closeDatabase();
    cool::IDatabasePtr dbNull;
    myConnection = dbNull;
  }

  catch ( cool::DatabaseDoesNotExist& e)
  {
    std::cerr << "'createCOOLDatabase() Connection' COOL Database '" << dbName << "' does not exist and will be created" << std::endl;
  }
  catch ( cool::Exception& e )
  {
    std::cerr << "'createCOOLDatabase() Connection' COOL exception : " << e.what() << std::endl;
    return (false);
  }
  catch( ... )
  {
    std::cerr << "'createCOOLDatabase() Connection' Unknown exception ..." << std::endl;
    return (false);
  }

  try {

    if (!databaseExists)
      coolSvc.createDatabase(connectionString);

  }

  //==================
  // CATCH EXCEPTIONS
  //==================
  catch ( coral::Exception& e )
  {
    std::cerr << "'createCOOLDatabase() CreateDB' CORAL exception : " << e.what() << std::endl;
    return (false);
  }
  catch ( std::exception& e )
  {
    std::cerr << "'createCOOLDatabase() CreateDB' Standard C++ exception : " << e.what() << std::endl;
    return (false);
  }
  catch( ... )
  {
    std::cerr << "'createCOOLDatabase() CreateDB' Unknown exception ..." << std::endl;
    return (false);
  }

  return (true);
}

bool dropCoolDatabase(cool::IDatabaseSvc& coolSvc,
                      std::string serviceName,
                      std::string dbName)
{
  std::string myAnswer;
  std::string connectionString = serviceName + std::string("(") + FW_CONDDB_UPDATER + std::string (")/") + dbName;

  try {
    std::cout << "This will drop the database defined by the connection string '" << connectionString << "' and all its data" << std::endl;
    std::cout << "Are you sure you want to do this? (Y/N)" << std::endl;
    std::cin >> myAnswer;
    if (myAnswer.compare("Y") == 0)
      coolSvc.dropDatabase(connectionString);
    else
      std::cout << "'dropCoolDatabase' operation cancelled" << std::endl;
  }

  //==================
  // CATCH EXCEPTIONS
  //==================
  catch ( coral::Exception& e )
  {
    std::cerr << "'dropCoolDatabase()' CORAL exception : " << e.what() << std::endl;
    return (false);
  }
  catch ( std::exception& e )
  {
    std::cerr << "'dropCoolDatabase()' Standard C++ exception : " << e.what() << std::endl;
    return (false);
  }
  catch( ... )
  {
    std::cerr << "'dropCoolDatabase()' Unknown exception ..." << std::endl;
    return (false);
  }

  return (true);
}

bool dropCoolFolder(cool::IDatabaseSvc& coolSvc,
                    std::string serviceName,
                    std::string dbName,
                    std::string folderName)
{
  std::string myAnswer;

  PvssCondDbFolderConfig pcFolderConfig;

  try {
    std::cout << "This will drop the folder '" << folderName << "' and all its data" << std::endl;
    std::cout << "Are you sure you want to do this? (Y/N)" << std::endl;
    std::cin >> myAnswer;
    if (myAnswer.compare("Y") == 0) {
      if (pcFolderConfig.connect(coolSvc, serviceName, dbName, false))
        pcFolderConfig.dropFolder(folderName);
      else
        std::cout << "Could not connect to database '" << dbName << "'" << std::endl;
    } else {
      std::cout << "'dropCoolFolder' operation cancelled" << std::endl;
    }
  }

  //==================
  // CATCH EXCEPTIONS
  //==================
  catch ( coral::Exception& e )
  {
    std::cerr << "'dropCoolFolder()' CORAL exception : " << e.what() << std::endl;
    return (false);
  }
  catch ( std::exception& e )
  {
    std::cerr << "'dropCoolFolder()' Standard C++ exception : " << e.what() << std::endl;
    return (false);
  }
  catch( ... )
  {
    std::cerr << "'dropCoolFolder()' Unknown exception ..." << std::endl;
    return (false);
  }

  return (true);
}

bool configure(std::string folderConfigurationTable,
               cool::IDatabaseSvc& coolSvc,
               coral::IConnectionService& connSvc,
               std::string configServiceName,
               std::string coolServiceName,
               std::string coolDbName,
               long folderGroup)
{
  bool status = false;
  bool connected = false;

  std::string answer;

  PvssArchiveFolderConfig paFolderConfig;
  PvssCondDbFolderConfig pcFolderConfig;

  try {
    connected = paFolderConfig.connect(connSvc, configServiceName);
    if (connected) {
      if (!pcFolderConfig.connect(coolSvc, coolServiceName, coolDbName, false)) {
        connected = false;
        paFolderConfig.disconnect();
      }
    }

    if (connected) {
      // Get folders configured from PVSS which are not marked as removed
      std::vector<FolderConfigurationInformation> pvssFolders = paFolderConfig.readFolderConfigurationTable(folderConfigurationTable, folderGroup, true, FW_CONDDB_STATUS_ANY);

      // Get folders which exist in COOL DB
      std::vector<std::string> coolFolders;
      status = pcFolderConfig.getFolders(coolFolders);

      if (status) {
        /*
        for (std::vector<std::string>::iterator iIter = coolFolders.begin(); iIter != coolFolders.end(); iIter++)
          std::cout << *iIter << std::endl;
          */
        std::vector<FolderConfigurationInformation> toBeCreated;
        std::vector<FolderConfigurationInformation> alreadyDefined;
//        std::vector<FolderConfigurationInformation> toBeRemoved;
        for (std::vector<FolderConfigurationInformation>::iterator iIter = pvssFolders.begin(); iIter != pvssFolders.end(); iIter++) {

        // Loop through all folders, checking which need to be created, and which already exist
          std::vector<std::string>::iterator iExists = std::find(coolFolders.begin(),
                                                                 coolFolders.end(),
                                                                 iIter->folderName());
//          std::cout << iIter->folderName() << std::endl;
          if (iExists != coolFolders.end()) {
            // Folder already exists in COOL DB
            alreadyDefined.push_back(*iIter);
          } else {
            // Folder is new and needs to be created
            toBeCreated.push_back(*iIter);
          }
        }

        if (toBeCreated.empty()) {
          std::cout << "No new folders have been defined for COOL Database " << coolDbName << std::endl;
        } else {
          std::cout << "These folders will be created in the COOL Database " << coolDbName << std::endl;
          for (std::vector<FolderConfigurationInformation>::iterator iIter = toBeCreated.begin(); iIter != toBeCreated.end(); iIter++) {
            std::cout << "  " << iIter->folderName() << std::endl;
          }
        }

        // Loop through and create the non-existing folders
        for (std::vector<FolderConfigurationInformation>::iterator iIter = toBeCreated.begin(); iIter != toBeCreated.end(); iIter++) {
          std::vector<FolderFieldConfiguration>& folderFields = paFolderConfig.readFolderFieldConfigurationTable(iIter->folderId());

          // Can safely create this folder as it does not exist
          if (pcFolderConfig.createFolder(iIter->folderName(),
                                          std::string(PVSS_COOL_FOLDER_DESC_MULTI_CHANNEL),
                                          folderFields)) {
            paFolderConfig.changeStatus(folderConfigurationTable, iIter->folderId(), FW_CONDDB_FOLDERSTAT_EXISTS);
            std::string updateTime = CondDBUtilities::formatTime(CondDBUtilities::getTimeNow());
            paFolderConfig.updateLastUpdateTime(folderConfigurationTable, iIter->folderId(), updateTime);
            paFolderConfig.updateLastUpdateTime(folderConfigurationTable, iIter->folderId(), updateTime, false);
            paFolderConfig.updateFields(iIter->folderId());
            paFolderConfig.updateLists(iIter->folderId());
            std::cout << "Folder '" << iIter->folderName() << "' has been created in COOL Database " << coolDbName << std::endl;
          } else {
            std::cout << "Folder '" << iIter->folderName() << "' could not be created in COOL Database " << coolDbName << std::endl;
            paFolderConfig.changeStatus(folderConfigurationTable, iIter->folderId(), FW_CONDDB_FOLDERSTAT_SUSPENDED);
          }
        }

        if (!alreadyDefined.empty()) {
          std::cout << "These folders will be checked for existence in the COOL Database " << coolDbName << std::endl;
          for (std::vector<FolderConfigurationInformation>::iterator iIter = alreadyDefined.begin(); iIter != alreadyDefined.end(); iIter++) {
            std::cout << "  " << iIter->folderName() << std::endl;
          }
        }

        // Loop through all folders that should already exist
        for (std::vector<FolderConfigurationInformation>::iterator iIter = alreadyDefined.begin(); iIter != alreadyDefined.end(); iIter++) {
          std::vector<FolderFieldConfiguration>& folderFields = paFolderConfig.readFolderFieldConfigurationTable(iIter->folderId());
          std::vector<std::string> removed;
          std::vector<std::string> valid;
          std::vector<std::string> differentTypes;
          std::vector<std::string> newField;
          if (pcFolderConfig.existsFolderAndIsDifferent(*iIter, folderFields, removed, valid, differentTypes, newField)) {
            if (removed.empty() && differentTypes.empty() && newField.empty()) {
              // Do not need to create as already exists and nothing has changed
              std::cout << "Folder '" << iIter->folderName() << "' already exists in COOL Database " << coolDbName << " and configuration is identical" << std::endl;
            } else {
              // JRC CHANGE give more choices. If new fields added, they can be ignored (marked as removed immediately), if fields
              // have been removed, they can have NULLs inserted instead of values (marked as removed also)
              std::cout << "Folder '" << iIter->folderName() << "' already exists and the structure is different" << std::endl;
              std::cout << "This folder will be marked as 'removed' in CondDB" << std::endl;
              if (iIter->status() != FW_CONDDB_FOLDERSTAT_SUSPENDED) {
                paFolderConfig.changeStatus(folderConfigurationTable, iIter->folderId(), FW_CONDDB_FOLDERSTAT_SUSPENDED);
                std::string updateTime = CondDBUtilities::formatTime(CondDBUtilities::getTimeNow());
                paFolderConfig.updateLastUpdateTime(folderConfigurationTable, iIter->folderId(), updateTime);
                paFolderConfig.updateLastUpdateTime(folderConfigurationTable, iIter->folderId(), updateTime, false);
              }
            }
          }
        }

        // Mark any other folders that are inconsistent as removed
//        paFolderConfig.markAsRemoved(folderConfigurationTable, toBeRemoved);
      } else {
        std::cout << "Could not get folders from COOL DB" << std::endl;
      }

      // Disconnect from both databases
      paFolderConfig.disconnect();
      pcFolderConfig.disconnect();
    } else {
      std::cout << "Could not connect to either the CondDB configuration or COOL database" << std::endl;
    }
  }

  //==================
  // CATCH EXCEPTIONS
  //==================
  catch ( coral::Exception& e )
  {
    *error << "'configure()' CORAL exception : " << e.what() << std::endl << coral::MessageStream::endmsg;
    status = false;
  }
  catch ( std::exception& e )
  {
    *error << "'configure()' Standard C++ exception : " << e.what() << std::endl << coral::MessageStream::endmsg;
    status = false;
  }
  catch( ... )
  {
    *error << "'configure()' Unknown exception ..." << std::endl << coral::MessageStream::endmsg;
    status = false;
  }

  return (status);
}

bool transferToCool(std::string folderConfigurationTable,
                    PvssArchiveFolderConfig& paFolderConfig,
                    cool::IDatabaseSvc& coolSvc,
                    coral::IConnectionService& connSvc,
                    std::string dbName,
                    long folderGroup,
                    int rowCacheSize,
                    bool firstTime,
                    int timeSpan /* = 0 */,
                    bool showInfo /* = false */)
{
  bool status = true;
  int iovCount = 0;
  int tempTimeSpan;
  double readTime = 0.0;
  double insertTime = 0.0;
  PvssArchiveFolderData paFolderData;
  PvssCondDbFolderData pcFolderData;
  std::string endTime;
 // Introduced 21.06.10 by SK for determining time precision: msec for TRUE
  bool milliPrecision;
  std::vector<std::string> milliFolders;

  try {
    if (paFolderData.connect(connSvc, PVSS_COOL_PVSS_CONNECTION)) {
      if (pcFolderData.connect(coolSvc, PVSS_COOL_COOL_CONNECTION, dbName)) {
        if (paFolderConfig.connect(connSvc, PVSS_COOL_CONFIGURATION_CONNECTION)) {

// Corresponding attribute removed from the folder config => next line commented
//          paFolderConfig.resetIntervalCounters(firstTime);

          std::vector<FolderConfigurationInformation>& folderConfig = paFolderConfig.readFolderConfigurationTable(folderConfigurationTable, folderGroup, firstTime, FW_CONDDB_STATUS_ANY);

          // Check at least one folder is defined
          if (firstTime && folderConfig.empty()) {
            std::cout << std::endl << " No folders have been defined, so no data can be transferred. You must run 'configurePvssCool' before trying again" << std::endl << std::endl;
            status = false;
          } else {
  		    // Commented 13.02.17 as not used any more
		    // initiateMilliFolderList(milliFolders);

            for (std::vector<FolderConfigurationInformation>::iterator iIter = folderConfig.begin(); iIter != folderConfig.end(); iIter++) {

              // First check for any suspended folders that should be re-instated
              if ((iIter->status() == FW_CONDDB_FOLDERSTAT_SUSPENDED) && (iIter->statusReq() == FW_CONDDB_REQFOLDERSTAT_REINSTATE)) {

                // Check if update time is in the future, in which case ignore this folder for now
                if (CondDBUtilities::getTimeDifference(CondDBUtilities::getTimeStamp(iIter->lastUpdateAfter()), CondDBUtilities::getTimeNow()) > 0) {
                  PvssCoolStatistics myStats;
                  paFolderConfig.changeStatus(folderConfigurationTable, iIter->folderId(), FW_CONDDB_FOLDERSTAT_EXISTS);
                  /*
                  // Reset statistics?
                  myStats.resetStatistics(connSvc, iIter->folderName());
                  */
                  myStats.setRunning(connSvc, iIter->folderName());
                }
              }

              // Check folder exists and should have data transferred
              if ((iIter->status() == FW_CONDDB_FOLDERSTAT_EXISTS) && (iIter->statusReq() == FW_CONDDB_REQFOLDERSTAT_NOCHANGE)) {
                std::vector<FolderFieldConfiguration>& folderFields = paFolderConfig.readFolderFieldConfigurationTable(iIter->folderId(), FW_CONDDB_STATUS_NOCHANGE);
                // Calculate the end time required from the last update time and the timespan requested
				
				// Several new lines introduced 25.10.11 by SK for possible correction of last update time
				if(checkDbSyncFile(iIter->folderId())) {
					iIter->lastUpdateAfter(iIter->lastUpdateBefore());		// Correct update time that was not stored due to archive DB problem
			    }
				// End of new lines as of 25.10.11
				
                tempTimeSpan = timeSpan;
	      		endTime = CondDBUtilities::addSeconds(iIter->lastUpdateAfter(), tempTimeSpan);

                // Final check that we have a valid time range
		        if (tempTimeSpan > 0) {

                  // From the start and end times, get the (possibly) different archive numbers for the relevant group
                  std::vector<PvssArchiveNumber> archiveNumbers;
                  PvssArchiveUtilities::getArchiveNumbers(paFolderData.connection(),
                                                          archiveNumbers,
                                                          iIter->group(),
                                                          iIter->lastUpdateAfter(),
														  endTime);

                  if (archiveNumbers.size() > 1)
                    std::cout << "Table switch detected, will retrieve data with multiple passes" << std::endl;

                  for (std::vector<PvssArchiveNumber>::iterator iArcNumber = archiveNumbers.begin(); (iArcNumber != archiveNumbers.end()) && status; iArcNumber++) {
                    if (showInfo) 
                      std::cout << std::string("Retrieving folder data for '") << iIter->folderName() << "' between " << iArcNumber->startTime() << " and " 
					  			<< iArcNumber->endTime() << " from the archive table " << iArcNumber->archiveNumber() << std::endl;
							//	", time is " << CondDBUtilities::formatTime(CondDBUtilities::getTimeNow(), false) << std::endl;
					  
				   // Introduced 22.06.10 by SK for milliPrecision
					// milliPrecision = isMilliFolder(milliFolders, iIter->folderName());  // replaced 13.02.17 by the following
					if(iIter->timePrecision() == TIME_PRECISION_SECONDS)
						milliPrecision = false;
					else
						milliPrecision = true;

				   // End of the code for milliPrecision
                    // Dbg insertion
					// if(iIter->folderName() != "/TGC/DCS/PSHVVMON")
					//	continue;
					// End of Dbg insertion
					
					coral::ICursor& pvssData = paFolderData.getFolderData(folderConfigurationTable,
					                                                      *iIter,
                                                                          folderFields,
                                                                          *iArcNumber,
                                                                          rowCacheSize,
                                                                          readTime,
									  milliPrecision);	// Last parameter introduced 21.06.10 by SK
									  
                    paFolderConfig.updateLastUpdateTime(folderConfigurationTable, iIter->folderId(),
                                                        archiveNumbers.back().endTime());

                    status = pcFolderData.writeFolderSmoothed(iIter->folderName(),
                                                              pvssData,
                                                              folderFields,
                                                              iIter->channelIdType(),
                                                              iovCount,
                                                              insertTime);

                    if (showInfo)
                      std::cout << iovCount << " IOVs written, Read Time - " << readTime << ", Insert Time - " << insertTime << std::endl;
					  
                    if (status) {
				  	  // New line introduced 26.10.11 by SK for possible correction of last update time
					  createDbSyncFile(iIter->folderId());	// To keep track, if the following function crashes due to no connection to DB
					  // End of new lines as of 26.10.11
				
                      paFolderConfig.updateLastUpdateTime(folderConfigurationTable, iIter->folderId(),
                                                          archiveNumbers.back().endTime(),
                                                          false);

					  // New line introduced 26.10.11 by SK for possible correction of last update time
					  removeDbSyncFile(iIter->folderId());	// Not needed any more, as the previous function was executed
					  // End of new lines as of 26.10.11
					
                      PvssCoolStatistics myStats;
                      myStats.updateStatistics(connSvc,
                                               iIter->folderName(),
                                               iovCount,
                                               readTime,
                                               insertTime,
                                               tempTimeSpan,
                                               archiveNumbers.back().endTime());
                    }
                    paFolderData.cleanUp();
                  } // End of loop over event history tables
                } // End if for valid time range

              // Check whether folder should be suspended from tranferring data
              } else if ((iIter->status() == FW_CONDDB_FOLDERSTAT_EXISTS) && (iIter->statusReq() == FW_CONDDB_REQFOLDERSTAT_SUSPEND)) {
                // Close any and all open IOVs
                long iovsClosed = 0;
                if (pcFolderData.closeIOVs(iIter->folderName(), iIter->lastUpdateAfter(), iovsClosed)) {
                  PvssCoolStatistics myStats;
                  paFolderConfig.changeStatus(folderConfigurationTable, iIter->folderId(), FW_CONDDB_FOLDERSTAT_SUSPENDED);
                  myStats.setRunning(connSvc, iIter->folderName(), false);
                  if (showInfo)
                    std::cout << "Folder " << iIter->folderName() << " suspended, " << iovsClosed << " IOV(s) closed" << std::endl;
                } else {
                  std::cout << "Error encountered while attempting to close IOVs for folder " << iIter->folderName() << std::endl;
                }
              }
            } // End of loop over folders

            if (!firstTime)
              paFolderConfig.decrementIntervalCounters(folderConfigurationTable);
          }

          paFolderConfig.disconnect();
        }
        pcFolderData.disconnect();
      }
      paFolderData.disconnect();
    }
  }

  //==================
  // CATCH EXCEPTIONS
  //==================
  catch ( coral::Exception& e )
  {
    *error << "'transferToCool()' CORAL exception : " << e.what() << std::endl << coral::MessageStream::endmsg;
    status = false;
  }
  catch ( std::exception& e )
  {
    *error << "'transferToCool()' Standard C++ exception : " << e.what() << std::endl << coral::MessageStream::endmsg;
    status = false;
  }
  catch( ... )
  {
    *error << "'transferToCool()' Unknown exception ..." << std::endl << coral::MessageStream::endmsg;
    status = false;
  }

  return (status);
}

// ============================================================================
//              COMMAND FUNCTIONS
// ----------------------------------------------------------------------------
// configurePvssCool
// ----------------------------------------------------------------------------
void configurePvssCool(std::string folderConfigurationTable,
                       cool::IDatabaseSvc& coolSvc,
                       coral::IConnectionService& connSvc,
                       std::string configSvcName,
                       std::string coolSvcName,
                       std::string coolDbName,
                       long folderGroup)
{
  try {

    // Create COOL database if it does not exist
    if (createCOOLDatabase(coolSvc, coolSvcName, coolDbName)) {

      // Read configuration information and (re)create required folders if confirmed
      configure(folderConfigurationTable, coolSvc, connSvc, configSvcName, coolSvcName, coolDbName, folderGroup);

    }
  }

  //==================
  // CATCH EXCEPTIONS
  //==================
  catch ( coral::Exception& e )
  {
    *error << "'configurePvssCool()' CORAL exception : " << e.what() << std::endl << coral::MessageStream::endmsg;
  }
  catch ( std::exception& e )
  {
    *error << "'configurePvssCool()' Standard C++ exception : " << e.what() << std::endl << coral::MessageStream::endmsg;
  }
  catch( ... )
  {
    *error << "'configurePvssCool()' Unknown exception ..." << std::endl << coral::MessageStream::endmsg;
  }

  return;
}

// ----------------------------------------------------------------------------
// verifyCondDbWithCool
// ----------------------------------------------------------------------------
void verifyCondDbWithCool(std::string folderConfigurationTable,
                          coral::IConnectionService& connSvc,
                          cool::IDatabaseSvc& coolSvc,
                          std::string dbName,
                          long folderGroup)
{
  PvssArchiveFolderConfig paFolderConfig;
  PvssCondDbFolderConfig pcFolderConfig;

  if (paFolderConfig.connect(connSvc, PVSS_COOL_CONFIGURATION_CONNECTION)) {
    if (pcFolderConfig.connect(coolSvc, PVSS_COOL_COOL_CONNECTION, dbName)) {
      std::vector<FolderConfigurationInformation> condDBFolders = paFolderConfig.readFolderConfigurationTable(folderConfigurationTable, 
	  																							folderGroup, true, FW_CONDDB_STATUS_ANY);
      if (condDBFolders.empty()) {
        std::cout << std::endl << " No folders have been defined for the CondDB process, so there is nothing to verify." << std::endl << std::endl;
      } else {
        for (std::vector<FolderConfigurationInformation>::iterator iIter = condDBFolders.begin(); iIter != condDBFolders.end(); iIter++) {
          if (iIter->status() == FW_CONDDB_FOLDERSTAT_SUSPENDED) {
            std::cout << "Folder '" << iIter->folderName() << "' has been marked as suspended/removed and so will not be checked" << std::endl;
          } else {
            std::vector<FolderFieldConfiguration> condDBFieldConfig = paFolderConfig.readFolderFieldConfigurationTable(iIter->folderId());
            std::vector<std::string> removed;
            std::vector<std::string> valid;
            std::vector<std::string> differentTypes;
            std::vector<std::string> newField;
            if (pcFolderConfig.existsFolderAndIsDifferent(*iIter, condDBFieldConfig, removed, valid, differentTypes, newField)) {
              if (removed.empty() && differentTypes.empty() && newField.empty()) {
                std::cout << "Folder '" << iIter->folderName() << "' exists in COOL and is identical" << std::endl;
              } else {
                std::cout << "Folder '" << iIter->folderName() << "' exists in COOL but is different" << std::endl;
                std::cout << "This folder must be renamed" << std::endl;

                // Display information found
                std::cout << "These fields configured in CondDB are not yet defined in COOL:" << std::endl;
                if (newField.empty()) {
                  std::cout << "\t<None>" << std::endl;
                } else {
                  for (std::vector<std::string>::iterator iTemp = newField.begin(); iTemp != newField.end(); iTemp++)
                    std::cout << "\t" << *iTemp << std::endl;
                }
                std::cout << std::endl;
                std::cout << "These fields configured in COOL have been removed from CondDB:" << std::endl;
                if (removed.empty()) {
                  std::cout << "\t<None>" << std::endl;
                } else {
                  for (std::vector<std::string>::iterator iTemp = removed.begin(); iTemp != removed.end(); iTemp++)
                    std::cout << "\t" << *iTemp << std::endl;
                }
                std::cout << std::endl;
                std::cout << "These fields configured in CondDB exist in COOL but have different types:" << std::endl;
                if (differentTypes.empty()) {
                  std::cout << "\t<None>" << std::endl;
                } else {
                  for (std::vector<std::string>::iterator iTemp = differentTypes.begin(); iTemp != differentTypes.end(); iTemp++)
                    std::cout << "\t" << *iTemp << std::endl;
                }
                std::cout << std::endl;
                std::cout << "These fields configured in CondDB are consistent with COOL:" << std::endl;
                if (valid.empty()) {
                  std::cout << "\t<None>" << std::endl;
                } else {
                  for (std::vector<std::string>::iterator iTemp = valid.begin(); iTemp != valid.end(); iTemp++)
                    std::cout << "\t" << *iTemp << std::endl;
                }
                std::cout << std::endl;
              }
            } else {
              std::cout << "Folder '" << iIter->folderName() << "' does not exist in COOL and can be created" << std::endl;
            }
          }
        }
      }
      pcFolderConfig.disconnect();
    }
    paFolderConfig.disconnect();
  }

  return;
}

// ----------------------------------------------------------------------------
// runOnce
// ----------------------------------------------------------------------------
void runOnce(std::string folderConfigTable,
             cool::IDatabaseSvc& coolSvc,
             coral::IConnectionService& connSvc,
             std::string dbName,
             long folderGroup,
             int rowCacheSize,
             int timeSpan /* = 0 */,
             bool showInfo /* = false */)
{
  bool bStatus = true;

  PvssArchiveFolderConfig paFolderConfig;

  // Get data from archive and write it to COOL
  bStatus = transferToCool(folderConfigTable, paFolderConfig, coolSvc, connSvc, dbName, folderGroup, rowCacheSize, true, timeSpan, showInfo);
  if (!bStatus) {
    std::cout << "Error occurred during transfer, time is " << CondDBUtilities::formatTime(CondDBUtilities::getTimeNow(), false) << std::endl;
  }

  return;
}

// ============================================================================
//              DEBUG FUNCTIONS
// ----------------------------------------------------------------------------
// readData
// ----------------------------------------------------------------------------
void readData(std::string folderConfigurationTable,
              coral::IConnectionService& connSvc,
              long folderGroup,
              int rowCacheSize,
              int iMaxRows,
              int timeSpan /* = 0 */)
{
  bool firstTime = true;

  int iRowCount = 0;
//  int iStoreCount = 0;
  int iDisplayedRows = 0;
  int tempTimeSpan;

  double readTime = 0.0;

  std::string endTime;

  PvssArchiveFolderData paFolderData;

  PvssArchiveFolderConfig paFolderConfig;

  try {
    if (paFolderData.connect(connSvc, PVSS_COOL_PVSS_CONNECTION)) {
      if (paFolderConfig.connect(connSvc, PVSS_COOL_CONFIGURATION_CONNECTION)) {
        std::vector<FolderConfigurationInformation>& folderConfig = paFolderConfig.readFolderConfigurationTable(folderConfigurationTable, folderGroup, firstTime);

        if (folderConfig.empty()) {
          std::cout << std::endl << " No folders have been defined for the CondDB process, so there is no data to read." << std::endl << std::endl;
        } else {

          // Loop through each folder in turn
          for (std::vector<FolderConfigurationInformation>::iterator iIter = folderConfig.begin(); iIter != folderConfig.end(); iIter++) {

            // Get the field information for the current folder
            std::vector<FolderFieldConfiguration>& folderFields = paFolderConfig.readFolderFieldConfigurationTable(iIter->folderId(), FW_CONDDB_STATUS_NOCHANGE);

            // Calculate the end time required from the last update time and the timespan requested
            tempTimeSpan = timeSpan;
            endTime = CondDBUtilities::addSeconds(iIter->lastUpdateAfter(), tempTimeSpan);
//            std::cout << iIter->lastUpdateAfter() << ", " << endTime << " (Timespan = " << tempTimeSpan << ")" << std::endl;

            // Final check for valid time range
            if (tempTimeSpan > 0) {

              // From the start and end times, get the (possibly) different archive numbers for the relevant group
              std::vector<PvssArchiveNumber> archiveNumbers;
              PvssArchiveUtilities::getArchiveNumbers(paFolderData.connection(), archiveNumbers, iIter->group(), iIter->lastUpdateAfter(), endTime);

              if (archiveNumbers.size() > 1)
                std::cout << "Table switch detected, will retrieve data with multiple passes" << std::endl;

              iRowCount = 0;
              iDisplayedRows = 0;
//              iStoreCount = 0;
              for (std::vector<PvssArchiveNumber>::iterator iArcNumber = archiveNumbers.begin(); iArcNumber != archiveNumbers.end(); iArcNumber++) {
                std::cout << std::string("Retrieving folder data for '") << iIter->folderName() << "' between " << iArcNumber->startTime() << " and " << iArcNumber->endTime() << ", time is " << CondDBUtilities::formatTime(CondDBUtilities::getTimeNow(), false) << std::endl;
                coral::ICursor& pvssData = paFolderData.getFolderData(folderConfigurationTable,
				                                                      *iIter,
                                                                      folderFields,
                                                                      *iArcNumber,
                                                                      rowCacheSize,
                                                                      readTime,
																	  false);
    
                std::cout << std::string("Folder data retrieved, query time: ") << readTime << std::endl;

                if (pvssData.next()) {
                  do {
                    const coral::AttributeList& row = pvssData.currentRow();
                    row.toOutputStream(std::cout);
                    std::cout << std::endl;
                    iDisplayedRows++;
                    iRowCount++;
                  } while (pvssData.next() && (iDisplayedRows < iMaxRows)); // End of while loop over rows to display

                  while (pvssData.next()) {
                    iRowCount++;
                  }
                }
                paFolderData.cleanUp();
              }
              std::cout << iDisplayedRows << " row(s) displayed (out of " << iRowCount << "), update time = " << archiveNumbers.back().endTime() << std::endl << std::endl;
            } else {
              std::cout << "Start time is in future. Nothing to query!" << std::endl << std::endl;
            }
          }
        }
        paFolderConfig.disconnect();
      }
      paFolderData.disconnect();
    }
  }

  //==================
  // CATCH EXCEPTIONS
  //==================
  catch ( coral::Exception& e )
  {
    std::cout << "'readData()' CORAL exception : " << e.what() << std::endl;
  }
  catch ( std::exception& e )
  {
    std::cout << "'readData()' Standard C++ exception : " << e.what() << std::endl;
  }
  catch( ... )
  {
    std::cout << "'readData()' Unknown exception ..." << std::endl;
  }

  return;
}

// ----------------------------------------------------------------------------
// readFolders
// ----------------------------------------------------------------------------
void readFolders(std::string folderConfigurationTable,
                 coral::IConnectionService& connSvc,
                 long folderGroup,
                 bool ignoreRemoved)
{
  long withState = FW_CONDDB_FOLDERSTAT_EXISTS;

  PvssArchiveFolderConfig paFolderConfig;

  try {

    if (paFolderConfig.connect(connSvc, PVSS_COOL_CONFIGURATION_CONNECTION)) {
      if (!ignoreRemoved)
        withState = FW_CONDDB_STATUS_ANY;
      std::vector<FolderConfigurationInformation>& folderConfig = paFolderConfig.readFolderConfigurationTable(folderConfigurationTable, folderGroup, true, withState);

      if (folderConfig.empty()) {
        std::cout << std::endl << " No folders have been defined in CondDB." << std::endl << std::endl;
      } else {
        std::cout << "There are " << folderConfig.size() << " folders defined in CondDB";
        if (folderGroup != PVSS_COOL_ALL_FOLDERS)
          std::cout << " for folder group " << folderGroup;
        std::cout << "." << std::endl << std::endl;
        if (ignoreRemoved) {
          std::cout << "  Update Time\t\tFolder Group\tFolder Name" << std::endl;
          std::cout << "  -----------\t\t------------\t-----------" << std::endl;
        } else {
          // JRC CHANGE needs to show any possible folder status information
          std::cout << "  Update Time\t\tSuspended/Removed\tFolder Group\tFolder Name" << std::endl;
          std::cout << "  -----------\t\t-----------------\t------------\t-----------" << std::endl;
        }
        for (std::vector<FolderConfigurationInformation>::iterator iIter = folderConfig.begin(); iIter != folderConfig.end(); iIter++) {
          if (ignoreRemoved)
            std::cout << (iIter->lastUpdateAfter().length() == 0 ? "  Never updated\t" : iIter->lastUpdateAfter()) << "\t " << iIter->folderGroup() << "\t\t" << iIter->folderName() << std::endl;
          else
            std::cout << (iIter->lastUpdateAfter().length() == 0 ? "  Never updated\t" : iIter->lastUpdateAfter()) << "\t " << ((iIter->status() == FW_CONDDB_FOLDERSTAT_SUSPENDED) ? "true" : "false") << "\t\t\t  " << iIter->folderGroup() << "\t\t" << iIter->folderName() << std::endl;
        }
      }
      paFolderConfig.disconnect();
    }
  }

  //==================
  // CATCH EXCEPTIONS
  //==================
  catch ( coral::Exception& e )
  {
    *error << "'readFolders()' CORAL exception : " << e.what() << std::endl << coral::MessageStream::endmsg;
  }
  catch ( std::exception& e )
  {
    *error << "'readFolders()' Standard C++ exception : " << e.what() << std::endl << coral::MessageStream::endmsg;
  }
  catch( ... )
  {
    *error << "'readFolders()' Unknown exception ..." << std::endl << coral::MessageStream::endmsg;
  }

  return;
}

// ----------------------------------------------------------------------------
// updateTime
// ----------------------------------------------------------------------------
void updateTime(std::string folderConfigurationTable,
                coral::IConnectionService& connSvc,
                long folderGroup, /* = PVSS_COOL_ALL_FOLDERS */
                std::string folderName, /* = "" */
                std::string time /* = "" */)
{
  bool proceed = true;

  std::size_t iPos;

  std::string answer;

  PvssArchiveFolderConfig paFolderConfig;

  try {

    // Check if time has been given
    if (time.empty()) {
      time = CondDBUtilities::formatTime(CondDBUtilities::getTimeNow());
    } else {
      // Remove the underscore from the time given
      iPos = time.find_first_of("_");
      if (iPos != std::string::npos)
        time[iPos] = ' ';
    }

    if (paFolderConfig.connect(connSvc, PVSS_COOL_CONFIGURATION_CONNECTION)) {
      std::vector<FolderConfigurationInformation>& folderConfig = paFolderConfig.readFolderConfigurationTable(folderConfigurationTable, folderGroup, true);
      if (folderName.empty()) {
        if (folderConfig.empty()) {
          if (folderGroup != PVSS_COOL_ALL_FOLDERS)
            std::cout << "No folders have been configured for folder group " << folderGroup << " or they are all marked as removed" << std::endl;
          else
            std::cout << "No folders have been configured in CondDB or they are all marked as removed" << std::endl;
        } else {
          // Request confirmation before doing it
          std::cout << "  Update Time\t\tFolder Group\tFolder Name" << std::endl;
          std::cout << "  -----------\t\t------------\t-----------" << std::endl;
          for (std::vector<FolderConfigurationInformation>::iterator iIter = folderConfig.begin(); iIter != folderConfig.end(); iIter++)
            std::cout << iIter->lastUpdateAfter() << "\t  " << iIter->folderGroup() << "\t" << iIter->folderName() << std::endl;
          std::cout << "Are you sure you want to change the time for the above folders to " << time << "? (Y/N)" << std::endl;
          std::cin >> answer;
          if (answer.compare("Y") == 0) {
            for (std::vector<FolderConfigurationInformation>::iterator iIter = folderConfig.begin(); iIter != folderConfig.end(); iIter++) {
              paFolderConfig.updateLastUpdateTime(folderConfigurationTable, iIter->folderId(), time);
              paFolderConfig.updateLastUpdateTime(folderConfigurationTable, iIter->folderId(), time, false);
            }
            std::cout << "'updateTime' operation complete" << std::endl;
          } else {
            std::cout << "'updateTime' operation cancelled" << std::endl;
          }
        }
      } else {
        // Change the time for the folder specified
        std::vector<FolderConfigurationInformation>::iterator iIter = std::find(folderConfig.begin(),
                                                                                folderConfig.end(),
                                                                                folderName);

        // Check folder has been found
        if (iIter != folderConfig.end()) {
          // Quick check for if a system number was also given
          if (folderGroup != PVSS_COOL_ALL_FOLDERS) {
            if (folderGroup != iIter->folderGroup()) {
              std::cout << "Folder '" << folderName << "' was not found in folder group " << folderGroup << ", please check folder group number and spelling" << std::endl;
              proceed = false;
            }
          }

          if (proceed) {
            // Request confirmation before doing it
            std::cout << "Are you sure you want to change the time for the folder '" << folderName << "' to " << time << "? (Y/N)" << std::endl;
            std::cin >> answer;
            if (answer.compare("Y") == 0) {
              paFolderConfig.updateLastUpdateTime(folderConfigurationTable, iIter->folderId(), time);
              paFolderConfig.updateLastUpdateTime(folderConfigurationTable, iIter->folderId(), time, false);
              std::cout << "'updateTime' operation complete" << std::endl;
            } else {
              std::cout << "'updateTime' operation cancelled" << std::endl;
            }
          }
        } else {
          std::cout << "Folder '" << folderName << "' was not found, please check spelling" << std::endl;
        }
      }
      paFolderConfig.disconnect();
    }
  
  }

  //==================
  // CATCH EXCEPTIONS
  //==================
  catch ( coral::Exception& e )
  {
    *error << "'updateTime()' CORAL exception : " << e.what() << std::endl << coral::MessageStream::endmsg;
  }
  catch ( std::exception& e )
  {
    *error << "'updateTime()' Standard C++ exception : " << e.what() << std::endl << coral::MessageStream::endmsg;
  }
  catch( ... )
  {
    *error << "'updateTime()' Unknown exception ..." << std::endl << coral::MessageStream::endmsg;
  }

  return;
}

// ----------------------------------------------------------------------------
// updateTime
// ----------------------------------------------------------------------------
void closeIOVs(std::string folderConfigurationTable,
               cool::IDatabaseSvc& coolSvc,
               coral::IConnectionService& connSvc,
               std::string coolDbName,
               std::string folderName,
               std::string time)
{
  bool bStatus = false;

  std::size_t iPos;

  std::string answer;

  long iovsClosed = 0;

  PvssArchiveFolderConfig paFolderConfig;
  PvssCondDbFolderData pcFolderData;

  try {
    if (paFolderConfig.connect(connSvc, PVSS_COOL_CONFIGURATION_CONNECTION)) {
      std::vector<FolderConfigurationInformation>& folderConfig = paFolderConfig.readFolderConfigurationTable(folderConfigurationTable, PVSS_COOL_ALL_FOLDERS, true, FW_CONDDB_STATUS_ANY);

      if (folderConfig.empty()) {
        std::cout << std::endl << " No folders have been defined in CondDB." << std::endl << std::endl;
      } else {
        // Check the folder specified does really exist
        std::vector<FolderConfigurationInformation>::iterator iIter = std::find(folderConfig.begin(),
                                                                                folderConfig.end(),
                                                                                folderName);

        // Check folder has been found
        if (iIter != folderConfig.end()) {

          // Check if time has been given
          if (time.empty()) {
            time = iIter->lastUpdateAfter();
          } else {
            // Remove the underscore from the time given
            iPos = time.find_first_of("_");
            if (iPos != std::string::npos)
              time[iPos] = ' ';
          }

          std::cout << "Are you sure you want to close the IOVs for all channels in the folder '" << folderName << "' at time " << time << "? (Y/N)" << std::endl;
          std::cin >> answer;
          if (answer.compare("Y") == 0) {
            if (pcFolderData.connect(coolSvc, PVSS_COOL_COOL_CONNECTION, coolDbName)) {
              bStatus = pcFolderData.closeIOVs(folderName, time, iovsClosed);
              pcFolderData.disconnect();
            }

            if (bStatus)
              std::cout << "'closeIOVs' operation complete for COOL folder '" << folderName << "'. " << iovsClosed << " IOVs closed" << std::endl;
            else
              std::cout << "Error occurred closing IOVs for COOL folder '" << folderName << "'" << std::endl;
          } else {
            std::cout << "'closeIOVs' operation cancelled for COOL folder '" << folderName << "'" << std::endl;
          }
        } else {
          std::cout << "Folder '" << folderName << "' was not found, please check spelling" << std::endl;
        }
      }
      paFolderConfig.disconnect();
    }
  }

  //==================
  // CATCH EXCEPTIONS
  //==================
  catch ( coral::Exception& e )
  {
    *error << "'closeIOVs()' CORAL exception : " << e.what() << std::endl << coral::MessageStream::endmsg;
  }
  catch ( std::exception& e )
  {
    *error << "'closeIOVs()' Standard C++ exception : " << e.what() << std::endl << coral::MessageStream::endmsg;
  }
  catch( ... )
  {
    *error << "'closeIOVs()' Unknown exception ..." << std::endl << coral::MessageStream::endmsg;
  }

  return;
}

//-----------------------------------------------------------------------------
