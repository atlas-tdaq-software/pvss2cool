#ifndef __FOLDER_FIELD_CONFIGURATION_H__  // Gate keeper code
#define __FOLDER_FIELD_CONFIGURATION_H__

// Include file for class which stores field information for folders
//
// Created: 27.03.2007
// Author:  Jim Cook
// Modified: 09.11.12 by SK: introducing the channelName into the field configuration
//                           caused by "Collection" folder type

// Include Files
// -------------
#include <iostream>
#include <string>

// Class Definitions
// -----------------
class FolderFieldConfiguration {
  public:
    FolderFieldConfiguration();
    ~FolderFieldConfiguration() {;}
   // Main constructor extended with newChannelName 09.11.12 by SK
    FolderFieldConfiguration(bool newIsId, long newFolderId, long newPosition, long newStatus, long newTimeout, double newDeadband, 
							 std::string newDpElementName, std::string alteredFieldName, std::string newNewFieldName, 
//							 std::string newCoolFieldType, std::string newPvssFieldType, std::string newChannelName);
							 std::string newCoolFieldType, std::string newPvssFieldType);
    FolderFieldConfiguration(const FolderFieldConfiguration &old);
    FolderFieldConfiguration& operator=(const FolderFieldConfiguration &rhs);
    bool operator==(const FolderFieldConfiguration &rhs);
    bool operator==(const std::string cmpFieldName);

    inline bool isId() {return (m_isId);}
    inline long folderId() {return (m_folderId);}
    inline long position() {return (m_position);}
    inline long status() {return (m_status);}
    inline long timeout() {return (m_timeout);}
    inline double deadband() {return (m_deadband);}
    inline std::string dpElementName() {return (m_dpElementName);}
    inline std::string fieldName() {return (m_fieldName);}
    inline std::string newFieldName() {return (m_newFieldName);}
    inline std::string coolFieldType() {return (m_coolFieldType);}
    inline std::string pvssFieldType() {return (m_pvssFieldType);}
   // added 09.11.12 by SK
//    inline std::string channelName() {return(m_channelName);}

    inline void isId(bool newIsId) {m_isId = newIsId;}
    inline void folderId(long newFolderId) {m_folderId = newFolderId;}
    inline void position(long newPosition) {m_position = newPosition;}
    inline void changeStatus(long newStatus) {m_status = newStatus;}
    inline void timeout(long newTimeout) {m_timeout = newTimeout;}
    inline void deadband(double newDeadband) {m_deadband = newDeadband;}
    inline void dpElementName(std::string newDpElementName) {m_dpElementName = newDpElementName;}
    inline void fieldName(std::string alteredFieldName) {m_fieldName = alteredFieldName;}
    inline void newFieldName(std::string newNewFieldName) {m_newFieldName = newNewFieldName;}
    void coolFieldType(std::string newFieldType);
    inline void pvssFieldType(std::string newFieldType) {m_pvssFieldType = newFieldType;}
   // added 09.11.12 by SK
//    inline void channelName(std::string newChannelName) {m_channelName = newChannelName;}
	
	inline void printtoerr() { std::cerr << m_folderId <<", "<<m_dpElementName <<", "<<m_fieldName << std::endl; }
  private:
    bool m_isId;

    long m_folderId;
    long m_position;
    long m_status;
    long m_timeout;

    double m_deadband;


    std::string m_dpElementName;
    std::string m_fieldName;
    std::string m_newFieldName;
    std::string m_coolFieldType;
    std::string m_pvssFieldType;
   // Introduced 09.11.12 by SK - needed for the 'Collection' folder type
	std::string m_channelName;
};

#endif // End of gate keeper code '__FOLDER_FIELD_CONFIGURATION_H__'
