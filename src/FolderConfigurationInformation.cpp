// Include Files
// -------------
#include "globals.h"
#include "FolderConfigurationInformation.h"

//-----------------------------------------------------------------------------
FolderConfigurationInformation::FolderConfigurationInformation()
: m_usesList(false),
  m_listChange(false),
  m_folderId(0),
  m_folderGroup(0),
//  m_mechanism(0),
//  m_interval(0),
  m_channelIdType(FW_CONDDB_IDINFO_DPNAME),
  m_dptInfo(FW_CONDDB_DPTINFO_SIMPLE),
  m_statusReq(FW_CONDDB_STATUS_NOCHANGE),
  m_status(FW_CONDDB_STATUS_NOCHANGE)
{
  this->m_folderName.erase();
  this->m_folderDescription.erase();
  this->m_group.erase();
  this->m_dpTypeName.erase();
  this->m_dpSubTypeName.erase();
  this->m_pattern.erase();
  this->m_channelIdInfo.erase();
  this->m_lastUpdateBefore.erase();
  this->m_lastUpdateAfter.erase();

  return;
}

FolderConfigurationInformation::FolderConfigurationInformation(bool useList,
                                                               bool listChanged,
                                                               long newFolderId,
                                                               long newFolderGroup,
//                                                               long newMechanism,
//                                                               long newInterval,
                                                               long newChannelIdType,
                                                               long newDptInfo,
                                                               long newStatusReq,
                                                               long newStatus,
                                                               std::string newFolderName,
                                                               std::string newFolderDescription,
                                                               std::string newGroup,
                                                               std::string newDpTypeName,
                                                               std::string newDpSubTypeName,
                                                               std::string newPattern,
                                                               std::string newChannelIdInfo,
                                                               std::string updateTimeBefore,
                                                               std::string updateTimeAfter,
															   std::string newTimePrecision)
{
  this->m_usesList = useList;
  this->m_listChange = listChanged;
  this->m_folderId = newFolderId;
  this->m_folderGroup = newFolderGroup;
//  this->m_mechanism = newMechanism;
//  this->m_interval = newInterval;
  this->m_channelIdType = newChannelIdType;
  this->m_dptInfo = newDptInfo;
  this->m_statusReq = newStatusReq;
  this->m_status = newStatus;
  this->m_folderName = newFolderName;
  this->m_folderDescription = newFolderDescription;
  this->m_group = newGroup;
  this->m_dpTypeName = newDpTypeName;
  this->m_dpSubTypeName = newDpSubTypeName;
  this->m_pattern = newPattern;
  this->m_channelIdInfo = newChannelIdInfo;
  this->m_lastUpdateBefore = updateTimeBefore;
  this->m_lastUpdateAfter = updateTimeAfter;
  this->m_timePrecision = newTimePrecision;
  return;
}

FolderConfigurationInformation::FolderConfigurationInformation(const FolderConfigurationInformation &old)
{
  this->m_usesList = old.m_usesList;
  this->m_listChange = old.m_listChange;
  this->m_folderId = old.m_folderId;
  this->m_folderGroup = old.m_folderGroup;
//  this->m_mechanism = old.m_mechanism;
//  this->m_interval = old.m_interval;
  this->m_channelIdType = old.m_channelIdType;
  this->m_dptInfo = old.m_dptInfo;
  this->m_statusReq = old.m_statusReq;
  this->m_status = old.m_status;
  this->m_folderName = old.m_folderName;
  this->m_folderDescription = old.m_folderDescription;
  this->m_group = old.m_group;
  this->m_dpTypeName = old.m_dpTypeName;
  this->m_dpSubTypeName = old.m_dpSubTypeName;
  this->m_pattern = old.m_pattern;
  this->m_channelIdInfo = old.m_channelIdInfo;
  this->m_lastUpdateBefore = old.m_lastUpdateBefore;
  this->m_lastUpdateAfter = old.m_lastUpdateAfter;
  this->m_timePrecision = old.m_timePrecision;
  return;
}

FolderConfigurationInformation &FolderConfigurationInformation::operator=(const FolderConfigurationInformation &rhs)
{
  this->m_usesList = rhs.m_usesList;
  this->m_listChange = rhs.m_listChange;
  this->m_folderId = rhs.m_folderId;
  this->m_folderGroup = rhs.m_folderGroup;
//  this->m_mechanism = rhs.m_mechanism;
//  this->m_interval = rhs.m_interval;
  this->m_channelIdType = rhs.m_channelIdType;
  this->m_dptInfo = rhs.m_dptInfo;
  this->m_statusReq = rhs.m_statusReq;
  this->m_status = rhs.m_status;
  this->m_folderName = rhs.m_folderName;
  this->m_folderDescription = rhs.m_folderDescription;
  this->m_group = rhs.m_group;
  this->m_dpTypeName = rhs.m_dpTypeName;
  this->m_dpSubTypeName = rhs.m_dpSubTypeName;
  this->m_pattern = rhs.m_pattern;
  this->m_channelIdInfo = rhs.m_channelIdInfo;
  this->m_lastUpdateBefore = rhs.m_lastUpdateBefore;
  this->m_lastUpdateAfter = rhs.m_lastUpdateAfter;
  this->m_timePrecision = rhs.m_timePrecision;
  return (*this);
}

bool FolderConfigurationInformation::operator==(const FolderConfigurationInformation &rhs)
{
  if ((this->m_folderId == rhs.m_folderId) && (this->m_folderName == this->m_folderName) &&
      (this->m_folderDescription == rhs.m_folderDescription) &&
      (this->m_folderGroup == rhs.m_folderGroup) &&
      (this->m_group == rhs.m_group) &&
      (this->m_dpTypeName == rhs.m_dpTypeName) && (this->m_dpSubTypeName == rhs.m_dpSubTypeName) &&
      (this->m_usesList == rhs.m_usesList) && (this->m_pattern == rhs.m_pattern) &&
      (this->m_dptInfo == rhs.m_dptInfo) &&
      (this->m_statusReq == rhs.m_statusReq) && (this->m_status == rhs.m_status) &&
//      (this->m_mechanism == rhs.m_mechanism) && (this->m_interval == rhs.m_interval) &&
      (this->m_channelIdType == rhs.m_channelIdType) && (this->m_channelIdInfo == rhs.m_channelIdInfo) &&
	  (this->m_timePrecision == rhs.m_timePrecision))
    return (true);
  else
    return (false);
}

bool FolderConfigurationInformation::operator==(const long cmpFolderId)
{
  if (this->m_folderId == cmpFolderId)
    return (true);
  else
    return (false);
}

bool FolderConfigurationInformation::operator==(const std::string cmpFolderName)
{
  if (this->m_folderName == cmpFolderName)
    return (true);
  else
    return (false);
}

std::ostream &FolderConfigurationInformation::toOutputStream(std::ostream &os,
                                                             bool showHeading /* = false */) const
{
  if (showHeading)
//    os << "folderId, folderName, folderGroup, group, dpTypeName, dpSubTypeName, usesList, pattern, dptInfo, mechanism, interval, channelIdType, channelIdInfo" << std::endl;
//  os << this->m_folderId << ", " << this->m_folderName << ", " << this->m_folderGroup << ", " << this->m_group << ", " << this->m_dpTypeName << ", " << this->m_dpSubTypeName << ", " << this->m_usesList << ", " << this->m_pattern << ", " << this->m_dptInfo << ", " << this->m_mechanism << ", " << this->m_interval << ", " << this->m_channelIdType << ", " << this->m_channelIdInfo << std::endl;
    os << "folderId, folderName, folderGroup, group, dpTypeName, dpSubTypeName, usesList, pattern, dptInfo, channelIdType, channelIdInfo, timePrecision" << std::endl;
  os << this->m_folderId << ", " << this->m_folderName << ", " << this->m_folderGroup << ", " << this->m_group << ", " << this->m_dpTypeName << ", " << this->m_dpSubTypeName << ", " << this->m_usesList << ", " << this->m_pattern << ", " << this->m_dptInfo << ", " << this->m_channelIdType << ", " << this->m_channelIdInfo << ", " << this->m_timePrecision << std::endl;
  return (os);
}
