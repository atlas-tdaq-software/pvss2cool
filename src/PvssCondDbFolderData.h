#ifndef __PVSS_CONDDB_FOLDER_DATA_H__  // Gate keeper code
#define __PVSS_CONDDB_FOLDER_DATA_H__

// Include file for class which writes data to the conditions database
//
// Created: 15.07.2005
// Author:  Jim Cook

// Include Files
// -------------
//#include "RelationalAccess/ISession.h"
#include "RelationalAccess/ISessionProxy.h"
#include "RelationalAccess/ISessionProperties.h"
#include "RelationalAccess/ICursor.h"
#include "FolderFieldConfiguration.h"

// Class Definitions
// -----------------
class PvssCondDbFolderData {
  public:
    PvssCondDbFolderData();
    ~PvssCondDbFolderData();

    bool writeFolder(std::string folderName, coral::ICursor &pvssData, long channelIdType, int &iovCount, double &insertTime);
    bool writeFolderSmoothed(std::string folderName, coral::ICursor &pvssData, std::vector<FolderFieldConfiguration>& folderFields, long channelIdType, int &iovCount, double &insertTime);
    bool closeIOVs(std::string folderName, std::string time, long &iovsClosed);
    bool connect(cool::IDatabaseSvc &coolService, std::string serviceName, std::string dbName);
    bool disconnect();

  private:
    cool::IDatabasePtr m_connection;
};

#endif // End of gate keeper code '__PVSS_CONDDB_FOLDER_DATA_H__'
