#ifndef __CONDDB_UTILITIES_H__  // Gate keeper code
#define __CONDDB_UTILITIES_H__

// Include file for class giving various utilities for the CondDB process
//
// Created: 30.03.2007
// Author:  Jim Cook

// Include Files
// -------------
#include "CoralBase/Attribute.h"
#include "CoralBase/AttributeList.h"
#include "CoralBase/TimeStamp.h"

#include "CoolKernel/ValidityKey.h"
#include "CoolKernel/Record.h"

#include <string>
#include <vector>
#ifdef CORAL300CPP11
	#include <chrono>
#endif
#include <algorithm>

// Commented 14.10.10 by SK
//class coral::TimeStamp;
//class coral::AttributeList;
class FolderFieldConfiguration;

// Class Definitions
// -----------------
class CondDBUtilities {
  public:
    CondDBUtilities();
    ~CondDBUtilities();

    static std::string addSeconds(std::string timeStart,
                                  int &timeSpan);
// Default value removed - incorrect syntax (gcc43) and also not necessary, as is not being used
//                                  int &timeSpan = 0);

    static double getTimeDifference(coral::TimeStamp startTime,
                                    coral::TimeStamp endTime);

    static coral::TimeStamp getTimeStamp(std::string time);

    static coral::TimeStamp getTimeNow();

    static std::string formatTime(coral::TimeStamp time,
                                  bool milli = false);

    static cool::ValidityKey convertTime(coral::TimeStamp time);
    static cool::ValidityKey convertTime(std::string time);

    static void copyRow(cool::Record& copyTo,
                        const coral::AttributeList& copyFrom,
                        const cool::Record& lastValues,
                        cool::ValidityKey& lastUpdate);
    static bool doStore(const coral::AttributeList& currentRow,
                        cool::Record& lastValues,
                        std::vector<FolderFieldConfiguration>& folderFields,
                        cool::ValidityKey& lastUpdate);
//    static bool copyRow(coral::AttributeList& copyTo,
//                        const coral::AttributeList& copyFrom,
//                        coral::AttributeList& lastValues);
};

#endif // End of gate keeper code '__CONDDB_UTILITIES_H__'
