// Include files
// -------------
#include "PvssArchiveNumber.h"

//-----------------------------------------------------------------------------

PvssArchiveNumber::PvssArchiveNumber()
: m_arcNumber(0)
{
  this->m_startTime.erase();
  this->m_endTime.erase();
  return;
}

PvssArchiveNumber::PvssArchiveNumber(long arcNumber,
                                       std::string timeStart,
                                       std::string timeEnd /* = std::string("") */)
{
  this->m_arcNumber = arcNumber;
  this->m_startTime = timeStart;
  this->m_endTime = timeEnd;
  return;
}

PvssArchiveNumber::PvssArchiveNumber(const PvssArchiveNumber &old)
{
  this->m_arcNumber = old.m_arcNumber;
  this->m_startTime = old.m_startTime;
  this->m_endTime = old.m_endTime;
  return;
}

PvssArchiveNumber& PvssArchiveNumber::operator=(const PvssArchiveNumber &rhs)
{
  this->m_arcNumber = rhs.m_arcNumber;
  this->m_startTime = rhs.m_startTime;
  this->m_endTime = rhs.m_endTime;
  return (*this);
}

bool PvssArchiveNumber::operator==(const PvssArchiveNumber &rhs)
{
  if ((this->m_arcNumber == rhs.m_arcNumber) &&
      (this->m_startTime.compare(rhs.m_startTime) == 0) &&
      (this->m_endTime.compare(rhs.m_endTime) == 0))
    return (true);
  else
    return (false);
}
