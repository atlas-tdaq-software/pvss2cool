// Include Files
// -------------
#include "globals.h"
#include "FolderFieldConfiguration.h"

//-----------------------------------------------------------------------------
FolderFieldConfiguration::FolderFieldConfiguration()
: m_isId(false),
  m_folderId(0),
  m_position(0),
  m_status(FW_CONDDB_STATUS_NOCHANGE),
  m_timeout(0),
  m_deadband(0.0)
{
  this->m_dpElementName.erase();
  this->m_fieldName.erase();
  this->m_newFieldName.erase();
  this->m_coolFieldType.erase();
  this->m_pvssFieldType.erase();
 // added 09.11.12 by SK. In ALL other methods corresponding
 // changes made WITHOUT any note
//  this->m_channelName.erase();

  return;
}

FolderFieldConfiguration::FolderFieldConfiguration(bool newIsId,
                                                   long newFolderId,
                                                   long newPosition,
                                                   long newStatus,
                                                   long newTimeout,
                                                   double newDeadband,
                                                   std::string newDpElementName,
                                                   std::string alteredFieldName,
                                                   std::string newNewFieldName,
                                                   std::string newCoolFieldType,
                                                   std::string newPvssFieldType) //,
//                                                   std::string newChannelName)
{
  this->m_isId = newIsId;
  this->m_folderId = newFolderId;
  this->m_position = newPosition;
  this->m_status = newStatus;
  this->m_timeout = newTimeout;
  this->m_deadband = newDeadband;
  this->m_dpElementName = newDpElementName;
  this->m_fieldName = alteredFieldName;
  this->m_newFieldName = newNewFieldName;
  this->m_coolFieldType = newCoolFieldType;
  this->m_pvssFieldType = newPvssFieldType;
//  this->m_channelName = newChannelName;
  return;
}

void FolderFieldConfiguration::coolFieldType(std::string newFieldType)
{
  this->m_coolFieldType = newFieldType;
  return;
}

FolderFieldConfiguration::FolderFieldConfiguration(const FolderFieldConfiguration &old)
{
  this->m_isId = old.m_isId;
  this->m_folderId = old.m_folderId;
  this->m_position = old.m_position;
  this->m_status = old.m_status;
  this->m_timeout = old.m_timeout;
  this->m_deadband = old.m_deadband;
  this->m_dpElementName = old.m_dpElementName;
  this->m_fieldName = old.m_fieldName;
  this->m_newFieldName = old.m_newFieldName;
  this->m_coolFieldType = old.m_coolFieldType;
  this->m_pvssFieldType = old.m_pvssFieldType;
//  this->m_channelName = old.m_channelName;
  return;
}

FolderFieldConfiguration &FolderFieldConfiguration::operator=(const FolderFieldConfiguration &rhs)
{
  this->m_isId = rhs.m_isId;
  this->m_folderId = rhs.m_folderId;
  this->m_position = rhs.m_position;
  this->m_status = rhs.m_status;
  this->m_timeout = rhs.m_timeout;
  this->m_deadband = rhs.m_deadband;
  this->m_dpElementName = rhs.m_dpElementName;
  this->m_fieldName = rhs.m_fieldName;
  this->m_newFieldName = rhs.m_newFieldName;
  this->m_coolFieldType = rhs.m_coolFieldType;
  this->m_pvssFieldType = rhs.m_pvssFieldType;
//  this->m_channelName = rhs.m_channelName;
  return (*this);
}

bool FolderFieldConfiguration::operator==(const FolderFieldConfiguration &rhs)
{
  if ((this->m_isId == rhs.m_isId) &&
      (this->m_dpElementName == rhs.m_dpElementName) &&
      (this->m_fieldName == rhs.m_fieldName) &&
      (this->m_coolFieldType == rhs.m_coolFieldType) &&
      (this->m_pvssFieldType == rhs.m_pvssFieldType)  // &&
//	  (this->m_channelName == rhs.m_channelName) 
	 )
    return (true);
  else
    return (false);
}

bool FolderFieldConfiguration::operator==(const std::string cmpFieldName)
{
  if (this->m_fieldName == cmpFieldName)
    return (true);
  else
    return (false);
}
