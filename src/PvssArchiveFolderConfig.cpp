// Include files
#include "CoralBase/Exception.h"
#include "CoolKernel/IDatabaseSvc.h"
#include "CoolApplication/Application.h"
#include "RelationalAccess/IConnectionService.h"
#include "RelationalAccess/ISessionProxy.h"
#include "RelationalAccess/IColumn.h"
#include "RelationalAccess/ISchema.h"
#include "RelationalAccess/IPrimaryKey.h"
#include "RelationalAccess/ITransaction.h"
#include "RelationalAccess/IQuery.h"
#include "RelationalAccess/ICursor.h"
#include "RelationalAccess/ITable.h"
#include "RelationalAccess/ITableDataEditor.h"
#include "RelationalAccess/IBulkOperation.h"
#include "RelationalAccess/SchemaException.h"
// Commented moved to private headers 14.10.10 by SK
//#include "CoralBase/Attribute.h"
//#include "CoralBase/AttributeList.h"
//#include "CoralBase/TimeStamp.h"
#include "CoralBase/AttributeSpecification.h"
#include "CoralBase/AttributeListSpecification.h"

#include <iostream>
#include <stdio.h>
#include <exception>
#include <map>

// Application Specific Include Files
// ----------------------------------
#include "globals.h"
#include "PvssArchiveNumber.h"
#include "PvssArchiveUtilities.h"
#include "CondDBUtilities.h"
#include "PvssArchiveFolderConfig.h"
#include "PvssCoolDefaultConnections.h"

//-----------------------------------------------------------------------------
PvssArchiveFolderConfig::PvssArchiveFolderConfig()
: m_session(0)
{
  this->m_folderConfiguration.clear();
  this->m_folderFieldConfiguration.clear();
  return;
}

PvssArchiveFolderConfig::~PvssArchiveFolderConfig()
{
  return;
}

bool PvssArchiveFolderConfig::connect(coral::IConnectionService& connectionService,
                                      std::string serviceName,
                                      bool useOwner /* = false */)
{
  bool status = true;

  try {

    if (useOwner)
      this->m_session = connectionService.connect(serviceName, FW_CONDDB_UPDATER, coral::Update);
    else
      this->m_session = connectionService.connect(serviceName, FW_CONDDB_WRITER, coral::Update);
  }

  //==================
  // CATCH EXCEPTIONS
  //==================
  catch ( coral::Exception& e )
  {
    std::cerr << "'PvssArchiveFolderConfig::connect()' CORAL exception : " << e.what() << std::endl;
    status = false;
  }
  catch ( std::exception& e )
  {
    std::cerr << "'PvssArchiveFolderConfig::connect()' Standard C++ exception : " << e.what() << std::endl;
    status = false;
  }
  catch( ... )
  {
    std::cerr << "'PvssArchiveFolderConfig::connect()' Unknown exception ..." << std::endl;
    status = false;
  }

  return (status);
}

void PvssArchiveFolderConfig::disconnect()
{
  if (this->m_session)
    delete this->m_session;

  return;
}

void PvssArchiveFolderConfig::changeStatus(std::string PVSS_COOL_FOLDER_CONFIGURATION_TABLE,
										   long folderId,
                                           long newStatus)
{

  try {

    this->m_session->transaction().start();
    coral::ISchema& mySchema = this->m_session->nominalSchema();
    coral::ITableDataEditor& myEditor = mySchema.tableHandle(PVSS_COOL_FOLDER_CONFIGURATION_TABLE).dataEditor();

    coral::AttributeList bindList;
    bindList.extend("status", "long");
    bindList["status"].setValue(newStatus);
    bindList.extend("statusReq", "long");
    bindList["statusReq"].setValue((long)FW_CONDDB_REQFOLDERSTAT_NOCHANGE);
    bindList.extend("folderId", "long");
    bindList["folderId"].setValue(folderId);

    myEditor.updateRows(std::string("STATUS = :status, STATUSREQ = :statusReq"), "FOLDERID = :folderId", bindList);

    this->m_session->transaction().commit();

    // Update local information if
    std::vector<FolderConfigurationInformation>::iterator iIter = std::find(this->m_folderConfiguration.begin(),
                                                                            this->m_folderConfiguration.end(),
                                                                            folderId);
    if (iIter != this->m_folderConfiguration.end()) {
      iIter->statusReq((long)FW_CONDDB_REQFOLDERSTAT_NOCHANGE);
      iIter->status(newStatus);
    }
  }

  //==================
  // CATCH EXCEPTIONS
  //==================
  catch ( coral::Exception& e )
  {
    std::cerr << "'PvssArchiveFolderConfig::changeStatus()' CORAL exception : " << e.what() << std::endl;
    this->m_session->transaction().rollback();
  }
  catch ( std::exception& e )
  {
    std::cerr << "'PvssArchiveFolderConfig::changeStatus()' Standard C++ exception : " << e.what() << std::endl;
    this->m_session->transaction().rollback();
  }
  catch( ... )
  {
    std::cerr << "'PvssArchiveFolderConfig::changeStatus()' Unknown exception ..." << std::endl;
    this->m_session->transaction().rollback();
  }

  return;
}

void PvssArchiveFolderConfig::updateFields(long folderId)
{

  try {

    this->m_session->transaction().start();
    coral::ISchema& mySchema = this->m_session->nominalSchema();
    coral::ITableDataEditor& myEditor = mySchema.tableHandle(PVSS_COOL_FOLDER_FIELD_CONFIGURATION_TABLE).dataEditor();

    coral::AttributeList bindList;
    bindList.extend("status", "long");
    bindList["status"].setValue((long)FW_CONDDB_STATUS_NOCHANGE);
    bindList.extend("folderId", "long");
    bindList["folderId"].setValue(folderId);
    myEditor.updateRows(std::string("STATUS = :status, FIELDNAME = NEWFIELDNAME"), "FOLDERID = :folderId", bindList);

    coral::AttributeList bindList2;
    bindList2.extend("folderId", "long");
    bindList2["folderId"].setValue(folderId);
    myEditor.updateRows(std::string("NEWFIELDNAME = ''"), "FOLDERID = :folderId", bindList2);

    this->m_session->transaction().commit();
  }

  //==================
  // CATCH EXCEPTIONS
  //==================
  catch ( coral::Exception& e )
  {
    std::cerr << "'PvssArchiveFolderConfig::updateFields()' CORAL exception : " << e.what() << std::endl;
  }
  catch ( std::exception& e )
  {
    std::cerr << "'PvssArchiveFolderConfig::updateFields()' Standard C++ exception : " << e.what() << std::endl;
  }
  catch( ... )
  {
    std::cerr << "'PvssArchiveFolderConfig::updateFields()' Unknown exception ..." << std::endl;
  }

  return;
}

void PvssArchiveFolderConfig::updateLists(long folderId)
{

  try {

    // JRC CHANGE need to add channel names at this point?
    this->m_session->transaction().start();
    coral::ISchema& mySchema = this->m_session->nominalSchema();
    coral::ITableDataEditor& myEditor = mySchema.tableHandle(PVSS_COOL_FOLDER_LIST_CONFIGURATION_TABLE).dataEditor();

    coral::AttributeList bindList;
    bindList.extend("status", "long");
    bindList["status"].setValue((long)FW_CONDDB_STATUS_NOCHANGE);
    bindList.extend("folderId", "long");
    bindList["folderId"].setValue(folderId);
    myEditor.updateRows(std::string("STATUS = :status, DPNAME = NEWDPNAME"), "FOLDERID = :folderId", bindList);

    coral::AttributeList bindList2;
    bindList2.extend("folderId", "long");
    bindList2["folderId"].setValue(folderId);
    myEditor.updateRows(std::string("NEWDPNAME = ''"), "FOLDERID = :folderId", bindList2);

    this->m_session->transaction().commit();
  }

  //==================
  // CATCH EXCEPTIONS
  //==================
  catch ( coral::Exception& e )
  {
    std::cerr << "'PvssArchiveFolderConfig::updateLists()' CORAL exception : " << e.what() << std::endl;
  }
  catch ( std::exception& e )
  {
    std::cerr << "'PvssArchiveFolderConfig::updateLists()' Standard C++ exception : " << e.what() << std::endl;
  }
  catch( ... )
  {
    std::cerr << "'PvssArchiveFolderConfig::updateLists()' Unknown exception ..." << std::endl;
  }

  return;
}

void PvssArchiveFolderConfig::markAsRemoved(std::string PVSS_COOL_FOLDER_CONFIGURATION_TABLE,
										    std::vector<FolderConfigurationInformation>& toBeRemoved,
                                            bool remove /* = true */)
{
  bool firstTime = true;

  int i = 1;

  char cpBind[50];

  std::string whereClause;

  if (toBeRemoved.empty())
    return;

  try {

    this->m_session->transaction().start();
    coral::ISchema& mySchema = this->m_session->nominalSchema();
    coral::ITableDataEditor& myEditor = mySchema.tableHandle(PVSS_COOL_FOLDER_CONFIGURATION_TABLE).dataEditor();

    coral::AttributeList bindList;
    bindList.extend("status", "long");
    if (remove)
      bindList["status"].setValue((long)FW_CONDDB_FOLDERSTAT_SUSPENDED);
    else
      bindList["status"].setValue((long)FW_CONDDB_FOLDERSTAT_EXISTS);
    bindList.extend("statusReq", "long");
    bindList["statusReq"].setValue((long)FW_CONDDB_STATUS_NOCHANGE);
    for (std::vector<FolderConfigurationInformation>::iterator iIter = toBeRemoved.begin(); iIter != toBeRemoved.end(); iIter++) {
      if (firstTime)
        firstTime = false;
      else
        whereClause += std::string(" OR ");
      sprintf(cpBind, "fId%02d", i);
      bindList.extend(cpBind, "long");
      bindList[cpBind].setValue(iIter->folderId());
      whereClause += std::string("FOLDERID = :") + cpBind;
      i++;
    }

    myEditor.updateRows(std::string("STATUS = :status, STATUSREQ = :statusReq"), whereClause, bindList);

    this->m_session->transaction().commit();
  }

  //==================
  // CATCH EXCEPTIONS
  //==================
  catch ( coral::Exception& e )
  {
    std::cerr << "'PvssArchiveFolderConfig::markAsRemoved()' CORAL exception : " << e.what() << std::endl;
  }
  catch ( std::exception& e )
  {
    std::cerr << "'PvssArchiveFolderConfig::markAsRemoved()' Standard C++ exception : " << e.what() << std::endl;
  }
  catch( ... )
  {
    std::cerr << "'PvssArchiveFolderConfig::markAsRemoved()' Unknown exception ..." << std::endl;
  }

  return;
}

bool PvssArchiveFolderConfig::updateLastUpdateTime(std::string PVSS_COOL_FOLDER_CONFIGURATION_TABLE,
                                                   long folderId,
                                                   std::string updateTime,
                                                   bool before /* = true */)
{
  bool status = false;

  std::string updateString;

  try {

    // Get handle to table
    this->m_session->transaction().start();
    coral::ISchema& mySchema = this->m_session->nominalSchema();
    coral::ITableDataEditor& myEditor = mySchema.tableHandle(PVSS_COOL_FOLDER_CONFIGURATION_TABLE).dataEditor();

    coral::AttributeList bindList;
    bindList.extend("updateTime", "string");
    bindList.extend("folderId", "long");
    if (before) {
      bindList["updateTime"].setValue(updateTime);
      updateString = "LASTUPDATEBEFORE = :updateTime";
    } else {
      bindList["updateTime"].setValue(updateTime);
      updateString = "LASTUPDATEAFTER = :updateTime";
    }
    bindList["folderId"].setValue(folderId);
    if (myEditor.updateRows(updateString, std::string("FOLDERID = :folderId"), bindList) == 1) {
      std::vector<FolderConfigurationInformation>::iterator iIter = std::find(this->m_folderConfiguration.begin(),
                                                                              this->m_folderConfiguration.end(),
                                                                              folderId);
      if (iIter != this->m_folderConfiguration.end()) {
        if (before)
          iIter->lastUpdateBefore(updateTime);
        else
          iIter->lastUpdateAfter(updateTime);
      }
      status = true;
    }
    this->m_session->transaction().commit();
  }

  //==================
  // CATCH EXCEPTIONS
  //==================
  catch ( coral::Exception& e )
  {
    std::cerr << "'PvssArchiveFolderConfig::updateLastUpdateTime()' CORAL exception : " << e.what() << std::endl;
    status = false;
  }
  catch ( std::exception& e )
  {
    std::cerr << "'PvssArchiveFolderConfig::updateLastUpdateTime()' Standard C++ exception : " << e.what() << std::endl;
    status = false;
  }
  catch( ... )
  {
    std::cerr << "'PvssArchiveFolderConfig::updateLastUpdateTime()' Unknown exception ..." << std::endl;
    status = false;
  }

  return (status);
}

void PvssArchiveFolderConfig::decrementIntervalCounters(std::string PVSS_COOL_FOLDER_CONFIGURATION_TABLE)
{
  try {

    // Get handle to table
    this->m_session->transaction().start();
    coral::ISchema& mySchema = this->m_session->nominalSchema();
    coral::ITableDataEditor& myEditor = mySchema.tableHandle(PVSS_COOL_FOLDER_CONFIGURATION_TABLE).dataEditor();
    myEditor.updateRows(std::string("INTERVALCOUNT = INTERVALCOUNT - 1"), std::string("INTERVALCOUNT > 0"), coral::AttributeList());
    this->m_session->transaction().commit();
  }

  //==================
  // CATCH EXCEPTIONS
  //==================
  catch ( coral::Exception& e )
  {
    std::cerr << "'PvssArchiveFolderConfig::decrementIntervalCounters()' CORAL exception : " << e.what() << std::endl;
  }
  catch ( std::exception& e )
  {
    std::cerr << "'PvssArchiveFolderConfig::decrementIntervalCounters()' Standard C++ exception : " << e.what() << std::endl;
  }
  catch( ... )
  {
    std::cerr << "'PvssArchiveFolderConfig::decrementIntervalCounters()' Unknown exception ..." << std::endl;
  }

  return;
}


void PvssArchiveFolderConfig::resetIntervalCounters(std::string PVSS_COOL_FOLDER_CONFIGURATION_TABLE, bool forceReset /* = false */)
{
  try {

    this->m_session->transaction().start();
    coral::ISchema& mySchema = this->m_session->nominalSchema();
    coral::ITableDataEditor& myEditor = mySchema.tableHandle(PVSS_COOL_FOLDER_CONFIGURATION_TABLE).dataEditor();

    if (forceReset)
      myEditor.updateRows(std::string("INTERVALCOUNT = UPDATEINTERVAL"), std::string(""), coral::AttributeList());
    else
      myEditor.updateRows(std::string("INTERVALCOUNT = UPDATEINTERVAL"), std::string("INTERVALCOUNT = 0"), coral::AttributeList());

    this->m_session->transaction().commit();
  }

  //==================
  // CATCH EXCEPTIONS
  //==================
  catch ( coral::Exception& e )
  {
    std::cerr << "'PvssArchiveFolderConfig::resetIntervalCounters()' CORAL exception : " << e.what() << std::endl;
  }
  catch ( std::exception& e )
  {
    std::cerr << "'PvssArchiveFolderConfig::resetIntervalCounters()' Standard C++ exception : " << e.what() << std::endl;
  }
  catch( ... )
  {
    std::cerr << "'PvssArchiveFolderConfig::resetIntervalCounters()' Unknown exception ..." << std::endl;
  }

  return;
}

bool PvssArchiveFolderConfig::updateFolderTable(std::string PVSS_COOL_FOLDER_CONFIGURATION_TABLE,
                                                long folderId,
                                                bool useList,
//                                                long interval,
                                                long channelIdType,
                                                std::string channelIdInfo,
                                                std::string dpPattern)
{
  bool status = false;

  try {
    // Get handle to table
    this->m_session->transaction().start();
    coral::ISchema& mySchema = this->m_session->nominalSchema();
    coral::ITableDataEditor& myEditor = mySchema.tableHandle(PVSS_COOL_FOLDER_CONFIGURATION_TABLE).dataEditor();

    coral::AttributeList bindList;
    bindList.extend("useList", "bool");
    bindList.extend("interval", "long");
    bindList.extend("channelIdType", "long");
    bindList.extend("channelIdInfo", "string");
    bindList.extend("dpPattern", "string");
    bindList.extend("folderId", "long");
    bindList["useList"].setValue(useList);
//    bindList["interval"].setValue(interval);
    bindList["channelIdType"].setValue(channelIdType);
    bindList["channelIdInfo"].setValue(channelIdInfo);
    bindList["dpPattern"].setValue(dpPattern);
    bindList["folderId"].setValue(folderId);
// Commented/replaced 30.11.12 by SK for removing 'interval'
//    if (myEditor.updateRows(std::string("USELIST = :useList, UPDATEINTERVAL = :interval, CHANNELIDTYPE = :channelIdType, CHANNELIDINFO = :channelIdInfo, DPPATTERN = :dpPattern"), std::string("FOLDERID = :folderId"), bindList) == 1)
    if (myEditor.updateRows(std::string("USELIST = :useList, CHANNELIDTYPE = :channelIdType, CHANNELIDINFO = :channelIdInfo, DPPATTERN = :dpPattern"), std::string("FOLDERID = :folderId"), bindList) == 1)
      status = true;
    this->m_session->transaction().commit();
  }

  //==================
  // CATCH EXCEPTIONS
  //==================
  catch ( coral::Exception& e )
  {
    std::cerr << "'PvssArchiveFolderConfig::updateFolderTable()' CORAL exception : " << e.what() << std::endl;
    status = false;
  }
  catch ( std::exception& e )
  {
    std::cerr << "'PvssArchiveFolderConfig::updateFolderTable()' Standard C++ exception : " << e.what() << std::endl;
    status = false;
  }
  catch( ... )
  {
    std::cerr << "'PvssArchiveFolderConfig::updateFolderTable()' Unknown exception ..." << std::endl;
    status = false;
  }

  return (status);
}

float PvssArchiveFolderConfig::readSchemaVersion()
{

  try {

    this->m_session->transaction().start(true);
    coral::ISchema& mySchema = this->m_session->nominalSchema();

    // Create the query
    coral::IQuery* myQuery = mySchema.newQuery();

    // Add the 'SELECT' part
    coral::AttributeList outputList;
    myQuery->addToOutputList("DB_ATTRIBUTE_VALUE");
    outputList.extend("DB_ATTRIBUTE_VALUE", "string");

    // Add the 'FROM' part
    myQuery->addToTableList(PVSS_COOL_ATTRIBUTES_TABLE);

    // Set condition(s)
    coral::AttributeList bindList;
    bindList.extend("dbAttributeName", "string");
    bindList["dbAttributeName"].setValue(std::string("SCHEMA_VERSION"));

    myQuery->setCondition("DB_ATTRIBUTE_NAME = :dbAttributeName", bindList);
    myQuery->defineOutput(outputList);
    myQuery->setRowCacheSize(10);

    coral::ICursor& myCursor = myQuery->execute();

    while (myCursor.next()) {
      const coral::AttributeList& row = myCursor.currentRow();
      std::string temp;
      temp = row["DB_ATTRIBUTE_VALUE"].data<std::string>();
      this->m_schemaVersion = atof(temp.data());
    }
    delete myQuery;
    this->m_session->transaction().commit();
  }

  //==================
  // CATCH EXCEPTIONS
  //==================
  catch ( coral::Exception& e )
  {
    std::cerr << "'PvssArchiveFolderConfig::readSchemaVersion()' CORAL exception : " << e.what() << std::endl;
  }
  catch ( std::exception& e )
  {
    std::cerr << "'PvssArchiveFolderConfig::readSchemaVersion()' Standard C++ exception : " << e.what() << std::endl;
  }
  catch( ... )
  {
    std::cerr << "'PvssArchiveFolderConfig::readSchemaVersion()' Unknown exception ..." << std::endl;
  }

  return (this->m_schemaVersion);
}

std::vector<FolderConfigurationInformation>& PvssArchiveFolderConfig::readFolderConfigurationTable(std::string PVSS_COOL_FOLDER_CONFIGURATION_TABLE,
                                                                                                   long folderGroup, /* = PVSS_COOL_ALL_FOLDERS */
                                                                                                   bool ignoreUpdateInterval, /* = false */
                                                                                                   long withState /* = FW_CONDDB_FOLDERSTAT_EXISTS */)
{
  bool useList = false;
  bool listChanged;

// All concerned these two variables and DB fields commented 30.11.12 by SK
//  long mechanism = 0;
//  long interval = 0;
  long channelIdType = FW_CONDDB_IDINFO_DPNAME;
  long dptInfo = FW_CONDDB_DPTINFO_SIMPLE;
  long statusReq;
  long status;

  long folderId = 0;
  
  std::string folderName;
  std::string folderDesc;
  std::string groupTable;
  std::string dpTypeName;
  std::string dpSubTypeName;
  std::string dpPattern;
  std::string channelIdInfo;
  std::string lastUpdateBefore;
  std::string lastUpdateAfter;
  std::string condition;
  std::string timePrecision;

  try {

    // First get CondDB schema version
    this->readSchemaVersion();

    // Clear any existing information first
    this->m_folderConfiguration.clear();

    this->m_session->transaction().start(true);
    coral::ISchema& mySchema = this->m_session->nominalSchema();

    // Create the query
    coral::IQuery* myQuery = mySchema.newQuery();

    // Add the 'SELECT' part
    coral::AttributeList outputList;
    myQuery->addToOutputList("FOLDERID");
    outputList.extend("FOLDERID", "long");
    myQuery->addToOutputList("FOLDERNAME");
    outputList.extend("FOLDERNAME", "string");
    myQuery->addToOutputList("FOLDERDESC");
    outputList.extend("FOLDERDESC", "string");
    myQuery->addToOutputList("FOLDERGROUP");
    outputList.extend("FOLDERGROUP", "long");
    myQuery->addToOutputList("GROUPTABLE");
    outputList.extend("GROUPTABLE", "string");
    myQuery->addToOutputList("DPTYPENAME");
    outputList.extend("DPTYPENAME", "string");
    myQuery->addToOutputList("DPSUBTYPENAME");
    outputList.extend("DPSUBTYPENAME", "string");
    myQuery->addToOutputList("DPTINFO");
    outputList.extend("DPTINFO", "long");
    myQuery->addToOutputList("USELIST");
    outputList.extend("USELIST", "bool");
    myQuery->addToOutputList("DPPATTERN");
    outputList.extend("DPPATTERN", "string");
    myQuery->addToOutputList("CHANNELIDTYPE");
    outputList.extend("CHANNELIDTYPE", "long");
    myQuery->addToOutputList("CHANNELIDINFO");
    outputList.extend("CHANNELIDINFO", "string");
    myQuery->addToOutputList("LASTUPDATEBEFORE");
    outputList.extend("LASTUPDATEBEFORE", "string");
    myQuery->addToOutputList("LASTUPDATEAFTER");
    outputList.extend("LASTUPDATEAFTER", "string");
//    myQuery->addToOutputList("MECHANISM");
//    outputList.extend("MECHANISM", "long");
//    myQuery->addToOutputList("UPDATEINTERVAL");
//    outputList.extend("UPDATEINTERVAL", "long");
//    myQuery->addToOutputList("INTERVALCOUNT");
//    outputList.extend("INTERVALCOUNT", "long");
    myQuery->addToOutputList("STATUSREQ");
    outputList.extend("STATUSREQ", "long");
    myQuery->addToOutputList("STATUS");
    outputList.extend("STATUS", "long");
    myQuery->addToOutputList("LISTCHANGE");
    outputList.extend("LISTCHANGE", "bool");
	myQuery->addToOutputList("TIMEPRECISION");
    outputList.extend("TIMEPRECISION", "string");

    // Add the 'FROM' part
    myQuery->addToTableList(PVSS_COOL_FOLDER_CONFIGURATION_TABLE);

    // Set condition(s)
    coral::AttributeList bindList;
    if (folderGroup != PVSS_COOL_ALL_FOLDERS) {
      bindList.extend("folderGroup", "long");
      bindList["folderGroup"].setValue(folderGroup);
      condition = std::string("FOLDERGROUP = :folderGroup");
    }
    if (!ignoreUpdateInterval) {
      if (condition.empty())
        condition = std::string("INTERVALCOUNT = 1");
      else
        condition += std::string(" AND INTERVALCOUNT = 1");
    }
    if (withState != FW_CONDDB_STATUS_ANY) {
      bindList.extend("withState", "long");
      bindList["withState"].setValue(withState);
      if (condition.empty())
        condition = std::string("STATUS = :withState");
      else
        condition += std::string(" AND STATUS = :withState");
    }
    myQuery->setCondition(condition, bindList);
    myQuery->defineOutput(outputList);
    myQuery->setRowCacheSize(100);

    coral::ICursor& myCursor = myQuery->execute();

    while (myCursor.next()) {
      const coral::AttributeList& row = myCursor.currentRow();
      folderId = row["FOLDERID"].data<long>();
      folderName = row["FOLDERNAME"].data<std::string>();
      folderDesc = row["FOLDERDESC"].data<std::string>();
      folderGroup = row["FOLDERGROUP"].data<long>();
      groupTable = row["GROUPTABLE"].data<std::string>();
      dpTypeName = row["DPTYPENAME"].data<std::string>();
      dpSubTypeName = row["DPSUBTYPENAME"].data<std::string>();
      dptInfo = row["DPTINFO"].data<long>();
      useList = row["USELIST"].data<bool>();
      dpPattern = row["DPPATTERN"].data<std::string>();
      channelIdType = row["CHANNELIDTYPE"].data<long>();
      channelIdInfo = row["CHANNELIDINFO"].data<std::string>();
      if (row["LASTUPDATEBEFORE"].isNull())
        lastUpdateBefore = "";
      else
        lastUpdateBefore = row["LASTUPDATEBEFORE"].data<std::string>();
      if (row["LASTUPDATEAFTER"].isNull())
        lastUpdateAfter = "";
      else
        lastUpdateAfter = row["LASTUPDATEAFTER"].data<std::string>();
//      mechanism = row["MECHANISM"].data<long>();
//      interval = row["UPDATEINTERVAL"].data<long>();
      statusReq = row["STATUSREQ"].data<long>();
      status = row["STATUS"].data<long>();
      listChanged = row["LISTCHANGE"].data<bool>();
	  timePrecision = row["TIMEPRECISION"].data<std::string>();

      FolderConfigurationInformation folderConfigInfo(useList, listChanged,
                                                      folderId, folderGroup,
//                                                      mechanism, interval, channelIdType,
                                                      channelIdType,
                                                      dptInfo, statusReq, status,
                                                      folderName, folderDesc, groupTable,
                                                      dpTypeName, dpSubTypeName, dpPattern, channelIdInfo,
                                                      lastUpdateBefore, lastUpdateAfter, timePrecision);
      this->m_folderConfiguration.push_back(folderConfigInfo);
    }
    delete myQuery;
    this->m_session->transaction().commit();
  }

  //==================
  // CATCH EXCEPTIONS
  //==================
  catch ( coral::Exception& e )
  {
    std::cerr << "'PvssArchiveFolderConfig::readFolderConfigurationTable()' CORAL exception : " << e.what() << std::endl;
  }
  catch ( std::exception& e )
  {
    std::cerr << "'PvssArchiveFolderConfig::readFolderConfigurationTable()' Standard C++ exception : " << e.what() << std::endl;
  }
  catch( ... )
  {
    std::cerr << "'PvssArchiveFolderConfig::readFolderConfigurationTable()' Unknown exception ..." << std::endl;
  }

  return (this->m_folderConfiguration);
}

std::vector<FolderFieldConfiguration>& PvssArchiveFolderConfig::readFolderFieldConfigurationTable(long folderId,
                                                                                                  long statusReq /* = FW_CONDDB_STATUS_ANY */)
{
  bool isIdElement = false;

  long myFolderId = 0;
  long position = 0;
  long status = 0;
  long timeout = 0;

  double deadband = 0.0;
  
  std::string dpElementName;
  std::string fieldName;
  std::string newFieldName;
  std::string coolFieldType;
  std::string pvssFieldType;
  std::string condition;
  // added 16.11.12 by SK
  std::vector<std::string> uniqueFieldNames;
  std::string realFieldName;

//	std::cerr << "readFolderFieldConfigurationTable() for folder " << folderId << std::endl;
  try {

    // Clear any existing information
    this->m_folderFieldConfiguration.clear();

    // Create the query
    this->m_session->transaction().start(true);
    coral::ISchema& mySchema = this->m_session->nominalSchema();
    coral::IQuery* myQuery = mySchema.newQuery();

    // Add the 'SELECT' part
    coral::AttributeList outputList;
    myQuery->addToOutputList("FOLDERID");
    outputList.extend("FOLDERID", "long");
    myQuery->addToOutputList("POSITION");
    outputList.extend("POSITION", "long");
    myQuery->addToOutputList("DPELEMENTNAME");
    outputList.extend("DPELEMENTNAME", "string");
    myQuery->addToOutputList("FIELDNAME");
    outputList.extend("FIELDNAME", "string");
    myQuery->addToOutputList("NEWFIELDNAME");
    outputList.extend("NEWFIELDNAME", "string");
    myQuery->addToOutputList("COOLFIELDTYPE");
    outputList.extend("COOLFIELDTYPE", "string");
    myQuery->addToOutputList("PVSSFIELDTYPE");
    outputList.extend("PVSSFIELDTYPE", "string");
    myQuery->addToOutputList("ISID");
    outputList.extend("ISID", "bool");
    myQuery->addToOutputList("STATUS");
    outputList.extend("STATUS", "long");
    if (this->schemaVersion() > 3.0) {
      myQuery->addToOutputList("DEADBAND");
      outputList.extend("DEADBAND", "double");
      myQuery->addToOutputList("TIMEOUT");
      outputList.extend("TIMEOUT", "long");
    }
	// Two lines added 09.11.12 by SK
    myQuery->addToOutputList("CHNAME");
    outputList.extend("CHNAME", "string");

    // Add the 'FROM' part
    myQuery->addToTableList(PVSS_COOL_FOLDER_FIELD_CONFIGURATION_TABLE);

    // Add 'WHERE' clause
    coral::AttributeList bindList;
    bindList.extend("folderId", "long");
    bindList["folderId"].setValue(folderId);
    condition = std::string("FOLDERID = :folderId");
    if (statusReq != FW_CONDDB_STATUS_ANY) {
      bindList.extend("statusReq", "long");
      bindList["statusReq"].setValue(statusReq);
      condition += std::string(" AND STATUS = :statusReq");
    }
    myQuery->setCondition(condition, bindList);
// Added here two lines 09.11.12 by SK
	myQuery->addToOrderList("CHNAME");
	myQuery->addToOrderList("POSITION");
	
    myQuery->defineOutput(outputList);
    myQuery->setRowCacheSize(100);

    coral::ICursor& myCursor = myQuery->execute();

    while (myCursor.next()) {
//	std::cerr << "Something found" << std::endl;
      const coral::AttributeList& row = myCursor.currentRow();
	// Introduced 16.11.12 by SK
	 // We suppose that exactly single of FIELDNAME and NEWFIELDNAME is not empty
	 realFieldName = row["NEWFIELDNAME"].data<std::string>();
	 realFieldName += row["FIELDNAME"].data<std::string>();
	 std::vector<std::string>::const_iterator alreadyFound = std::find(uniqueFieldNames.begin(), uniqueFieldNames.end(), realFieldName);
	 if(alreadyFound == uniqueFieldNames.end())	{	// This field name not yet stored
	     uniqueFieldNames.push_back(realFieldName);
    // End of new code (still '}' see below)
	
         myFolderId = row["FOLDERID"].data<long>();
         position = row["POSITION"].data<long>();
         dpElementName = row["DPELEMENTNAME"].data<std::string>();
         fieldName = row["FIELDNAME"].data<std::string>();
         newFieldName = row["NEWFIELDNAME"].data<std::string>();
         coolFieldType = row["COOLFIELDTYPE"].data<std::string>();
         pvssFieldType = row["PVSSFIELDTYPE"].data<std::string>();
         isIdElement = row["ISID"].data<bool>();
         status = row["STATUS"].data<long>();
         if (this->schemaVersion() > 3.0) {
           if (row["DEADBAND"].isNull())
             deadband = 0.0;
           else
             deadband = row["DEADBAND"].data<double>();
           if (row["TIMEOUT"].isNull())
             timeout = 0;
           else
             timeout = row["TIMEOUT"].data<long>();
         } 
		 else {
           deadband = 0.0;
           timeout = 0;
         }
	  // Single line Added 09.11.12 by SK (needed for collection folder introducing)
	  // Commented 16.11.12 by sk because seems not to be necessary
//	  channelName = row["CHNAME"].data<std::string>();
      // Really, we never use that CHNAME value within the C++ code
	  // THOUGH, the symbol 'CHNAME' and its value is used within the 
	  // query for extracting archive data 
	  
//      std::cout << (this->schemaVersion() > 3.0 ? "Use smoothing" : "No smoothing") << ", Deadband = " << deadband << ", timeout = " << timeout << std::endl;
//      std::cout << myFolderId << ", " << position << ", " << dpElementName << ", " << fieldName << ", " << coolFieldType << ", " << pvssFieldType << ", " << isIdElement << std::endl;
         FolderFieldConfiguration folderFieldConfig(isIdElement, myFolderId, position, status, timeout,
                                                    deadband, dpElementName, fieldName, newFieldName,
                                                    coolFieldType, pvssFieldType);
         this->m_folderFieldConfiguration.push_back(folderFieldConfig);
	 }
    }

    delete myQuery;
    this->m_session->transaction().commit();
  }

  //==================
  // CATCH EXCEPTIONS
  //==================
  catch ( coral::Exception& e )
  {
    std::cerr << "'PvssArchiveFolderConfig::readFolderFieldConfigurationTable()' - CORAL exception : " << e.what() << std::endl;
  }
  catch ( std::exception& e )
  {
    std::cerr << "'PvssArchiveFolderConfig::readFolderFieldConfigurationTable()' - Standard C++ exception : " << e.what() << std::endl;
  }
  catch( ... )
  {
    std::cerr << "'PvssArchiveFolderConfig::readFolderFieldConfigurationTable()' - Unknown exception ..." << std::endl;
  }

  return (this->m_folderFieldConfiguration);
}
