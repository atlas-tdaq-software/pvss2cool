// Include files
#include <iostream>
#include "CoolKernel/Exception.h"
#include "CoolKernel/IDatabaseSvc.h"
#include "CoolKernel/IDatabase.h"
#include "CoolKernel/IFolder.h"
#include "CoolKernel/FolderSpecification.h"
#include "CoolKernel/FolderVersioning.h"
#include "CoolKernel/types.h"
#include "CoolApplication/Application.h"
#include "RelationalAccess/IAuthenticationService.h"
#include "RelationalAccess/IAuthenticationCredentials.h"
#include "RelationalAccess/IRelationalService.h"
#include "CoralBase/Attribute.h"
#include "CoralBase/AttributeList.h"
#include "CoralBase/AttributeListException.h"
//#include "CoolKernel/ExtendedAttributeListSpecification.h"

// Application Specific Include Files
// ----------------------------------
#include "globals.h"
#include "PvssCoolDefaultConnections.h"
#include "PvssCondDbFolderConfig.h"

// Message output
#define LOG std::cout << "__PvssCondDbFolderConfig "

//-----------------------------------------------------------------------------

PvssCondDbFolderConfig::PvssCondDbFolderConfig()
{
  return;
}

PvssCondDbFolderConfig::~PvssCondDbFolderConfig()
{
  return;
}

bool PvssCondDbFolderConfig::connect(cool::IDatabaseSvc &coolService,
                                     std::string serviceName,
                                     std::string dbName,
                                     bool readOnly /* = true */)
{
  bool status = true;

  std::string connectionString = serviceName + std::string("(") + FW_CONDDB_UPDATER + std::string (")/") + dbName;

  try {

    this->m_connection = coolService.openDatabase(connectionString, readOnly);

  }

  //==================
  // CATCH EXCEPTIONS
  //==================
  catch( cool::Exception& e ) 
  {
    LOG << "'connect()' COOL exception: '" << e.what() << "'" << std::endl;
    status = false;
  }
  catch( std::exception& e )
  {
    LOG << "'connect()' Standard C++ exception: '" << e.what() << "'" << std::endl;
    status = false;
  }
  catch( ... ) 
  {
    LOG << "'connect()' Unknown exception caught" << std::endl;
    status = false;
  }

  return (status);
}

bool PvssCondDbFolderConfig::disconnect()
{
  cool::IDatabasePtr dbNull;
  this->m_connection = dbNull;
  return (true);
}

bool PvssCondDbFolderConfig::getFolders(std::vector<std::string> &folders)
{
  bool status = true;

  try {

    folders = this->m_connection->listAllNodes();
    /*
  for (std::vector<std::string>::iterator iIter = folders.begin(); iIter != folders.end(); iIter++)
    std::cout << (*iIter) << std::endl;
    */

  }

  //==================
  // CATCH EXCEPTIONS
  //==================
  catch( cool::Exception& e ) 
  {
    LOG << "'getFolders()' COOL exception: '" << e.what() << "'" << std::endl;
    status = false;
  }
  catch( std::exception& e )
  {
    LOG << "'getFolders()' Standard C++ exception: '" << e.what() << "'" << std::endl;
    status = false;
  }
  catch( ... ) 
  {
    LOG << "'getFolders()' Unknown exception caught" << std::endl;
    status = false;
  }

  return (status);
}

bool PvssCondDbFolderConfig::existsFolderAndIsDifferent(FolderConfigurationInformation folderInfo,
                                                        std::vector<FolderFieldConfiguration>& folderFields,
                                                        std::vector<std::string>& removed,
                                                        std::vector<std::string>& valid,
                                                        std::vector<std::string>& differentTypes,
                                                        std::vector<std::string>& newField)
{
  bool exists = false;
  cool::UInt32 index;
  std::string fieldName;

  try {

    // Ensure all vectors start empty
    removed.clear();
    valid.clear();
    differentTypes.clear();
    newField.clear();

    // First check if folder already exists
    if (this->m_connection->existsFolder(folderInfo.folderName())) {

      exists = true;

      // Create attribute list specification for given information, adding DP name
      // as one field (until string channel IDs are allowed)
      cool::RecordSpecification payload;

      // Loop through all user fields
      for (std::vector<FolderFieldConfiguration>::iterator iIter = folderFields.begin(); iIter != folderFields.end(); iIter++) {
        // JRC CHANGE need to handle all possible status values
        if (iIter->status() == FW_CONDDB_STATUS_NEW)
          fieldName = iIter->newFieldName();
        else
          fieldName = iIter->fieldName();
        if (!iIter->isId()) { // JRC CHANGE must be a better way to do this!
          if (iIter->coolFieldType().compare("bool") == 0)
            payload.extend(fieldName, cool::StorageType::Bool);
          else if (iIter->coolFieldType().compare("int") == 0)
            payload.extend(fieldName, cool::StorageType::Int32);
          else if ((iIter->coolFieldType().compare("unsigned") == 0) ||
                   (iIter->coolFieldType().compare("unsigned int") == 0))
            payload.extend(fieldName, cool::StorageType::UInt32);
          else if (iIter->coolFieldType().compare("float") == 0)
            payload.extend(fieldName, cool::StorageType::Float);
          else if (iIter->coolFieldType().compare("string") == 0)
            payload.extend(fieldName, cool::StorageType::String4k);
        }
      }

      // Get attribute list specification of existing folder
      cool::IFolderPtr folderPtr = this->m_connection->getFolder(folderInfo.folderName());
      const cool::RecordSpecification& existingPayload = folderPtr->payloadSpecification();

      // Loop through existing payload. Need to know which fields are still valid (same name and type), which have changed
      // (same name but different type), which have been removed (name exists in existingPayload only) and those which are
      // new (name exists only in CondDB).

      for (index = 0; index < existingPayload.size(); index++) {

        try {
          if (existingPayload[index] == payload[existingPayload[index].name()]) {
            // Field is still valid
            valid.push_back(existingPayload[index].name());
          } else {
            // Field type has changed
            differentTypes.push_back(existingPayload[index].name());
          }
        }

        catch ( coral::AttributeListException& e ) {
          // This is almost certainly because the name exists in the existingPayload only
          removed.push_back(existingPayload[index].name());
        }
      }

      // Now find any new fields
      for (index = 0; index < existingPayload.size(); index++) {
        try {
          // Just need to see if field name exists or not, so attempt to get the name (and catch exception)
		      existingPayload[existingPayload[index].name()].name();
        }

        catch ( coral::AttributeListException& e ) {
          // This is almost certainly because the name exists in the new payload only
          newField.push_back(existingPayload[index].name());
        }
      }
    }
  }

  //==================
  // CATCH EXCEPTIONS
  //==================
  catch( cool::Exception& e ) 
  {
    LOG << "'existsFolderAndIsDifferent()' COOL exception: '" << e.what() << "'" << std::endl;
  }
  catch( std::exception& e )
  {
    LOG << "'existsFolderAndIsDifferent()' Standard C++ exception: '" << e.what() << "'" << std::endl;
  }
  catch( ... ) 
  {
    LOG << "'existsFolderAndIsDifferent()' Unknown exception caught" << std::endl;
  }

  return (exists);
}

bool PvssCondDbFolderConfig::dropFolder(std::string folderName)
{
  bool status;

  try {

    status = this->m_connection->dropNode(folderName);
  }

  //==================
  // CATCH EXCEPTIONS
  //==================
  catch( cool::Exception& e ) 
  {
    LOG << "'dropFolder()' COOL exception: '" << e.what() << "'" << std::endl;
    status = false;
  }
  catch( std::exception& e )
  {
    LOG << "'dropFolder()' Standard C++ exception: '" << e.what() << "'" << std::endl;
    status = false;
  }
  catch( ... ) 
  {
    LOG << "'dropFolder()' Unknown exception caught" << std::endl;
    status = false;
  }

  return (status);
}

bool PvssCondDbFolderConfig::createFolder(std::string folderName,
                                          std::string folderDescription,
                                          std::vector<FolderFieldConfiguration>& folderFields)
{
  bool status = false;

  try {

    cool::RecordSpecification payloadSpec;  // JRC CHANGE should be IFolderSpecification

    if (folderFields.size()  > 0) {

      // Loop through all user fields
      for (std::vector<FolderFieldConfiguration>::iterator iIter = folderFields.begin(); iIter != folderFields.end(); iIter++) {
        if (!iIter->isId()) { // JRC CHANGE must be a better way to do this!
          if (iIter->coolFieldType().compare("bool") == 0)
            payloadSpec.extend(iIter->newFieldName(), cool::StorageType::Bool);
          else if (iIter->coolFieldType().compare("int") == 0)
            payloadSpec.extend(iIter->newFieldName(), cool::StorageType::Int32);
          else if ((iIter->coolFieldType().compare("unsigned") == 0) ||
                   (iIter->coolFieldType().compare("unsigned int") == 0))
            payloadSpec.extend(iIter->newFieldName(), cool::StorageType::UInt32);
          else if (iIter->coolFieldType().compare("float") == 0)
            payloadSpec.extend(iIter->newFieldName(), cool::StorageType::Float);
          else if (iIter->coolFieldType().compare("string") == 0)
            payloadSpec.extend(iIter->newFieldName(), cool::StorageType::String4k);
        }
      }

	  cool::FolderSpecification fSpec(payloadSpec);
      this->m_connection->createFolder(folderName,
//                                       payloadSpec,
                                       fSpec,
									   folderDescription,
// 29.09.14                                      cool::FolderVersioning::SINGLE_VERSION,
                                       true);
      status = true;
    } else {
      std::cout << "No fields for folder '" << folderName << "'" << std::endl;
    }
  }

  //==================
  // CATCH EXCEPTIONS
  //==================
  catch( cool::Exception& e ) 
  {
    LOG << "'createFolder()' COOL exception: '" << e.what() << "'" << std::endl;
  }
  catch( std::exception& e )
  {
    LOG << "'createFolder()' Standard C++ exception: '" << e.what() << "'" << std::endl;
  }
  catch( ... ) 
  {
    LOG << "'createFolder()' Unknown exception caught" << std::endl;
  }

  return (status);
}
