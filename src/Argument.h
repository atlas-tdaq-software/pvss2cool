#ifndef __ARGUMENT_H__	// Gate-keeper code
#define __ARGUMENT_H__
// ----------------------------------------------------------------
// File:				  Argument.h
//
// Description:	  Include file for Argument class definition
//
// Author:			  J.R.Cook
// Creation Date:	02/03/2001
// ----------------------------------------------------------------
// Revision History
// ================
//  Version | Inits |    Date    |           Comment
// ---------|-------|------------|---------------------------------
//     A    |  JRC  | 02/03/2001 | Created
//     B    |  JRC  | 01/08/2006 | Updated to use std::string instead
//          |       |            |   of char

// Include Files
// -------------
#include <string>

#define PVSSCOOL_ARG_CHAR
typedef char ARG_TYPE;

// Argument class
// --------------
class Argument {

public:
	enum FlagStatus {unknown, found, stored};

  Argument();
#ifdef PVSSCOOL_ARG_CHAR
  Argument(ARG_TYPE flagID, std::string newArgument = std::string(""));
#else
  Argument(std::string flagID, std::string newArgument = std::string(""));
#endif
	~Argument();

  Argument(const Argument &old);
  Argument& operator=(const Argument &rhs);
#ifdef PVSSCOOL_ARG_CHAR
  bool operator==(const ARG_TYPE myFlagID);
#else
  bool operator==(const std::string &myFlagID);
#endif

#ifdef PVSSCOOL_ARG_CHAR
  inline void flag(ARG_TYPE flagID) {m_flagID = flagID;}
#else
  inline void flag(std::string flagID) {m_flagID = flagID;}
#endif
  void argument(std::string newArgument);

#ifdef PVSSCOOL_ARG_CHAR
  inline ARG_TYPE flag() {return (m_flagID);}
#else
  inline std::string flag() {return (m_flagID);}
#endif
  inline std::string argument() {return (m_argument);}
  inline FlagStatus status() {return (m_status);}

private:
#ifdef PVSSCOOL_ARG_CHAR
	ARG_TYPE m_flagID;
#else
	std::string m_flagID;
#endif
	std::string m_argument;

	FlagStatus m_status;
};

#endif // End of Gate-keeper code '__ARGUMENT_H__'
