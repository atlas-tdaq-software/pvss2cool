#ifndef __PVSS_ARCHIVE_FOLDER_CONFIG_DPE_NAMES_H__ // Get keeper code
#define __PVSS_ARCHIVE_FOLDER_CONFIG_DPE_NAMES_H__

// File containing definitions of constants for the datapoint element
// names used for the configuration of the conditions database folders.
// These names are used to obtain the datapoint element IDs in the database.

#define PVSS_CONDDB_DPT_NAME "FwCondDBConfig"

#define PVSS_CONDDB_DPE_FOLDER "folder"
#define PVSS_CONDDB_DPE_DESCRIPTION "folderDescription"
#define PVSS_CONDDB_DPE_DPT "dpt"
#define PVSS_CONDDB_DPE_SUBDPT "subDpt"
#define PVSS_CONDDB_DPE_DPPATTERN "dpPattern"
#define PVSS_CONDDB_DPE_DPTINFO "dpTypeInfo"
#define PVSS_CONDDB_DPE_DPELEMENTS "dpElements"
#define PVSS_CONDDB_DPE_FIELDNAMES "fieldNames"
#define PVSS_CONDDB_DPE_FIELDTYPES "fieldTypes"
#define PVSS_CONDDB_DPE_DPLIST "dpList"
#define PVSS_CONDDB_DPE_UPDATEMECHANISM "updateMechanism.mechanism"
#define PVSS_CONDDB_DPE_UPDATEINTERVAL "updateMechanism.interval"
#define PVSS_CONDDB_DPE_CHANNELIDINFO "channelIdInfo"
#define PVSS_CONDDB_DPE_CHANNELIDTYPE "channelIdType"
#define PVSS_CONDDB_DPE_USELIST "useList"

#endif // End of gate keeper code '__PVSS_ARCHIVE_FOLDER_CONFIG_DPE_NAMES_H__'
