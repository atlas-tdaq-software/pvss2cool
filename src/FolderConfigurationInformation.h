#ifndef __FOLDER_CONFIGURATION_INFORMATION_H__  // Gate keeper code
#define __FOLDER_CONFIGURATION_INFORMATION_H__

// Include file for class which stores information on the folder configuration
//
// Created: 27.03.2007
// Author:  Jim Cook

// Unnecessary class memebers (and corresponding actions in the .cpp file)
// commented 30.11.12 by SK

// Include Files
// -------------
#include <ostream>
#include <string>

// Class Definitions
// -----------------
class FolderConfigurationInformation {
  public:
    FolderConfigurationInformation();
    ~FolderConfigurationInformation() {;}

    FolderConfigurationInformation(bool useList,
                                   bool listChanged,
                                   long newFolderId,
                                   long newFolderGroup,
//                                   long newMechanism,
//                                   long newInterval,
                                   long newChannelIdType,
                                   long newDptInfo,
                                   long newStatusReq,
                                   long newStatus,
                                   std::string newFolderName,
                                   std::string newFolderDescription,
                                   std::string newGroup,
                                   std::string newDpTypeName,
                                   std::string newDpSubTypeName,
                                   std::string newPattern,
                                   std::string newChannelIdInfo,
                                   std::string updateTimeBefore,
                                   std::string updateTimeAfter,
								   std::string newTimePrecision);

    FolderConfigurationInformation(const FolderConfigurationInformation &old);
    FolderConfigurationInformation& operator=(const FolderConfigurationInformation &rhs);
    bool operator==(const FolderConfigurationInformation &rhs);
    bool operator==(const long cmpFolderId);
    bool operator==(const std::string cmpFolderName);

    std::ostream &toOutputStream(std::ostream &os, bool showHeading = false) const;

    inline bool usesList() {return (m_usesList);}
    inline bool listChange() {return (m_listChange);}
    inline long folderId() {return (m_folderId);}
    inline long folderGroup() {return (m_folderGroup);}
//    inline long mechanism() {return (m_mechanism);}
//    inline long interval() {return (m_interval);}
    inline long channelIdType() {return (m_channelIdType);}
    inline long dpTypeInfo() {return (m_dptInfo);}
    inline long statusReq() {return (m_statusReq);}
    inline long status() {return (m_status);}
    inline std::string folderName() {return (m_folderName);}
    inline std::string folderDescription() {return (m_folderDescription);}
    inline std::string group() {return (m_group);}
    inline std::string dpTypeName() {return (m_dpTypeName);}
    inline std::string dpSubTypeName() {return (m_dpSubTypeName);}
    inline std::string pattern() {return (m_pattern);}
    inline std::string channelIdInfo() {return (m_channelIdInfo);}
    inline std::string lastUpdateBefore() {return (m_lastUpdateBefore);}
    inline std::string lastUpdateAfter() {return (m_lastUpdateAfter);}
    inline std::string timePrecision() {return (m_timePrecision);}

    inline void usesList(bool useList) {m_usesList = useList;}
    inline void listChange(bool listChanged) {m_listChange = listChanged;}
    inline void folderId(long newFolderId) {m_folderId = newFolderId;}
    inline void folderGroup(long newFolderGroup) {m_folderGroup = newFolderGroup;}
//    inline void mechanism(long newMechanism) {m_mechanism = newMechanism;}
//    inline void interval(long newInterval) {m_interval = newInterval;}
    inline void channelIdType(long newChannelIdType) {m_channelIdType = newChannelIdType;}
    inline void dpTypeInfo(long newDptInfo) {m_dptInfo = newDptInfo;}
    inline void statusReq(long newStatusReq) {m_statusReq = newStatusReq;}
    inline void status(long newStatus) {m_status = newStatus;}
    inline void folderName(std::string newFolderName) {m_folderName = newFolderName;}
    inline void folderDescription(std::string newFolderDescription) {m_folderDescription = newFolderDescription;}
    inline void group(std::string newGroup) {m_group = newGroup;}
    inline void dpTypeName(std::string newDpTypeName) {m_dpTypeName = newDpTypeName;}
    inline void dpSubTypeName(std::string newDpSubTypeName) {m_dpSubTypeName = newDpSubTypeName;}
    inline void pattern(std::string newPattern) {m_pattern = newPattern;}
    inline void channelIdInfo(std::string newChannelIdInfo) {m_channelIdInfo = newChannelIdInfo;}
    inline void lastUpdateBefore(std::string updateTime) {m_lastUpdateBefore = updateTime;}
    inline void lastUpdateAfter(std::string updateTime) {m_lastUpdateAfter = updateTime;}

  private:
    bool m_usesList;
    bool m_listChange;

    long m_folderId;
    long m_folderGroup;
//    long m_mechanism;
//    long m_interval;
//    long m_intervalCount; // Not needed?
    long m_channelIdType;
    long m_dptInfo;
    long m_statusReq;
    long m_status;

    std::string m_folderName;
    std::string m_folderDescription;
    std::string m_group;
    std::string m_dpTypeName;
    std::string m_dpSubTypeName;
    std::string m_pattern;
    std::string m_channelIdInfo;
    std::string m_lastUpdateBefore;
    std::string m_lastUpdateAfter;
    std::string m_timePrecision;
};

#endif // End of gate keeper code '__FOLDER_CONFIGURATION_INFORMATION_H__'
