// Include files
#include "CoralBase/Exception.h"
#include "CoolKernel/IDatabaseSvc.h"
#include "CoolApplication/Application.h"
#include "RelationalAccess/IConnectionService.h"
#include "RelationalAccess/ISessionProxy.h"
#include "RelationalAccess/ISchema.h"
#include "RelationalAccess/ITransaction.h"
#include "RelationalAccess/IQuery.h"
#include "RelationalAccess/ICursor.h"
#include "RelationalAccess/ITable.h"
#include "RelationalAccess/ITableDataEditor.h"
#include "RelationalAccess/SchemaException.h"
// Commented moved to private headers 14.10.10 by SK
//#include "CoralBase/Attribute.h"
//#include "CoralBase/AttributeList.h"
//#include "CoralBase/TimeStamp.h"
#include "CoralBase/AttributeSpecification.h"
#include "CoralBase/AttributeListSpecification.h"

#include <iostream>
#include <exception>

// Application Specific Include Files
// ----------------------------------
#include "globals.h"
#include "CondDBUtilities.h"
#include "PvssCoolStatistics.h"
#include "PvssCoolDefaultConnections.h"

//-----------------------------------------------------------------------------
PvssCoolStatistics::PvssCoolStatistics()
: m_isRunning(true),
  m_isExisting(false),
  m_session(0)
{
  this->initialise();
  return;
}

PvssCoolStatistics::~PvssCoolStatistics()
{
  return;
}

void PvssCoolStatistics::initialise(std::string folderName /* = std::string("") */)
{
  this->m_avgIovsPerHour = 0;
  this->m_minIovsPerHour = -1;
  this->m_maxIovsPerHour = 0;
  this->m_avgReadTimeForHour = 0.0;
  this->m_minReadTimeForHour = -1.0;
  this->m_maxReadTimeForHour = 0.0;
  this->m_avgInsertTimeForHour = 0.0;
  this->m_minInsertTimeForHour = -1.0;
  this->m_maxInsertTimeForHour = 0.0;
  if (!folderName.empty())
    this->m_folderName = folderName;
  this->m_totalHours = 0.0;
  return;
}

bool PvssCoolStatistics::connect(coral::IConnectionService& connectionService)
{
  bool status = true;

  try {
    this->m_session = connectionService.connect(PVSS_COOL_STATISTICS_CONNECTION,
                                                FW_CONDDB_WRITER,
                                                coral::Update);
  }

  //==================
  // CATCH EXCEPTIONS
  //==================
  catch ( coral::Exception& e )
  {
    std::cerr << "'PvssCoolStatistics::connect()' CORAL exception : " << e.what() << std::endl;
    status = false;
  }
  catch ( std::exception& e )
  {
    std::cerr << "'PvssCoolStatistics::connect()' Standard C++ exception : " << e.what() << std::endl;
    status = false;
  }
  catch( ... )
  {
    std::cerr << "'PvssCoolStatistics::connect()' Unknown exception ..." << std::endl;
    status = false;
  }

  return (status);
}

void PvssCoolStatistics::disconnect()
{
  if (this->m_session)
    delete this->m_session;

  return;
}

void PvssCoolStatistics::resetStatistics(coral::IConnectionService& connectionService,
                                         std::string folderName)
{
  try {
    if (this->connect(connectionService)) {
      this->readStatistics(folderName);
      if (this->m_isExisting) {
        this->initialise();
        this->writeStatistics();
      }
      this->disconnect();
    }
  }

  //==================
  // CATCH EXCEPTIONS
  //==================
  catch ( coral::Exception& e )
  {
    std::cerr << "'PvssCoolStatistics::resetStatistics()' CORAL exception : " << e.what() << std::endl;
  }
  catch ( std::exception& e )
  {
    std::cerr << "'PvssCoolStatistics::resetStatistics()' Standard C++ exception : " << e.what() << std::endl;
  }
  catch( ... )
  {
    std::cerr << "'PvssCoolStatistics::resetStatistics()' Unknown exception ..." << std::endl;
  }

  return;
}

void PvssCoolStatistics::setRunning(coral::IConnectionService& connectionService,
                                    std::string folderName,
                                    bool isRunning /* = true */)
{
  try {
    if (this->connect(connectionService)) {
      this->readStatistics(folderName);
      if (this->m_isExisting) {
        this->m_isRunning = isRunning;
        this->writeStatistics();
      }
      this->disconnect();
    }
  }

  //==================
  // CATCH EXCEPTIONS
  //==================
  catch ( coral::Exception& e )
  {
    std::cerr << "'PvssCoolStatistics::setRunning()' CORAL exception : " << e.what() << std::endl;
  }
  catch ( std::exception& e )
  {
    std::cerr << "'PvssCoolStatistics::setRunning()' Standard C++ exception : " << e.what() << std::endl;
  }
  catch( ... )
  {
    std::cerr << "'PvssCoolStatistics::setRunning()' Unknown exception ..." << std::endl;
  }

  return;
}

void PvssCoolStatistics::updateStatistics(coral::IConnectionService& connectionService,
                                          std::string folderName,
                                          int iovCount,
                                          double readTime,
                                          double insertTime,
                                          int timeSpan,
                                          std::string lastUpdate)
{
  // Convert string time into coral::TimeStamp
  coral::TimeStamp lastUpdateTS = CondDBUtilities::getTimeStamp(lastUpdate);

  // Call function with coral::TimeStamp
  this->updateStatistics(connectionService, folderName, iovCount, readTime, insertTime, timeSpan, lastUpdateTS);
}

void PvssCoolStatistics::updateStatistics(coral::IConnectionService& connectionService,
                                          std::string folderName,
                                          int iovCount,
                                          double readTime,
                                          double insertTime,
                                          int timeSpan,
                                          coral::TimeStamp lastUpdate)
{
  int iovsPerHour = 0;
  double readTimeForHour = 0.0;
  double insertTimeForHour = 0.0;

  double forHours = (double)timeSpan / 3600.0;

  if (forHours > 0) {
    iovsPerHour = (int)(iovCount / forHours);
    readTimeForHour = readTime / forHours;
    insertTimeForHour = insertTime / forHours;
  }

  try {
    if (this->connect(connectionService)) {
      this->readStatistics(folderName);

      // Calculate new minimums
      if ((this->m_minIovsPerHour < 0) || (iovsPerHour < this->m_minIovsPerHour))
        this->m_minIovsPerHour = iovsPerHour;
      if ((this->m_minReadTimeForHour < 0) || (readTimeForHour < this->m_minReadTimeForHour))
        this->m_minReadTimeForHour = readTimeForHour;
      if ((this->m_minInsertTimeForHour < 0) || (insertTimeForHour < this->m_minInsertTimeForHour))
        this->m_minInsertTimeForHour = insertTimeForHour;

      // Calculate new averages
      this->m_avgIovsPerHour = (int)this->calculateNewAverage(this->m_avgIovsPerHour,
                                                              this->m_totalHours,
                                                              iovCount,
                                                              forHours);
      this->m_avgReadTimeForHour = this->calculateNewAverage(this->m_avgReadTimeForHour,
                                                             this->m_totalHours,
                                                             readTime,
                                                             forHours);
      this->m_avgInsertTimeForHour = this->calculateNewAverage(this->m_avgInsertTimeForHour,
                                                               this->m_totalHours,
                                                               insertTime,
                                                               forHours);

      // Calculate new maximums
      if (iovsPerHour > this->m_maxIovsPerHour)
        this->m_maxIovsPerHour = iovsPerHour;
      if (readTimeForHour > this->m_maxReadTimeForHour)
        this->m_maxReadTimeForHour = readTimeForHour;
      if (insertTimeForHour > this->m_maxInsertTimeForHour)
        this->m_maxInsertTimeForHour = insertTimeForHour;

      // Set last update time
      this->m_lastUpdate = lastUpdate;

      // Calculate total hours (this has to be done last
      this->m_totalHours += forHours;

      // Write the information back
      this->writeStatistics();
      this->disconnect();
    }
  }

  //==================
  // CATCH EXCEPTIONS
  //==================
  catch ( coral::Exception& e )
  {
    std::cerr << "'PvssCoolStatistics::updateStatistics()' CORAL exception : " << e.what() << std::endl;
  }
  catch ( std::exception& e )
  {
    std::cerr << "'PvssCoolStatistics::updateStatistics()' Standard C++ exception : " << e.what() << std::endl;
  }
  catch( ... )
  {
    std::cerr << "'PvssCoolStatistics::updateStatistics()' Unknown exception ..." << std::endl;
  }

  return;
}

void PvssCoolStatistics::readStatistics(std::string folderName)
{
  try {

    // Initialise base information
    this->initialise(folderName);
    this->m_isExisting = false;

    // Create the query
    this->m_session->transaction().start(true);
    coral::ISchema& mySchema = this->m_session->nominalSchema();
    coral::IQuery* myQuery = mySchema.newQuery();

    // Add the 'SELECT' part
    coral::AttributeList outputList;
    myQuery->addToOutputList("IS_RUNNING");
    outputList.extend("IS_RUNNING", "bool");
    myQuery->addToOutputList("TOTAL_HOURS");
    outputList.extend("TOTAL_HOURS", "double");
    myQuery->addToOutputList("LAST_UPDATE_TIME");
    outputList.extend("LAST_UPDATE_TIME", coral::AttributeSpecification::typeNameForId(typeid(coral::TimeStamp)));
    myQuery->addToOutputList("AVG_IOVS_PER_HOUR");
    outputList.extend("AVG_IOVS_PER_HOUR", "int");
    myQuery->addToOutputList("MIN_IOVS_PER_HOUR");
    outputList.extend("MIN_IOVS_PER_HOUR", "int");
    myQuery->addToOutputList("MAX_IOVS_PER_HOUR");
    outputList.extend("MAX_IOVS_PER_HOUR", "int");
    myQuery->addToOutputList("AVG_REDTIME_FOR_HOUR");
    outputList.extend("AVG_REDTIME_FOR_HOUR", "double");
    myQuery->addToOutputList("MIN_REDTIME_FOR_HOUR");
    outputList.extend("MIN_REDTIME_FOR_HOUR", "double");
    myQuery->addToOutputList("MAX_REDTIME_FOR_HOUR");
    outputList.extend("MAX_REDTIME_FOR_HOUR", "double");
    myQuery->addToOutputList("AVG_INSTIME_FOR_HOUR");
    outputList.extend("AVG_INSTIME_FOR_HOUR", "double");
    myQuery->addToOutputList("MIN_INSTIME_FOR_HOUR");
    outputList.extend("MIN_INSTIME_FOR_HOUR", "double");
    myQuery->addToOutputList("MAX_INSTIME_FOR_HOUR");
    outputList.extend("MAX_INSTIME_FOR_HOUR", "double");

    // Add the 'FROM' part
    myQuery->addToTableList(PVSS_COOL_STATISTICS_TABLE);

    // Add 'WHERE' clause
    coral::AttributeList bindList;
    bindList.extend("folderName", "string");
    bindList["folderName"].setValue(this->m_folderName);
    myQuery->setCondition("FOLDERNAME = :folderName", bindList);

    myQuery->defineOutput(outputList);
    myQuery->setRowCacheSize(5);

    coral::ICursor& myCursor = myQuery->execute();

    while (myCursor.next()) {

      this->m_isExisting = true;

      const coral::AttributeList& row = myCursor.currentRow();

      this->m_isRunning = row["IS_RUNNING"].data<bool>();
      this->m_totalHours = row["TOTAL_HOURS"].data<double>();
      this->m_lastUpdate = row["LAST_UPDATE_TIME"].data<coral::TimeStamp>();
      this->m_avgIovsPerHour = row["AVG_IOVS_PER_HOUR"].data<int>();
      this->m_minIovsPerHour = row["MIN_IOVS_PER_HOUR"].data<int>();
      this->m_maxIovsPerHour = row["MAX_IOVS_PER_HOUR"].data<int>();
      this->m_avgReadTimeForHour = row["AVG_REDTIME_FOR_HOUR"].data<double>();
      this->m_minReadTimeForHour = row["MIN_REDTIME_FOR_HOUR"].data<double>();
      this->m_maxReadTimeForHour = row["MAX_REDTIME_FOR_HOUR"].data<double>();
      this->m_avgInsertTimeForHour = row["AVG_INSTIME_FOR_HOUR"].data<double>();
      this->m_minInsertTimeForHour = row["MIN_INSTIME_FOR_HOUR"].data<double>();
      this->m_maxInsertTimeForHour = row["MAX_INSTIME_FOR_HOUR"].data<double>();
    }

    /*
    std::cout << this->m_folderName << ", " << this->m_isRunning << ", " << this->m_totalHours << ", " << CondDBUtilities::formatTime(this->m_lastUpdate)
       << ", " << this->m_avgIovsPerHour << ", " << this->m_minIovsPerHour << ", " << this->m_maxIovsPerHour
       << ", " << this->m_avgReadTimeForHour << ", " << this->m_minReadTimeForHour << ", " << this->m_maxReadTimeForHour
       << ", " << this->m_avgInsertTimeForHour << ", " << this->m_minInsertTimeForHour << ", " << this->m_maxInsertTimeForHour << std::endl;
    */

    delete myQuery;
    this->m_session->transaction().commit();
  }

  //==================
  // CATCH EXCEPTIONS
  //==================
  catch ( coral::Exception& e )
  {
    std::cerr << "'PvssCoolStatistics::readStatistics()' CORAL exception : " << e.what() << std::endl;
  }
  catch ( std::exception& e )
  {
    std::cerr << "'PvssCoolStatistics::readStatistics()' Standard C++ exception : " << e.what() << std::endl;
  }
  catch( ... )
  {
    std::cerr << "'PvssCoolStatistics::readStatistics()' Unknown exception ..." << std::endl;
  }

  return;
}

void PvssCoolStatistics::writeStatistics()
{
  coral::AttributeList payload;

  try {

    this->m_session->transaction().start();
    coral::ISchema& mySchema = this->m_session->nominalSchema();
    coral::ITableDataEditor& myEditor = mySchema.tableHandle(PVSS_COOL_STATISTICS_TABLE).dataEditor();

    // Set up the folder payload
    myEditor.rowBuffer(payload);
    payload["FOLDERNAME"].data<std::string>() = this->m_folderName;
    payload["IS_RUNNING"].data<bool>() = this->m_isRunning;
    payload["TOTAL_HOURS"].data<double>() = this->m_totalHours;
    payload["LAST_UPDATE_TIME"].data<coral::TimeStamp>() = this->m_lastUpdate;
    payload["AVG_IOVS_PER_HOUR"].data<int>() = this->m_avgIovsPerHour;
    payload["MIN_IOVS_PER_HOUR"].data<int>() = this->m_minIovsPerHour;
    payload["MAX_IOVS_PER_HOUR"].data<int>() = this->m_maxIovsPerHour;
    payload["AVG_REDTIME_FOR_HOUR"].data<double>() = this->m_avgReadTimeForHour;
    payload["MIN_REDTIME_FOR_HOUR"].data<double>() = this->m_minReadTimeForHour;
    payload["MAX_REDTIME_FOR_HOUR"].data<double>() = this->m_maxReadTimeForHour;
    payload["AVG_INSTIME_FOR_HOUR"].data<double>() = this->m_avgInsertTimeForHour;
    payload["MIN_INSTIME_FOR_HOUR"].data<double>() = this->m_minInsertTimeForHour;
    payload["MAX_INSTIME_FOR_HOUR"].data<double>() = this->m_maxInsertTimeForHour;

    /*
    std::cout << this->m_folderName << ", " << this->m_isRunning << ", " << this->m_totalHours << ", " << CondDBUtilities::formatTime(this->m_lastUpdate)
       << ", " << this->m_avgIovsPerHour << ", " << this->m_minIovsPerHour << ", " << this->m_maxIovsPerHour
       << ", " << this->m_avgReadTimeForHour << ", " << this->m_minReadTimeForHour << ", " << this->m_maxReadTimeForHour
       << ", " << this->m_avgInsertTimeForHour << ", " << this->m_minInsertTimeForHour << ", " << this->m_maxInsertTimeForHour << std::endl;
    */

    if (this->m_isExisting) {

      // Update required information
      myEditor.updateRows(std::string("IS_RUNNING = :IS_RUNNING, TOTAL_HOURS = :TOTAL_HOURS, LAST_UPDATE_TIME = :LAST_UPDATE_TIME, AVG_IOVS_PER_HOUR = :AVG_IOVS_PER_HOUR, MIN_IOVS_PER_HOUR = :MIN_IOVS_PER_HOUR, MAX_IOVS_PER_HOUR = :MAX_IOVS_PER_HOUR, AVG_REDTIME_FOR_HOUR = :AVG_REDTIME_FOR_HOUR, MIN_REDTIME_FOR_HOUR = :MIN_REDTIME_FOR_HOUR, MAX_REDTIME_FOR_HOUR = :MAX_REDTIME_FOR_HOUR, AVG_INSTIME_FOR_HOUR = :AVG_INSTIME_FOR_HOUR, MIN_INSTIME_FOR_HOUR = :MIN_INSTIME_FOR_HOUR, MAX_INSTIME_FOR_HOUR = :MAX_INSTIME_FOR_HOUR"),
                          "FOLDERNAME = :folderName",
                          payload);

    } else {

      // Insert the information
      myEditor.insertRow(payload);
    }

    // If we get to here, then it was successful, so commit the transaction
    this->m_session->transaction().commit();
  }

  //==================
  // CATCH EXCEPTIONS
  //==================
  catch ( coral::Exception& e )
  {
    std::cerr << "'PvssCoolStatistics::writeStatistics()' CORAL exception : " << e.what() << std::endl;
    this->m_session->transaction().rollback();
  }
  catch ( std::exception& e )
  {
    std::cerr << "'PvssCoolStatistics::writeStatistics()' Standard C++ exception : " << e.what() << std::endl;
    this->m_session->transaction().rollback();
  }
  catch( ... )
  {
    std::cerr << "'PvssCoolStatistics::writeStatistics()' Unknown exception ..." << std::endl;
    this->m_session->transaction().rollback();
  }

  return;
}

double PvssCoolStatistics::calculateNewAverage(double currentAvg,
                                               double totalHours,
                                               double newCount,
                                               int timeSpan)
{
  double forHours = (double)timeSpan / 3600.0;
  return (this->calculateNewAverage(currentAvg, totalHours, newCount, forHours));
}

double PvssCoolStatistics::calculateNewAverage(double currentAvg,
                                               double totalHours,
                                               double newCount,
                                               double timeSpan)
{
  double newAverage = 0.0;

  if ((totalHours + timeSpan) > 0)
    newAverage = ((currentAvg * totalHours) + newCount) / (totalHours + timeSpan);

  return (newAverage);
}

