// Include files
// Commented moved to private headers 14.10.10 by SK
//#include "CoralBase/Exception.h"
//#include "CoralBase/Attribute.h"
//#include "CoralBase/AttributeList.h"
#include "RelationalAccess/ISessionProxy.h"
#include "RelationalAccess/ITransaction.h"
#include "RelationalAccess/ISchema.h"
#include "RelationalAccess/IQuery.h"
#include "RelationalAccess/IQueryDefinition.h"
#include "RelationalAccess/ICursor.h"

#include <iostream>
#include <exception>

#include "PvssArchiveNumber.h"
#include "PvssArchiveUtilities.h"

//-----------------------------------------------------------------------------

PvssArchiveUtilities::PvssArchiveUtilities()
{
  return;
}

PvssArchiveUtilities::~PvssArchiveUtilities()
{
  return;
}

bool PvssArchiveUtilities::getArchiveNumbers(coral::ISessionProxy* connection,
                                             std::vector<PvssArchiveNumber> &archiveNumbers,
                                             std::string groupName,
                                             std::string timeStart,
                                             std::string timeEnd /* = std::string("") */)
{
  bool status = false;

  long archiveNumber;

  std::string sTime;
  std::string eTime;

  try {

    // Ensure no data is currently in return vector
    archiveNumbers.clear();

    // Check for optional argument
    if (timeEnd.empty())
      timeEnd = timeStart;

    // Start a new (read-only) transaction and get working schema
    connection->transaction().start(true);
    coral::ISchema& mySchema = connection->nominalSchema();

    // Create the query and the intersection required
    coral::IQuery* myQuery = mySchema.newQuery();
    coral::IQueryDefinition &arcNumbers = myQuery->defineSubQuery("ARC_NUMBERS");
    coral::IQueryDefinition &intersect = arcNumbers.applySetOperation(coral::IQueryDefinition::Intersect);

    // Select the output for the intersection part
    intersect.addToOutputList("ARCHIVE#");
    intersect.addToOutputList("START_TIME");
    intersect.addToOutputList("END_TIME");

    // Add the table
    intersect.addToTableList("ARC_ARCHIVE");

    // Set the conditions (confusing as START_TIME is compared to the endTime given!)
    coral::AttributeList bindList1;
    bindList1.extend("groupName", "string");
    bindList1["groupName"].setValue(groupName);
    bindList1.extend("endTime", "string");
    bindList1["endTime"].setValue(timeEnd);
    intersect.setCondition("GROUP_NAME = :groupName AND START_TIME < TO_DATE(:endTime, 'dd-mm-yyyy hh24:mi:ss')", bindList1);

    // Select the output for the sub query
    arcNumbers.addToOutputList("ARCHIVE#");
    arcNumbers.addToOutputList("START_TIME");
    arcNumbers.addToOutputList("END_TIME");

    // Add the table
    arcNumbers.addToTableList("ARC_ARCHIVE");

    // Set the conditions (confusing as END_TIME is compared to the startTime given!)
    coral::AttributeList bindList2;
    bindList2.extend("groupName", "string");
    bindList2["groupName"].setValue(groupName);
    bindList2.extend("startTime", "string");
    bindList2["startTime"].setValue(timeStart);
    arcNumbers.setCondition("GROUP_NAME = :groupName AND (END_TIME > TO_DATE(:startTime, 'dd-mm-yyyy hh24:mi:ss') OR END_TIME IS NULL)", bindList2);

    // Set the output specification for the main query
    coral::AttributeList outputList;
    myQuery->addToOutputList("ARCHIVE#");
    outputList.extend("ARCHIVE#", "long");
    myQuery->addToOutputList("TO_CHAR(START_TIME, 'dd-mm-yyyy hh24:mi:ss')", "S_TIME");
    outputList.extend("S_TIME", "string");
    myQuery->addToOutputList("TO_CHAR(END_TIME, 'dd-mm-yyyy hh24:mi:ss')", "E_TIME");
    outputList.extend("E_TIME", "string");

    // Add the table
    myQuery->addToTableList("ARC_NUMBERS");

    // Set output information, order by clause and execute
    myQuery->defineOutput(outputList);
//========================
// Single line replaced 14.10.10 by SK (Could violate the 'natural' order depending on the day number )
//    myQuery->addToOrderList("S_TIME");
	myQuery->addToOrderList("START_TIME");
//========================
    myQuery->setRowCacheSize(10);
    coral::ICursor& myCursor = myQuery->execute();

    // Retrieve data and enter into return vector
    while (myCursor.next()) {
      const coral::AttributeList& row = myCursor.currentRow();
      archiveNumber = row["ARCHIVE#"].data<long>();
      sTime = row["S_TIME"].data<std::string>();
      eTime = row["E_TIME"].data<std::string>();

      PvssArchiveNumber arcInfo(archiveNumber, sTime, eTime);
      archiveNumbers.push_back(arcInfo);
    }

    // Set the first time to be the start time requested
    archiveNumbers.front().startTime(timeStart);

    // Set the last time to be the end time requested
    archiveNumbers.back().endTime(timeEnd);

// Debug msg 14.10.10 by SK
//if(archiveNumbers.size() > 1)
//	for(unsigned int i = 0; i < archiveNumbers.size(); i++) 
//		std::cout << "Table " << archiveNumbers[i].archiveNumber() << " since " << archiveNumbers[i].startTime() << " until " << archiveNumbers[i].endTime() << std::endl;

    // Clean up
    delete myQuery;
    connection->transaction().commit();
    if (archiveNumbers.size() > 0)
      status = true;
  }

  //==================
  // CATCH EXCEPTIONS
  //==================
  catch ( coral::Exception& e )
  {
    std::cerr << "'PvssArchiveUtilities::getArchiveNumbers()' CORAL exception : " << e.what() << std::endl;
  }
  catch ( std::exception& e )
  {
    std::cerr << "'PvssArchiveUtilities::getArchiveNumbers()' Standard C++ exception : " << e.what() << std::endl;
  }
  catch( ... )
  {
    std::cerr << "'PvssArchiveUtilities::getArchiveNumbers()' Unknown exception ..." << std::endl;
  }

  return (status);
}

PvssArchiveNumber PvssArchiveUtilities::getCurrentArchive(coral::ISessionProxy* connection,
                                                          std::string groupName)
{
  long archiveNumber = 0;

  std::string sTime;

  try {

    // Start a new (read-only) transaction and get working schema
    connection->transaction().start(true);
    coral::ISchema& mySchema = connection->nominalSchema();

    // Create the query
    coral::IQuery* myQuery = mySchema.newQuery();

    // Set the output specification
    coral::AttributeList outputList;
    myQuery->addToOutputList("ARCHIVE#");
    outputList.extend("ARCHIVE#", "long");
    myQuery->addToOutputList("TO_CHAR(START_TIME, 'dd-mm-yyyy hh24:mi:ss')", "S_TIME");
    outputList.extend("S_TIME", "string");

    // Add the table
    myQuery->addToTableList("ARC_ARCHIVE");

    // Set the conditions
    coral::AttributeList bindList;
    bindList.extend("groupName", "string");
    bindList["groupName"].setValue(groupName);
    myQuery->setCondition("GROUP_NAME = :groupName AND STATUS = 'CURRENT'", bindList);

    // Set output information and execute
    myQuery->defineOutput(outputList);
    myQuery->setRowCacheSize(10);
    coral::ICursor& myCursor = myQuery->execute();

    // Retrieve data and enter into return vector
    while (myCursor.next()) {
      const coral::AttributeList& row = myCursor.currentRow();
      archiveNumber = row["ARCHIVE#"].data<long>();
      sTime = row["S_TIME"].data<std::string>();
    }

    // Clean up
    delete myQuery;
    connection->transaction().commit();
  }
 
  //==================
  // CATCH EXCEPTIONS
  //==================
  catch ( coral::Exception& e )
  {
    std::cerr << "'PvssArchiveUtilities::getCurrentArchive()' CORAL exception : " << e.what() << std::endl;
  }
  catch ( std::exception& e )
  {
    std::cerr << "'PvssArchiveUtilities::getCurrentArchive()' Standard C++ exception : " << e.what() << std::endl;
  }
  catch( ... )
  {
    std::cerr << "'PvssArchiveUtilities::getCurrentArchive()' Unknown exception ..." << std::endl;
  }

  return (PvssArchiveNumber(archiveNumber, sTime));
}

long PvssArchiveUtilities::getElementId(coral::ISessionProxy* connection,
                                             long sysId,
                                             long dpId,
                                             long dpeId)
{
  long elementId = -1;

  try {

    // Use query to get element ID required
    connection->transaction().start(true);
    coral::ISchema& mySchema = connection->nominalSchema();
    coral::IQuery* myQuery = mySchema.newQuery();
    coral::AttributeList outputList;
    myQuery->addToOutputList("ELEMENT_ID");
    outputList.extend("ELEMENT_ID", "long");
    myQuery->addToTableList("ELEMENTS");
    coral::AttributeList bindList;
    bindList.extend("dpeId", "long");
    bindList.extend("sysId", "long");
    bindList.extend("dpId", "long");
    bindList["dpeId"].setValue(dpeId);
    bindList["sysId"].setValue(sysId);
    bindList["dpId"].setValue(dpId);
    myQuery->setCondition("DPE_ID = :dpeId AND SYS_ID = :sysId AND DP_ID = :dpId", bindList);
    myQuery->defineOutput(outputList);
    myQuery->setRowCacheSize(10);
    coral::ICursor& myCursor = myQuery->execute();
    while (myCursor.next()) {
      const coral::AttributeList& row = myCursor.currentRow();
      elementId = row["ELEMENT_ID"].data<long>();
    }
    delete myQuery;
    connection->transaction().commit();
  }
 
  //==================
  // CATCH EXCEPTIONS
  //==================
  catch ( coral::Exception& e )
  {
    std::cerr << "'PvssArchiveUtilities::getElementId()' CORAL exception : " << e.what() << std::endl;
  }
  catch ( std::exception& e )
  {
    std::cerr << "'PvssArchiveUtilities::getElementId()' Standard C++ exception : " << e.what() << std::endl;
  }
  catch( ... )
  {
    std::cerr << "'PvssArchiveUtilities::getElementId()' Unknown exception ..." << std::endl;
  }

  return (elementId);
}

