#ifndef __PVSS_ARCHIVE_FOLDER_DATA_H__  // Gate keeper code
#define __PVSS_ARCHIVE_FOLDER_DATA_H__

// Include file for class which extracts the data to be entered into the
// conditions database folders as described by the configuration
//
// Created: 19.07.2005
// Author:  Jim Cook

// Include Files
// -------------
#include "CoralBase/Attribute.h"
#include "CoralBase/AttributeList.h"
#include "CoralBase/TimeStamp.h"

#include "PvssArchiveFolderConfig.h"
#include "PvssArchiveNumber.h"

// Forward Class Declarations
// --------------------------

// Commented 14.10.10 by SK
//class coral::ISessionProxy;
//class coral::IQuery;
//class coral::ICursor;

// Class Definitions
// -----------------
class PvssArchiveFolderData {
  public:
    PvssArchiveFolderData();
    ~PvssArchiveFolderData();

    coral::ICursor& getFolderData(std::string folderConfigurationTable,
	                              FolderConfigurationInformation& folderConfig,
                                  std::vector<FolderFieldConfiguration>& folderFields,
                                  PvssArchiveNumber& archiveNumber,
                                  int rowCacheSize,
                                  double &readTime,
								  bool milliPrecision);		// Introduced 22.06.10 by SK
    bool connect(coral::IConnectionService& connectionService, std::string serviceName);
    inline coral::ISessionProxy* connection() {return (m_session);}
    void cleanUp();
    bool disconnect();
	// Introduced 02.11.11 by SK. 
	// The function checks whether all necessary DPEs are being archived
	// and returns the diffrence between "Should be archived" and "Are being archived"
	int dpeNotArchived(std::string folderConfigurationTable,
	                   FolderConfigurationInformation& folderConfig,
					   std::string dpSeparator, int rowCacheSize);
  private:
    coral::ISessionProxy *m_session;

    coral::IQuery *m_query;
    coral::ICursor *m_cursor;
	
};

#endif // End of gate keeper code '__PVSS_ARCHIVE_FOLDER_DATA_H__'
