#ifndef __GLOBALS_H__	// Gate-keeper code
#define __GLOBALS_H__
// ----------------------------------------------------------------
// File:				  globals.h
//
// Description:	  Include file for global constants
//
// Author:			  J.R.Cook
// Creation Date:	17/11/2006
// ----------------------------------------------------------------
// Revision History
// ================
//  Version | Inits |    Date    |           Comment
// ---------|-------|------------|---------------------------------
//     1    |  JRC  | 17/11/2006 | Created
//     2	|  SK	| 09/11/2012 | #define FW_CONDDB_DPTINFO_COLL 4
//    		|		|			 | introduced for Collection folder type

//#define TILDE_FIX
//#define PVSS2COOL_CLIENT_TIMING
#define INCLUDE_TEST

#define FW_CONDDB_DEFAULT_ROWCACHE 5000

// DPT information constants
#define FW_CONDDB_DPTINFO_SIMPLE 0
#define FW_CONDDB_DPTINFO_FW 1
#define FW_CONDDB_DPTINFO_REFS 2
#define FW_CONDDB_DPTINFO_FSM 3
#define FW_CONDDB_DPTINFO_COLL 4
#define FW_CONDDB_DPTINFO_ALCOLL 5

// Channel ID type information
//#define FW_CONDDB_IDINFO_INTERNAL 0 // No longer used
#define FW_CONDDB_IDINFO_ALIAS 1
//#define FW_CONDDB_IDINFO_VALUE 2 // No longer used
//#define FW_CONDDB_IDINFO_DERIVE 3 // No longer used
#define FW_CONDDB_IDINFO_DPNAME 4
#define FW_CONDDB_IDINFO_COMMENT 5		// No longer used
#define FW_CONDDB_IDINFO_ALIAS_STRING 6
#define FW_CONDDB_IDINFO_USER_DEFINED 7
#define FW_CONDDB_IDINFO_USER_DEFINED_STRING 8

#define FW_CONDDB_STATUS_ANY -1

// Status of fields and DP list entries
#define FW_CONDDB_STATUS_NOCHANGE 0 // Created and no changes
#define FW_CONDDB_STATUS_NEW 1 // New (to be created)
#define FW_CONDDB_STATUS_REMOVE 2 // Remove (IOV must be closed)
#define FW_CONDDB_STATUS_MODIFY 3 // Modified (e.g. name, description etc.)

// Status of folder entries
#define FW_CONDDB_FOLDERSTAT_NOTEXISTS 0 // Folder does not exist
#define FW_CONDDB_FOLDERSTAT_EXISTS 1 // Folder exists (normal running)
#define FW_CONDDB_FOLDERSTAT_SUSPENDED 2 // Folder exists (suspended running)

// Requested status for folder entries
#define FW_CONDDB_REQFOLDERSTAT_NOCHANGE 0 // No changes requested
#define FW_CONDDB_REQFOLDERSTAT_NEW 1 // New (to be created)
#define FW_CONDDB_REQFOLDERSTAT_SUSPEND 2 // Suspend (IOV must be closed)
#define FW_CONDDB_REQFOLDERSTAT_REINSTATE 3 // Re-instate normal running (after suspended running)

// Role names
#define FW_CONDDB_READER "reader"
#define FW_CONDDB_WRITER "writer"
#define FW_CONDDB_UPDATER "updater"

#endif // End of Gate-keeper code '__GLOBALS_H__'

